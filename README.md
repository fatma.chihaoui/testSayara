Sayara.tn
========
This document contain how to quick start the project in development and production mode
 
 ## Update schema
 Before we start we need to set up the database then update schema
   ```
   php bin/console doctrine:schema:update --force 
   ```
   
 ## Install dependecies
 If the you can't find vendor folder, you need to install composer then run this command
 ```
 composer install
 ```
 ##Admin Account
 To create admin account we need to __change the information below__ then execute the following command
 ```
 php bin/console sayara:admin:create admin3 admin-email@sayara.tn password firstname lastname 00216555333 --super-admin
 ```
 Then you can login at 
 ```
 http://127.0.0.1:8000/admin
 ```

## Clear cache in development mode

 ```
php bin/console cache:clear
 ```
 
 
## Clear cache production mode

 ```
 php bin/console cache:clear --no-warmup --env=prod
 ```
 
 ## Fix Symfony permission (Ubuntu)
 run the folliwing command if Apache server had an issue with folder permission
 ```
 ./bin/fixpermession
 ```
 
 ## Dump data
 You can find at the root of this repository an SQL file to quick-start the App.
 
 
 ## Modules
 Here's all the modules used in this project
 * [LiipImagineBundle](https://github.com/liip/LiipImagineBundle)
 * [KnpPaginatorBundle](https://github.com/KnpLabs/KnpPaginatorBundle)

## Running the app locally
```
  php bin/console server:run
 ```