<?php

namespace Sayara\FrontendBundle\EventListener;
use Doctrine\ORM\Event\LifecycleEventArgs;

class UploaderEvent
{
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->uploadFile($entity);
    }

}