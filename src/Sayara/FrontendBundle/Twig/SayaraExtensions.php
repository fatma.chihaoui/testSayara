<?php

namespace Sayara\FrontendBundle\Twig;
use Doctrine\Common\Collections\ArrayCollection;
use Sayara\BackendBundle\Entity\Address;
use Sayara\BackendBundle\Entity\Category;
use Sayara\BackendBundle\Entity\City;
use Sayara\BackendBundle\Entity\Photo;
use Sayara\BackendBundle\Entity\Spare;
use Sayara\BackendBundle\Entity\User;
use Sayara\BackendBundle\Entity\Vehicle;

use Symfony\Bridge\Doctrine\RegistryInterface;

class SayaraExtensions extends \Twig_Extension
{
    protected $doctrine;

    public function __construct(RegistryInterface $doctrine)
    {
        // if you want to do queries execute : $this->doctrine->getManager();
        $this->doctrine = $doctrine;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('nbApp', array($this, 'nbApp')),
            new \Twig_SimpleFunction('nbAppPart', array($this, 'nbAppPart')),
            new \Twig_SimpleFunction('nbAppPro', array($this, 'nbAppPro')),
            new \Twig_SimpleFunction('nbAppCategory', array($this, 'nbAppCategory')),
            new \Twig_SimpleFunction('nbAppCategoryPart', array($this, 'nbAppCategoryPart')),
            new \Twig_SimpleFunction('nbAppCategoryPro', array($this, 'nbAppCategoryPro')),
            new \Twig_SimpleFunction('newApp', array($this, 'newApp')),
            new \Twig_SimpleFunction('used', array($this, 'used')),
            new \Twig_SimpleFunction('nbAppCity', array($this, 'nbAppCity')),
            new \Twig_SimpleFunction('nbAppCityPro', array($this, 'nbAppCityPro')),
            new \Twig_SimpleFunction('nbAppCityPart', array($this, 'nbAppCityPart')),

            new \Twig_SimpleFunction('nbAppState', array($this, 'nbAppState')),
            new \Twig_SimpleFunction('nbAppStatePro', array($this, 'nbAppStatePro')),
            new \Twig_SimpleFunction('nbAppStatePart', array($this, 'nbAppStatePart')),


            new \Twig_SimpleFunction('nbAppPrice', array($this, 'nbAppPrice')),
            /*new \Twig_SimpleFunction('nbAppStatePro', array($this, 'nbAppStatePro')),
            new \Twig_SimpleFunction('nbAppStatePart', array($this, 'nbAppStatePart')),*/
        );
    }

    public function getTests()
    {
        return [
               new \Twig_SimpleTest('vehicle', function (Vehicle $vehicle) { return $vehicle instanceof Vehicle; }),
               new \Twig_SimpleTest('spare', function (Spare $spare) { return $spare instanceof Spare; }),
            ];
    }


    public function nbApp(){

        $em= $this->doctrine->getManager();
        $vehicles= $em->getRepository(Vehicle::class)->findAll();
        $spares= $em->getRepository(Spare::class)->findAll();
        return $nbApp= count($spares)+count($vehicles);
    }

    public function nbAppPart(){

        $em= $this->doctrine->getManager();
        $appParticulars= new ArrayCollection();
        $all=$em->createQuery("SELECT u FROM BackendBundle:User u WHERE u.roles  LIKE '%ROLE_PARTICULIER%'");
        $particulars = $all->getResult();
        foreach ($particulars as $particular){
            if($particular->getVehicles() !== null){
                $applications= $em->getRepository(Vehicle::class)->findByUser($particular);
                if($applications !== []){
                    foreach ($applications as $application){
                        $appParticulars->add($application);
                    }
                }

            }

        }
        foreach ($particulars as $particular){
            if($particular->getSpares() !== null){
                $spares= $em->getRepository(Spare::class)->findByUser($particular);
                if($spares !== []){
                    foreach ($spares as $spare){
                        $appParticulars->add($spare);
                    }
                }

            }

        }
        return $appParticulars->count();
    }

    public function nbAppPro(){

        $em= $this->doctrine->getManager();
        $appPros= new ArrayCollection();
        $all=$em->createQuery("SELECT u FROM BackendBundle:User u WHERE u.roles  LIKE '%ROLE_PRO%'");
        $pros = $all->getResult();
        foreach ($pros as $pro){
            if($pro->getVehicles() !== null){
                $applications= $em->getRepository(Vehicle::class)->findByUser($pro);
                if($applications !== []){
                    foreach ($applications as $application){
                        $appPros->add($application);
                    }
                }

            }

        }
        foreach ($pros as $pro){
            if($pro->getSpares() !== null){
                $spares= $em->getRepository(Spare::class)->findByUser($pro);
                if($spares !== []){
                    foreach ($spares as $spare){
                        $appPros->add($spare);
                    }
                }

            }

        }
        return $appPros->count();
    }

    public function nbAppCategory($code){

        $em= $this->doctrine->getManager();
        $category = $em->getRepository(Category::class)->findByCode($code);
        $all= new ArrayCollection();

        $vehicles= $em->getRepository(Vehicle::class)->findByCategory($category);
        $spares= $em->getRepository(Spare::class)->findByCategory($category);

        foreach ($vehicles as $vehicle){

            $all->add($vehicle);
        }
        foreach ($spares as $spare){

            $all->add($spare);
        }
        return $all->count();
    }

    public function nbAppCategoryPart($code){
        $em= $this->doctrine->getManager();

        $appParts = new ArrayCollection();
        $all=$em->createQuery("SELECT u FROM BackendBundle:User u WHERE u.roles  LIKE '%ROLE_PARTICULIER%'");
        $parts = $all->getResult();
        $category= $em->getRepository(Category::class)->findByCode($code);
        foreach ($parts as $part){
            if($part->getVehicles() !== null){
                $applications= $em->getRepository(Vehicle::class)->findBy(['user'=>$part,'category'=>$category]);
                if($applications !== []){
                    foreach ($applications as $application){

                        $appParts->add($application);
                    }
                }

            }

        }
        foreach ($parts as $part){
            if($part->getSpares() !== null){
                $spares= $em->getRepository(Spare::class)->findBy(['user'=>$part,'category'=>$category]);
                if($spares !== []){
                    foreach ($spares as $spare){

                        $appParts->add($spare);
                    }
                }

            }

        }
        return $appParts->count();
    }

    public function nbAppCategoryPro($code){

        $em= $this->doctrine->getManager();

        $appPros = new ArrayCollection();
        $all=$em->createQuery("SELECT u FROM BackendBundle:User u WHERE u.roles  LIKE '%ROLE_PRO%'");
        $pros = $all->getResult();
        $category= $em->getRepository(Category::class)->findByCode($code);
        foreach ($pros as $pro){
            if($pro->getVehicles() !== null){
                $applications= $em->getRepository(Vehicle::class)->findBy(['user'=>$pro,'category'=>$category]);
                if($applications !== []){
                    foreach ($applications as $application){

                        $appPros->add($application);
                    }
                }

            }

        }
        foreach ($pros as $pro){
            if($pro->getSpares() !== null){
                $spares= $em->getRepository(Spare::class)->findBy(['user'=>$pro,'category'=>$category]);
                if($spares !== []){
                    foreach ($spares as $spare){

                        $appPros->add($spare);
                    }
                }

            }

        }
        return $appPros->count();
    }
    public function newApp(){
        $em= $this->doctrine->getManager();
        $vehicles= $em->getRepository(Vehicle::class)->findByState(1);
        $spares= $em->getRepository(Spare::class)->findByState(1);
        return $nbApp= count($spares)+count($vehicles);
    }
    public function used(){
        $em= $this->doctrine->getManager();
        $vehicles= $em->getRepository(Vehicle::class)->findByState(2);
        $spares= $em->getRepository(Spare::class)->findByState(2);
        return $nbApp= count($spares)+count($vehicles);
    }

    public function nbAppCity($code){
        $em= $this->doctrine->getManager();
        $city = $em->getRepository(City::class)->findOneByCode($code);

        $usersArray= new ArrayCollection();
        $all= new ArrayCollection();
        $addresses = $em->getRepository(Address::class)->findByCity($city->getName());
        if(null !== $addresses){
            foreach ($addresses as $address){
                $users= $em->getRepository(User::class)->findByAddress($address);
                foreach ($users as $user){
                    $usersArray->add($user);
                }
            }
        }
        foreach ($usersArray as $user){
            $vehicles = $em->getRepository(Vehicle::class)->findByUser($user);
            foreach ($vehicles as $vehicle){

                $all->add($vehicle);
            }

            $spares = $em->getRepository(Spare::class)->findByUser($user);
            foreach ($spares as $spare){

                $all->add($spare);
            }
        }
        return $all->count();
    }
    public function nbAppCityPro($code){
        $em= $this->doctrine->getManager();
        $queryBuilder = $em->createQueryBuilder();
        $city = $em->getRepository(City::class)->findOneByCode($code);

        $usersArray= new ArrayCollection();
        $all= new ArrayCollection();

        $pros = $queryBuilder->select('u')
            ->from('BackendBundle:User','u')
            ->leftJoin('u.address' ,'a')
            ->where('a.city = :city')
            ->setParameter('city',$city->getName())
            ->andWhere('u.roles LIKE :role')
            ->setParameter('role', '%ROLE_PRO%')
            ->getQuery()
            ->getResult();

        foreach ($pros as $user){

            $usersArray->add($user);
        }


        foreach ($usersArray as $user){
            $vehicles = $em->getRepository(Vehicle::class)->findByUser($user);
            foreach ($vehicles as $vehicle){

                $all->add($vehicle);
            }

            $spares = $em->getRepository(Spare::class)->findByUser($user);
            foreach ($spares as $spare){

                $all->add($spare);
            }
        }
        return $all->count();
    }
    public function nbAppCityPart($code){
        $em= $this->doctrine->getManager();
        $queryBuilder = $em->createQueryBuilder();
        $city = $em->getRepository(City::class)->findOneByCode($code);

        $usersArray= new ArrayCollection();
        $all= new ArrayCollection();

        $pros = $queryBuilder->select('u')
            ->from('BackendBundle:User','u')
            ->leftJoin('u.address' ,'a')
            ->where('a.city = :city')
            ->setParameter('city',$city->getName())
            ->andWhere('u.roles LIKE :role')
            ->setParameter('role', '%ROLE_PARTICULIER%')
            ->getQuery()
            ->getResult();

        foreach ($pros as $user){

            $usersArray->add($user);
        }


        foreach ($usersArray as $user){
            $vehicles = $em->getRepository(Vehicle::class)->findByUser($user);
            foreach ($vehicles as $vehicle){

                $all->add($vehicle);
            }

            $spares = $em->getRepository(Spare::class)->findByUser($user);
            foreach ($spares as $spare){

                $all->add($spare);
            }
        }
        return $all->count();
    }

    public function nbAppState($state){
        $em= $this->doctrine->getManager();
        $all= new ArrayCollection();

        $vehicles= $em->getRepository(Vehicle::class)->findByState($state);
        $spares= $em->getRepository(Spare::class)->findByState($state);

        foreach ($vehicles as $vehicle){

            $all->add($vehicle);
        }
        foreach ($spares as $spare){

            $all->add($spare);
        }
        return $all->count();
    }

    public function nbAppStatePro($state){
        $em= $this->doctrine->getManager();
        $appPros = new ArrayCollection();
        $all=$em->createQuery("SELECT u FROM BackendBundle:User u WHERE u.roles  LIKE '%ROLE_PRO%'");
        $pros = $all->getResult();
        foreach ($pros as $pro){
            if($pro->getVehicles() !== null){
                $applications= $em->getRepository(Vehicle::class)->findBy(['user'=>$pro,'state'=>$state ]);
                if($applications !== []){
                    foreach ($applications as $application){

                        $appPros->add($application);
                    }
                }

            }

        }
        foreach ($pros as $pro){
            if($pro->getSpares() !== null){
                $spares= $em->getRepository(Spare::class)->findBy(['user'=>$pro,'state'=>$state]);
                if($spares !== []){
                    foreach ($spares as $spare){

                        $appPros->add($spare);
                    }
                }

            }

        }
        return $appPros->count();
    }

    public function nbAppStatePart($state){
        $em= $this->doctrine->getManager();
        $appParts = new ArrayCollection();
        $all=$em->createQuery("SELECT u FROM BackendBundle:User u WHERE u.roles  LIKE '%ROLE_PARTICULIER%'");
        $particulars = $all->getResult();
        foreach ($particulars as $part){
            if($part->getVehicles() !== null){
                $applications= $em->getRepository(Vehicle::class)->findBy(['user'=>$part,'state'=>$state ]);
                if($applications !== []){
                    foreach ($applications as $application){

                        $appParts->add($application);
                    }
                }

            }

        }
        foreach ($particulars as $part){
            if($part->getSpares() !== null){
                $spares= $em->getRepository(Spare::class)->findBy(['user'=>$part,'state'=>$state]);
                if($spares !== []){
                    foreach ($spares as $spare){

                        $appParts->add($spare);
                    }
                }

            }

        }
        return $appParts->count();
    }

    public function nbAppPrice($min, $max){

        $min= intval($min);
        $max= intval($max);
        $queryBuilder = $this->doctrine->getManager()->createQueryBuilder();

        $all= new ArrayCollection();

        if($min < $max){
            $vehicles = $queryBuilder->select('v')
                ->from('BackendBundle:Vehicle','v')
                ->where('v.price BETWEEN :min AND :max')
                ->setParameter('min',$min)

                ->setParameter('max',$max)
                ->getQuery()
                ->getResult();
            $spares = $queryBuilder->select('s')
                ->from('BackendBundle:Spare','s')
                ->where('s.price BETWEEN :min AND :max')
                ->setParameter('min',$min)

                ->setParameter('max',$max)
                ->getQuery()
                ->getResult();}

        return count($vehicles)+count($spares);
    }

    public function getName()
    {
        return 'TwigExtensions';
    }
}