<?php
namespace Sayara\FrontendBundle\Controller;

use Sayara\BackendBundle\Entity\User;
use Sayara\FrontendBundle\Form\RegistrationType;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Form\Factory\FactoryInterface;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\RegistrationController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use Sayara\FrontendBundle\Service\FileUploader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class RegistrationController
 * @package Sayara\FrontendBundle\Controller
 */
class RegistrationController extends BaseController
{
    private $fileUploader;
    private $userManager;
    private $eventDispatcher;
    private $formFactory;
    private $token;

    public function __construct(EventDispatcherInterface $eventDispatcher,
                                FactoryInterface $formFactory,
                                UserManagerInterface $userManager,
                                TokenStorageInterface $tokenStorage,
                                 FileUploader $fileUploader)
    {
        parent::__construct($eventDispatcher, $formFactory, $userManager, $tokenStorage);
        $this->userManager=$userManager;
        $this->formFactory=$formFactory;
        $this->eventDispatcher=$eventDispatcher;
        $this->token= $tokenStorage;
        $this->fileUploader= $fileUploader;
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function registerAction(Request $request)
    {
        $userManager = $this->userManager;
        $dispatcher = $this->eventDispatcher;

        $user = $userManager->createUser();
        $user->setEnabled(true);


        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $this->createForm(RegistrationType::class,$user);

        $form->setData($user);
        $session= $request->getSession();




        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $data= $form->getData();

                $file= $data->getPhoto()->getPath();
                if($file!== null){
                    $fileName = $this->fileUploader->upload($file);

                    $data->getPhoto()->setPath($fileName);
                    $data->getPhoto()->setName("autoplus" . $user->getUsername());
                }else{
                    $data->setPhoto(null);
                }
                $email = $data->getEmail();
                $username= $data->getUsername();
                $userEmail = $this->getDoctrine()->getManager()->getRepository(User::class)->findByEmailCanonical($email);
                $userUsername = $this->getDoctrine()->getManager()->getRepository(User::class)->findByUsername($username);
                if(null != $userEmail){
                    $session->getFlashBag()->add('error', 'Vous êtes déjà inscrit avec cet email !');
                    return $this->render('@FOSUser/Registration/register.html.twig', array(
                        'form' => $form->createView(),
                    ));
                }elseif (null != $userUsername){
                    $session->getFlashBag()->add('warning', 'Vous êtes déjà inscrit avec cet username !');
                    return $this->render('@FOSUser/Registration/register.html.twig', array(
                        'form' => $form->createView(),
                    ));
                }
                $event = new FormEvent($form, $request);
                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);
                //$user->setEnabled(true);

                $userManager->updateUser($user);

                if (null === $response = $event->getResponse()) {
                    $url = $this->generateUrl('fos_user_registration_confirmed');
                    $response = new RedirectResponse($url);
                }

                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

                return $response;
            }

            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_FAILURE, $event);

            if (null !== $response = $event->getResponse()) {
                return $response;
            }
        }

        return $this->render('@FOSUser/Registration/register.html.twig', array(
            'form' => $form->createView(),
        ));
    }

}