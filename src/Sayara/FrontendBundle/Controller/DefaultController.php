<?php

namespace Sayara\FrontendBundle\Controller;

use Cocur\Slugify\Slugify;
use Doctrine\Common\Collections\ArrayCollection;
use Sayara\BackendBundle\Entity\City;
use Sayara\BackendBundle\Entity\ImageIn;
use Sayara\BackendBundle\Entity\ImageOut;
use Sayara\BackendBundle\Entity\Marque;
use Sayara\BackendBundle\Entity\Modele;
use Sayara\BackendBundle\Entity\NewCar;
use Sayara\BackendBundle\Entity\Photo;
use Sayara\BackendBundle\Entity\Version;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{

    /**
     * Returns a JSON string with the models of the Marque with the providen id.
     * @Route("/vehicle-list-models", name="vehicle_model_list")
     * @Method({"GET","POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function listModelsOfMarqueAction(Request $request)
    {

        // Get Entity manager and repository
        $em = $this->getDoctrine()->getManager();
        $modelsRepository = $em->getRepository(Modele::class);

        $models = $modelsRepository->createQueryBuilder("q")
            ->where("q.marque = :marqueid")
            ->setParameter("marqueid", $request->get("marqueid"))
            ->getQuery()
            ->getResult();

        // Serialize into an array the data that we need, in this case only name and id
        // Note: you can use a serializer as well, for explanation purposes, we'll do it manually
        $responseArray = array();
        foreach($models as $model){
            $responseArray[] = array(
                "id" => $model->getId(),
                "libelle" => $model->getLibelle()
            );
        }

        // Return array with structure of the models of the providen marque id
        return new JsonResponse($responseArray);

    }

    /**
     * @Route("/", name="home_page")
     */
    public function indexAction()
    {
        /*$em=$this->getDoctrine()->getManager();
        $regions=$em->getRepository("AutoplusAnnonceBundle:Region")->findAll();
        $marques=$em->getRepository("AutoplusAnnonceBundle:Marque")->findAll();
        $vehicules=$em->createQuery("SELECT u FROM Autoplus\AnnonceBundle\Entity\VehiculeNeuf u")
            ->setMaxResults(6)->getResult();
        $nouvellesannonces=$em->createQuery("SELECT u FROM Autoplus\AnnonceBundle\Entity\Annonce u WHERE u.activer = 1 ORDER BY u.dateCreation DESC ")
            ->setMaxResults(4)->getResult();
        $annoncebonprix=$em->createQuery("SELECT u FROM Autoplus\AnnonceBundle\Entity\Annonce u WHERE u.activer = 1 AND u.prixAnnonce < 30000 ORDER BY u.dateCreation DESC ")
            ->setMaxResults(4)->getResult();
        */
        $em = $this->getDoctrine()->getManager();

        $newCars = $em->createQuery("SELECT c FROM BackendBundle:NewCar c")
            ->setMaxResults(6)->getResult();
        foreach ($newCars as $car){
            $photos= $em->getRepository(Photo::class)->findByNewCar($car);
            foreach ($photos as $photo){
                $car->addPhoto($photo);
            }

        }
        $newApplications = $em->createQuery("SELECT u FROM BackendBundle:Vehicle u WHERE u.isPublic = 1 ORDER BY u.createdAt DESC ")
            ->setMaxResults(4)->getResult();
        foreach ($newApplications as $vehicle){
            $photos= $em->getRepository(Photo::class)->findByVehicle($vehicle);
            foreach ($photos as $photo){
                $vehicle->addPhoto($photo);
            }

        }

        $marques= $this->getDoctrine()->getManager()->getRepository(Marque::class)->findAll();
        $cities=  $this->getDoctrine()->getManager()->getRepository(City::class)->findAll();



        return $this->render('default/index.html.twig', [
            'cars'=> $newCars,
            'features'=>$newApplications,
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'marques'=>$marques,
            'cities'=>$cities,

            //"regions"=>$regions,"marques"=>$marques,"vehicules"=>$vehicules,"annoncesn"=>$nouvellesannonces,"annoncesprix"=>$annoncebonprix
        ]);
    }

    /**
     * Finds and displays a type entity.
     *
     * @Route("/voitures-neuves/categorie/{marque_slug}/voiture/{id}/{voiture_slug}", name="new_car_show")
     * @Method("GET")
     */
    public function showAction(NewCar $newCar)
    {
        //dump($newCar->getImageouts());die();
        $images= new ArrayCollection();
        $photos = $this->getDoctrine()->getManager()->getRepository(ImageIn::class)->findByCar($newCar);
        foreach ($photos as $photo){
            $newCar->addImagein($photo);
            $images->add($photo);
        }
        $photouts = $this->getDoctrine()->getManager()->getRepository(ImageOut::class)->findByCar($newCar);
        foreach ($photouts as $photo){
            $newCar->addImageout($photo);
            $images->add($photo);
        }

        $cars= $this->getDoctrine()->getManager()->getRepository(NewCar::class)->findByMarque($newCar->getMarque());
        foreach ($cars as $car){
            $photos= $this->getDoctrine()->getManager()->getRepository(Photo::class)->findByNewCar($car);
            foreach ($photos as $photo){
                $car->addPhoto($photo);
            }


        }

        //dump($newCar->getVersions());die();
        $versions = $this->getDoctrine()->getManager()->getRepository(Version::class)->findByCar($newCar);

        foreach ($versions as $version){
            $newCar->addVersion($version);

        }
        $sameBrands= $this->getDoctrine()->getManager()->getRepository(NewCar::class)->findBy([
            'marque'=> $newCar->getMarque(),
            'modele'=>$newCar->getModele(),
        ]);
        $sames = new ArrayCollection();
        $imagesBrand= new ArrayCollection();
        foreach ($sameBrands as $sameBrand){
            $photos = $this->getDoctrine()->getManager()->getRepository(ImageIn::class)->findByCar($sameBrand);
            foreach ($photos as $photo){
                $sameBrand->addImagein($photo);
                $imagesBrand->add($photo);
            }
            $photouts = $this->getDoctrine()->getManager()->getRepository(ImageOut::class)->findByCar($sameBrand);
            foreach ($photouts as $photo){
                $sameBrand->addImageout($photo);
                $imagesBrand->add($photo);
            }
            if($sameBrand->getId()!= $newCar->getId()){

                $sames->add($sameBrand);
            }
        }
        $version_count = count($versions);
        $slugify = new Slugify();

        if($version_count == 1){
            // {'version': version.id , marque_slug : car.marque.libelle|slugify,voiture_slug : car.modele.libelle|slugify}
            return $this->redirectToRoute('new_car_version_show',
                array(
                    'version' => $versions[0]->getId(),
                    'marque_slug' =>     $slugify->slugify($newCar->getMarque()->getLibelle())  ,
                    'voiture_slug' =>    $slugify->slugify($newCar->getModele()->getLibelle() )
                )
            );
        }
     return $this->render('vehicle/show_new_car.html.twig', array(
            'car' => $newCar,
            'cars'=>$cars,
            'images'=>$images,
            'sames'=>$sames,
            'imagesBrand'=>$imagesBrand
        ));
    }

    /**
     * @param Request $request
     * @param Version $version
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/voitures-neuves/categorie/{marque_slug}/voiture/{voiture_slug}/version/{version}", name="new_car_version_show")
     * @Method("GET")
     */
    public function showCarVersion(Request $request, Version $version){
        $vehicle = $this->getDoctrine()->getManager()->getRepository(NewCar::class)->find($version->getCar());
        $photos= $this->getDoctrine()->getManager()->getRepository(Photo::class)->findByNewCar($vehicle);
        return $this->render('vehicle/_show_car_version.html.twig', array(
            'photos' => $photos,
            'version' => $version
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/searchSpare", name="search_spare_form")
     * @Method("GET")
     */
    public function createSearchSpareFormAction(Request $request){


        return $this->render('default/_form_spare_search.html.twig', array(
        ));

    }
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/searchOccasion", name="search_occasion_form")
     * @Method("GET")
     */
    public function createSearchOccasionFormAction(Request $request){



        return $this->render('default/_form_occasion_search.html.twig', array(
        ));

    }
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/searchNew", name="search_new_form")
     * @Method("GET")
     */
    public function createSearchNewFormAction(Request $request){

        return $this->render('default/_form_new_search.html.twig', array(
        ));

    }

    /**
     * @param Request $request
     * @Route("/voitures-neuves/{marque}/categorie/{slug}", name="new_car_page_index2")
     * @Method("GET")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getNewCarsAction(Request $request, Marque $marque, $slug ){

        $newCars= $this->getDoctrine()->getManager()->getRepository(NewCar::class)->findByMarque($marque);

        $imagesBrand= new ArrayCollection();
        foreach ($newCars as $car){
            $photos = $this->getDoctrine()->getManager()->getRepository(ImageIn::class)->findByCar($car);
            foreach ($photos as $photo){
                $car->addImagein($photo);
                $imagesBrand->add($photo);
            }
            $photouts = $this->getDoctrine()->getManager()->getRepository(ImageOut::class)->findByCar($car);
            foreach ($photouts as $photo){
                $car->addImageout($photo);
                $imagesBrand->add($photo);
            }

        }

        $paginator=$this->get('knp_paginator');
        $cars=$paginator->paginate(
            $newCars,
            $request->query->get('page', 1),
            $request->query->get('limit',6)
        );
        return $this->render('vehicle/index_new_car.html.twig', array(
            'marque' => $marque,
            'cars' => $cars,
            'images'=>$imagesBrand
        ));
    }

    /**
     * @param Request $request
     * @Route("/voitures-neuves/", name="new_car_page_index")
     * @Method("GET")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getCarsListAction(Request $request){

        $marques= $this->getDoctrine()->getManager()->getRepository(Marque::class)->findAll();
        return $this->render('vehicle/list_car_categories.html.twig', array(
            'marques' => $marques
        ));
    }






}
