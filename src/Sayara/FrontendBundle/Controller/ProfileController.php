<?php

namespace Sayara\FrontendBundle\Controller;

use FOS\UserBundle\Controller\ProfileController as BaseProfileController;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Form\Factory\FactoryInterface;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use Sayara\FrontendBundle\Form\ProfileType;
use Sayara\FrontendBundle\Service\FileUploader;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


class ProfileController extends BaseProfileController
{

    private $eventDispatcher;

    private $formFactory;

    private $userManager;

    private $fileUploader;

    public function __construct(EventDispatcherInterface $eventDispatcher,
                                FactoryInterface $formFactory,
                                UserManagerInterface $userManager,
                                FileUploader $fileUploader)
    {
        parent::__construct($eventDispatcher, $formFactory, $userManager);
        $this->eventDispatcher = $eventDispatcher;
        $this->fileUploader = $fileUploader;
        $this->formFactory = $formFactory;
        $this->userManager = $userManager;
    }


    /**
     * Show the user.
     */
    public function profileAction(Request $request)
    {
        $user = $this->getUser();
        $username= $user->getUsername();
        $email= $user->getEmail();
        $firstname= $user->getFirstname();
        $lastname= $user->getLastname();
        $address= $user->getAddress();
        $phone= $user->getPhone();
        $gender= $user->getGender();
        
        $applications=0;
        $spares=0;

        if($user->getVehicles() !== null){

            $applications = $user->getVehicles()->count();

        }
        if($user->getSpares()!== null){
            $spares= $user->getSpares()->count();
        }

        $photo= null;
        if (null !== $user->getPhoto()) {
            $photo = $user->getPhoto()->getPath();
        }

        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $dispatcher = $this->eventDispatcher;

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $this->createForm(ProfileType::class, $user);
        $form->setData($user);
        $user->setAcceptCondition(true);
        $form->handleRequest($request);

        $session= $request->getSession();
        $session->getFlashBag()->setName('profile');

        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();
            $data->setGender($gender);

            $file = $data->getPhoto()->getPath();

            if ($file !== null) {



                $fileName = $this->fileUploader->upload($file);

                $data->getPhoto()->setPath($fileName);
                $data->getPhoto()->setName("autoplus" . $user->getUsername());

                $session->getFlashBag()->add('success', 'Photo de profil modifiée avec success !');

            } else {

                $data->getPhoto()->setPath($photo);

            }

            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_SUCCESS, $event);
            $edited_username= $data->getUsername();
            $edited_email= $data->getEmail();
            $edited_firstname= $data->getFirstname();
            $edited_lastname= $data->getLastname();
            $edited_address= $data->getAddress();
            $edited_phone= $data->getPhone();

            if(strcmp($edited_username, $username) !== 0){ $session->getFlashBag()->add('success', 'Votre username a été modifiée avec success !'); }
            if(strcmp($edited_email,$email) !== 0){ $session->getFlashBag()->add('success', 'Votre address email a été modifiée avec success !'); }
            if(strcmp($edited_firstname, $firstname) !== 0){ $session->getFlashBag()->add('success', 'Votre firstname a été modifié avec success !'); }
            if(strcmp($edited_lastname, $lastname) !== 0){ $session->getFlashBag()->add('success', 'Votre lastname a été modifié avec success !'); }
            //if(strcmp($edited_address, $address) !== 0){ $session->getFlashBag()->add('success', 'Votre address a été modifiée avec success !'); }
            if(strcmp($edited_phone, $phone) !== 0){ $session->getFlashBag()->add('success', 'Votre numéro de téléphone a été modifiée avec success !'); }


            $this->userManager->updateUser($user);
            if (null === $response = $event->getResponse()) {
                $url = $this->generateUrl('fos_user_profile_show');
                $response = new RedirectResponse($url);
            }

            //$dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_COMPLETED, new FilterUserResponseEvent($user, $request, $response));
            //dump($session->getFlashBag());die();
            return $response;
        }
        return $this->render('@FOSUser/Profile/show.html.twig', array(
            'user' => $user,
            'nbApp'=> $applications,
            'nbSpares'=>$spares,
            'form' => $form->createView(),
        ));
    }

}

