<?php

namespace Sayara\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class PageController extends Controller
{

    /**
     * @Route("/conditions/", name="condition-page")
     */
    public function conditionPageAction()
    {
        return $this->render('page/condition.html.twig',[]);
    }

    /**
     * @Route("/about-us/", name="about-page")
     */
    public function aboutUsPageAction()
    {
        return $this->render('page/about-us.html.twig',[]);
    }

    /**
     * @Route("/contact-us/", name="contact-page")
     */
    public function contactUsPageAction()
    {
        return $this->render('page/contact-us.html.twig',[]);
    }

    /**
     * @Route("/policy/", name="policy-page")
     */
    public function privacyPolicyPageAction()
    {
        return $this->render('page/policy.html.twig',[]);
    }


    /**
     * @Route("/faq/", name="faq-page")
     */
    public function faqPageAction()
    {
        return $this->render('page/faq.html.twig',[]);
    }
}
