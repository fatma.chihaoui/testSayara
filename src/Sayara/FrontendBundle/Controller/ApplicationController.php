<?php

namespace Sayara\FrontendBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Entity;
use Sayara\BackendBundle\Entity\Application;
use Sayara\BackendBundle\Entity\Category;
use Sayara\BackendBundle\Entity\Marque;
use Sayara\BackendBundle\Entity\Modele;
use Sayara\BackendBundle\Entity\Photo;
use Sayara\BackendBundle\Entity\Spare;
use Sayara\BackendBundle\Entity\Type;
use Sayara\BackendBundle\Entity\Vehicle;
use Sayara\BackendBundle\Form\SpareType;
use Sayara\BackendBundle\Form\VehicleType;
use Sayara\FrontendBundle\Form\ApplicationType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UploaderEvent;

/**
 * Application controller.
 *
 * @Route("annonce")
 */
class ApplicationController extends Controller
{
    /**
     * Lists all application entities.
     *
     * @Route("/", name="application_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $applications = $em->getRepository('BackendBundle:Application')->findAll();

        return $this->render('application/index.html.twig', array(
            'applications' => $applications,
        ));
    }
    /**
     * Lists all spare entities.
     *
     * @Route("/piece-de-rechange", name="spare_index")
     * @Method("GET")
     */
    public function indexSpareByUserAction()
    {
        $em = $this->getDoctrine()->getManager();

        $vehicles = $em->getRepository(Vehicle::class)->findByUser($this->getUser());
        $spares = $em->getRepository('BackendBundle:Spare')->findByUser($this->getUser());


        foreach ($spares as $spare){
            $photos = $em->getRepository(Photo::class)->findBySpare($spare->getId());
            foreach ($photos as $photo){
                $spare->addPhoto($photo);

            }
        }

        return $this->render('spare/index.html.twig', array(
            'spares' => $spares,
            'nbApp'=> count($vehicles),
            'nbSpares'=>count($spares)
        ));
    }

    /**
     * Creates a new spare entity.
     *
     * @Route("/piece-de-rechange/nouveau", name="spare_new")
     * @Method({"GET", "POST"})
     */
    public function newSpareAction(Request $request)
    {
        $spare = new Spare();
        $form = $this->createForm(SpareType::class, $spare);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $spareCateg= $this->getDoctrine()->getManager()->getRepository(Category::class)->findOneByName('Piéces de rechange');
            $spare->setCategory($spareCateg);

            $em = $this->getDoctrine()->getManager();
            $spare->setType(3);
            $created = new \DateTime('now');
            $spare->setExpiredAt($created->modify('+1 month'));
            $spare->setUser($this->getUser());
            $em->persist($spare);
            $em->flush();

            return $this->redirectToRoute('spare_photo_edition', array('id' => $spare->getId()));
        }

        return $this->render('spare/new.html.twig', array(
            'spare' => $spare,
            'form' => $form->createView(),
        ));
    }

    /**
     *
     * @Method({"GET", "POST"})
     * @Route("/piece-de-rechange/image/send/{id}", name="spare_photo_send")
     */
    public function spareImageSendAction(Request $request, $id)
    {

        $photo = new Photo();
        $media = $request->files->get('file');

        $spare = $this->getDoctrine()->getManager()->getRepository(Spare::class)->find($id);

        $photo->setFile($media);


        $photo->setPath($media->getPathName());
        $photo->setName($media->getClientOriginalName());


        $photo->setSpare($spare);
        $spare->addPhoto($photo);

        $photo->upload();
        $em = $this->getDoctrine()->getManager();
        $em->persist($photo);
        $em->flush();


        if ($spare->getPhotos() !== null){
            $photos= $spare->getPhotos();
        }
        return new JsonResponse(array('success' => true, 'media' => $media, 'photos'=>$photos ));
    }

    /**
     * Deletes a spare entity.
     *
     * @Route("/piece-de-rechange/delete/", name="spare_delete")
     * @Method("POST")
     */
    public function deleteSpareAction(Request $request)
    {
        $id=$request->get("id");
        $em = $this->getDoctrine()->getManager();
        $spare =$em->getRepository(Spare::class)->find($id);

        //$vehicle->setActive(false);
        $em->remove($spare);
        $em->flush();

        return new JsonResponse(array('success' => true, 'spare' => $spare ));

    }

    /**
     *
     * @Route("/piece-de-rechange/addPhotos/{id}", name="spare_application_photo")
     * @Method({"GET", "POST"})
     */
    public function addPhotoSpareApplication(Request $request,Spare $spare){


        $session= $request->getSession();
        $session->getFlashBag()->add('success', 'Notre équipe éditoriale va maintenant réviser votre annonce et la publiera sur Sayara.tn !');
        return $this->render('application/add_spare_photos.html.twig', array(
            'id'=>$spare->getId(),'title'=>$spare->getTitle()


        ));

    }
    /**
     * Finds and displays a spare entity.
     *
     * @Route("/piece-de-rechange/{id}", name="spare_show")
     * @Method("GET")
     */
    public function showSpareAction(Spare $spare)
    {
        $em = $this->getDoctrine()->getManager();
        $photos = $em->getRepository(Photo::class)->findBySpare($spare);


        return $this->render('spare/show.html.twig', array(
            'spare' => $spare,
            'photos'=>$photos
        ));
    }
    /**
     * Displays a form to edit an existing spare entity.
     *
     * @Route("/piece-de-rechange/{id}/modifier", name="spare_edit")
     * @Method({"GET", "POST"})
     */
    public function editSpareAction(Request $request, Spare $spare)
    {

        $editForm = $this->createForm('Sayara\BackendBundle\Form\SpareType', $spare);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $spare->setType(3);
            $spare->setIsPublic(false);
            $this->getDoctrine()->getManager()->flush();

            //return $this->redirectToRoute('spare_photo_edition', array('id' => $spare->getId()));
            return $this->redirectToRoute('spare_index');
        }

        return $this->render('application/edit.html.twig', array(
            'spare' => $spare,
            'form_edit' => $editForm->createView(),
            'type'=>3
        ));
    }

    /**
     *
     * @Route("/piece-de-rechange/images/{id}", name="spare_photo_edition")
     * @Method({"GET", "POST"})
     */
    public function editSparePhotoApplication(Request $request, Spare $spare,$id){


        return $this->render('application/edit_spare_photos.html.twig', array(
            'id'=>$spare->getId(),'title'=>$spare->getTitle()


        ));
    }
    /**
     *
     * @Method({"GET", "POST"})
     * @Route("/piece-de-rechange/uploaded/receive/", name="get_uploaded_spare_photos")
     */

    public function getUploadedSparePhotos(Request $request){

        $id= $request->get('id');
        $spare = $this->getDoctrine()->getManager()->getRepository(Spare::class)->find($id);
        $photos= $this->getDoctrine()->getManager()->getRepository(Photo::class)->findBySpare($spare);


        $photosData= [];
        foreach ($photos as $photo){
            $photosData[] =['name' => $photo->getName()];
        }

        return new JsonResponse(array('success' => true, 'photos' => $photosData ));
    }


    /**
     * Finds and displays a vehicle entity.
     *
     * @Route("/{id}", name="vehicle_show")
     * @Method("GET")
     */
    public function showApplicationVehicleAction(Vehicle $vehicle)
    {

        $em = $this->getDoctrine()->getManager();
        $photos = $em->getRepository(Photo::class)->findByVehicle($vehicle);
        return $this->render('vehicle/show.html.twig', array(
            'vehicle' => $vehicle,
            'photos'=>$photos,
        ));
    }

    /**
     * Lists all application entities.
     *
     * @Route("/mes-annonces/", name="user_vehicles")
     * @Method("GET")
     */
    public function applicationsByUserAction()
    {
        $em = $this->getDoctrine()->getManager();

        $vehicles = $em->getRepository(Vehicle::class)->findByUser($this->getUser());
        $spares = $em->getRepository('BackendBundle:Spare')->findByUser($this->getUser());

        $vehiclePhotos= new ArrayCollection();
        foreach ($vehicles as $vehicle){
            $photos = $em->getRepository(Photo::class)->findByVehicle($vehicle->getId());




       foreach ($photos as $photo){
           $vehicle->addPhoto($photo);

       }
        }
        return $this->render('vehicle/index.html.twig', array(
            'vehicles' => $vehicles,
            'nbSpares'=> count($spares),
            'nbApp'=> count($vehicles)
        ));
    }

    /**
     * Creates a new application entity.
     *
     * @Route("/nouveau/{type}/{slug}", name="application_new", requirements={"type"="\d+"})
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, $type)
    {

        $type= $request->attributes->get('type');
        $application = new Application();
        $vehicle= new Vehicle();

        $categoryMoto= $this->getDoctrine()->getManager()->getRepository(Category::class)->findOneByName('Motos');
        $categoryVoiture= $this->getDoctrine()->getManager()->getRepository(Category::class)->findOneByName('Voitures');


        $form = $this->createForm(VehicleType::class, $vehicle);
        $formVehicle = $this->createForm(VehicleType::class, $vehicle);
        $form->handleRequest($request);


        $marques= $this->getDoctrine()->getManager()->getRepository(Marque::class)->findAll();

        if ($form->isSubmitted() && $form->isValid()) {


            $em = $this->getDoctrine()->getManager();
            $vehicle->setType($type);
            if($type == 1){
                $vehicle->setCategory($categoryMoto);
            }elseif ($type==2){
                $vehicle->setCategory($categoryVoiture);
            }elseif ($type==3){
                $vehicle->setCategory($categoryVoiture);
            }

            $created = new \DateTime('now');
            $vehicle->setExpiredAt($created->modify('+1 month'));

            $vehicle->setUser($this->getUser());
            $em->persist($vehicle);
            $em->flush();

            return $this->redirectToRoute('application_photo_edition', array('id' => $vehicle->getId()));
        }

        return $this->render('application/new.html.twig', array(
            'application' => $application,
            'marques'=>$marques,
            'form' => $form->createView(),
            'form_vehicle'=> $formVehicle->createView(),
            'type'=>$type
        ));
    }
    /**
     * Displays a form to edit an existing application entity.
     *
     * @Route("/{id}/modifier/{type}", name="application_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Vehicle $vehicle)
    {

        $type= $request->attributes->get('type');
        $editForm = $this->createForm(VehicleType::class, $vehicle);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $vehicle->setIsPublic(false);
            $this->getDoctrine()->getManager()->flush();

            //return $this->redirectToRoute('application_photo_edition', array('id' => $vehicle->getId()));
            return $this->redirectToRoute('user_vehicles');
        }

        return $this->render('application/edit.html.twig', array(
            'vehicle' => $vehicle,
            'edit_form' => $editForm->createView(),
            'type'=>$type
        ));
    }

    /**
     *
     * @Route("/images/{id}", name="application_photo_edition")
     * @Method({"GET", "POST"})
     */
    public function editPhotoApplication(Request $request, Vehicle $vehicle,$id){


        return $this->render('application/edit_photos.html.twig', array(
            'id'=>$vehicle->getId(),'title'=>$vehicle->getTitle(),'type'=>$vehicle->getType()


        ));
    }

    /**
     *
     * @Route("/addPhotos/{id}", name="application_photo")
     * @Method({"GET", "POST"})
     */
    public function addPhotoApplication(Request $request,Vehicle $vehicle){


        $request->getSession()->getFlashBag()->add('success', 'Notre équipe éditoriale va maintenant réviser votre annonce et la publiera sur Sayara.tn !');
        return $this->render('application/add_photos.html.twig', array(
            'id'=>$vehicle->getId(),'title'=>$vehicle->getTitle(), 'type'=>$vehicle->getType()


        ));

    }


    /**
     *
     * @Method({"GET", "POST"})
     * @Route("/ajax/snippet/uploaded/receive/", name="get_uploaded_photos")
     */

    public function getUploadedPhotos(Request $request){

        $id= $request->get('id');
        $vehicle = $this->getDoctrine()->getManager()->getRepository(Vehicle::class)->find($id);
        $photos= $this->getDoctrine()->getManager()->getRepository(Photo::class)->findByVehicle($vehicle);


        $photosData= [];
        foreach ($photos as $photo){
            $photosData[] =['name' => $photo->getName()];
        }

        return new JsonResponse(array('success' => true, 'photos' => $photosData ));
    }

    /**
     *
     * @Method({"GET", "POST"})
     * @Route("/ajax/snippet/image/send/{id}", name="ajax_snippet_photo_send")
     */
    public function ajaxSnippetImageSendAction(Request $request, $id)
    {

        $photo = new Photo();
        $media = $request->files->get('file');

        $vehicle = $this->getDoctrine()->getManager()->getRepository(Vehicle::class)->find($id);

        $photo->setFile($media);


        $photo->setPath($media->getPathName());
        $photo->setName($media->getClientOriginalName());


        $photo->setVehicle($vehicle);
        $vehicle->addPhoto($photo);

        $photo->upload();
        $em = $this->getDoctrine()->getManager();
        $em->persist($photo);
        $em->flush();

        if ($vehicle->getPhotos() !== null){
            $photos= $vehicle->getPhotos();
        }
        return new JsonResponse(array('success' => true, 'media' => $media, 'photos'=>$photos ));
    }

    /**
     * @Route("/delete-uploaded-photo", name="delete_uploaded_photo")
     * @Method({"GET", "POST"})
     */
    public function deleteUploadedPhotoAction(Request $request){
        $file= $request->get('deleted_photo');

        $em= $this->getDoctrine()->getManager();
        $photo= $em->getRepository(Photo::class)->findOneBy(['name'=>$file]);
        $toDelete= $em->getRepository(Photo::class)->findOneBy(['id'=>$photo->getId()]);


        $em->remove($toDelete);
        $em->flush();

       return new JsonResponse(array('success' => true, 'photo' => $toDelete->getName() ));
    }


    /**
     * Finds and displays a application entity.
     *
     * @Route("/{id}", name="application_show")
     * @Method("GET")
     */
    public function showAction(Application $application)
    {
        $deleteForm = $this->createDeleteForm($application);

        return $this->render('application/show.html.twig', array(
            'application' => $application,
            'delete_form' => $deleteForm->createView(),
        ));
    }



    /**
     * Deletes a application entity.
     *
     * @Route("/delete/", name="vehicle_delete")
     * @Method("POST")
     */
    public function deleteAction(Request $request)
    {
        $id = $request->get("id");
        $em = $this->getDoctrine()->getManager();
        $vehicle = $em->getRepository(Vehicle::class)->find($id);
        $em->remove($vehicle);
        $em->flush();

        return new JsonResponse(array('success' => true, 'vehicle' => $vehicle));
    }

}
