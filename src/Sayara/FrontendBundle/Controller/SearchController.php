<?php

namespace Sayara\FrontendBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Query;
use Sayara\BackendBundle\Entity\Address;
use Sayara\BackendBundle\Entity\Category;
use Sayara\BackendBundle\Entity\City;
use Sayara\BackendBundle\Entity\Marque;
use Sayara\BackendBundle\Entity\Modele;
use Sayara\BackendBundle\Entity\Photo;
use Sayara\BackendBundle\Entity\Spare;
use Sayara\BackendBundle\Entity\User;
use Sayara\BackendBundle\Entity\Vehicle;
use Sayara\BackendBundle\Form\SearchSpareType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;


/**
 * Class SearchController
 * @package Sayara\FrontendBundle\Controller
 * @Route("/annonces")
 */
class SearchController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/", name="search_index")
     * @Method("GET")
     */
    public function indexAction(Request $request){

        $marques= $this->getDoctrine()->getManager()->getRepository(Marque::class)->findAll();
        $categories= $this->getDoctrine()->getManager()->getRepository(Category::class)->findAll();
        $cities=  $this->getDoctrine()->getManager()->getRepository(City::class)->findAll();

        $marque_id = $request->query->get('marque');
        $modele_id = $request->query->get('modele');
        $minp = $request->query->get('minp');
        $maxp = $request->query->get('maxp');
        $city = $city_id = $request->query->get('city');
        $type = $request->query->get('type');
        if($request->isXmlHttpRequest()) {
            $em= $this->getDoctrine()->getManager();
            $queryBuilder = $em->createQueryBuilder();

            //$vehicles ;
            $q = $queryBuilder->select('v');
            if($type == 1 || !$type){

                $q = $q->from('BackendBundle:Vehicle','v');
            } else  if($type == 2){
                $q = $q->from('BackendBundle:Spare','v');
            }
            $q = $q->where('v.isPublic = 1');
            if($city_id){
                $q = $q->leftJoin('v.city' ,'c')->where('c.id >= :city ')->setParameter('city',$city_id);
            }


            if($marque_id){
                $q = $q->andWhere('v.marque = :marque_id');
                    $q = $q->setParameter('marque_id',$marque_id);
            }

            if($modele_id){
                $q = $q->andWhere('v.modele = :modele_id')
                    ->setParameter('modele_id',$modele_id);

            }
            if($minp){
                $q =  $q->andWhere('v.price >= :min ')
                    ->setParameter('min',$minp);
            }

            if($maxp){
                $q =$q->andWhere('v.price <= :max ')
                    ->setParameter('max',$maxp);
            }


            $vehicles = $q ->getQuery() ->getResult();

            $all= new ArrayCollection();

            foreach ($vehicles as $vehicle){
                if($type == 1 || !$type){
                    $photos = $em->getRepository(Photo::class)->findByVehicle($vehicle);

                } else  if($type == 2){
                    $photos = $em->getRepository(Photo::class)->findBySpare($vehicle);
                }


                foreach ($photos as $photo){
                    $vehicle->addPhoto($photo);
                }
                $all->add($vehicle);
            }

            if(count($all) == 0) {
                return $this->render('application/not_data.twig.html');
            }else {
                return $this->render('application/all.html.twig',['all'=>$all]);
            }


        } else {
            return $this->render('application/search_application.html.twig',
                [
                    'type' => $type,
                    'marque_id'=>$marque_id,
                    'modele_id'=>$modele_id,
                    'minp'=>$minp,
                    'maxp'=>$maxp,
                    'city'=> $city,
                    'categories'=>$categories,
                    'cities'=>$cities,
                    'marques'=>$marques
                ]);;
        }

    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/categorie/{code}/{slug}", name="search_category")
     * @Method("GET")
     */
    public function searchByCategoryAction(Request $request){
        $type = $request->query->get('type');
        $categories= $this->getDoctrine()->getManager()->getRepository(Category::class)->findAll();
        $cities = $this->getDoctrine()->getManager()->getRepository(City::class)->findAll();
        return $this->render('application/app_category_list.html.twig',[
            'cities'=>$cities,
            'type'=>$type,
            'code'=>(integer) $request->attributes->get('code'),
            'categories'=>$categories]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/category-code/{code}", name="search_category_index")
     * @Method("GET")
     */
    public function searchByCategoryIndexAction(Request $request, $code){
        $category = $this->getDoctrine()->getManager()->getRepository(Category::class)->findByCode($code);



        $em= $this->getDoctrine()->getManager();

        $all= new ArrayCollection();

        $vehicles= $em->getRepository(Vehicle::class)->findByCategory($category);
        $spares= $em->getRepository(Spare::class)->findByCategory($category);

        foreach ($vehicles as $vehicle){
            $photos = $em->getRepository(Photo::class)->findByVehicle($vehicle);
            foreach ($photos as $photo){
                $vehicle->addPhoto($photo);

            }
            if($vehicle->getIsPublic()){
                $all->add($vehicle);
            }

        }
        foreach ($spares as $spare){
            $photos = $em->getRepository(Photo::class)->findBySpare($spare);
            foreach ($photos as $photo){
                $spare->addPhoto($photo);

            }
            if($spare->getIsPublic()){
                $all->add($spare);
            }

        }
        if(count($spares) == 0&& count($vehicles) == 0 ) {
            return $this->render('application/not_data.twig.html');
        }else {
            return $this->render('application/all.html.twig',['all'=>$all]);
        }
        return $this->render('application/all.html.twig',['all'=>$all]);
    }
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/category-pros/{code}", name="search_category_pro")
     * @Method("GET")
     */
    public function proCategoryAction(Request $request,$code)
    {

        $em = $this->getDoctrine()->getManager();

        $appPros = new ArrayCollection();
        $all = $em->createQuery("SELECT u FROM BackendBundle:User u WHERE u.roles  LIKE '%ROLE_PRO%'");
        $pros = $all->getResult();
        $category = $this->getDoctrine()->getManager()->getRepository(Category::class)->findByCode($code);
        foreach ($pros as $pro) {
            if ($pro->getVehicles() !== null) {
                $applications = $this->getDoctrine()->getManager()->getRepository(Vehicle::class)->findBy(['user' => $pro, 'category' => $category]);
                if ($applications !== []) {
                    foreach ($applications as $application) {
                        $photos = $em->getRepository(Photo::class)->findByVehicle($application);

                        foreach ($photos as $photo) {
                            $application->addPhoto($photo);
                        }
                        $appPros->add($application);
                    }
                }

            }

        }
        foreach ($pros as $pro) {
            if ($pro->getSpares() !== null) {
                $spares = $this->getDoctrine()->getManager()->getRepository(Spare::class)->findBy(['user' => $pro, 'category' => $category]);
                if ($spares !== []) {
                    foreach ($spares as $spare) {
                        $photos = $em->getRepository(Photo::class)->findBySpare($spare);

                        foreach ($photos as $photo) {
                            $spare->addPhoto($photo);
                        }
                        $appPros->add($spare);
                    }
                }

            }

        }
        if (count($appPros) == 0 ) {
            return $this->render('application/not_data.twig.html');
        } else{
            return $this->render('application/all.html.twig', array('all' => $appPros));
         }
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/category-part/{code}", name="search_category_part")
     * @Method("GET")
     */
    public function partCategoryAction(Request $request,$code){

        $em= $this->getDoctrine()->getManager();

        $appParts = new ArrayCollection();
        $all=$em->createQuery("SELECT u FROM BackendBundle:User u WHERE u.roles  LIKE '%ROLE_PARTICULIER%'");
        $parts = $all->getResult();
        $category= $this->getDoctrine()->getManager()->getRepository(Category::class)->findByCode($code);
        foreach ($parts as $part){
            if($part->getVehicles() !== null){
                $applications= $this->getDoctrine()->getManager()->getRepository(Vehicle::class)->findBy(['user'=>$part,'category'=>$category]);
                if($applications !== []){
                    foreach ($applications as $application){
                        $photos = $em->getRepository(Photo::class)->findByVehicle($application);

                        foreach ($photos as $photo) {
                            $application->addPhoto($photo);
                        }
                        $appParts->add($application);
                    }
                }

            }

        }
        foreach ($parts as $part){
            if($part->getSpares() !== null){
                $spares= $this->getDoctrine()->getManager()->getRepository(Spare::class)->findBy(['user'=>$part,'category'=>$category]);
                if($spares !== []){
                    foreach ($spares as $spare){
                        $photos = $em->getRepository(Photo::class)->findBySpare($spare);

                        foreach ($photos as $photo) {
                            $spare->addPhoto($photo);
                        }
                        $appParts->add($spare);
                    }
                }

            }

        }

        if (count($appParts) == 0 ) {
            return $this->render('application/not_data.twig.html');
        } else{
            return $this->render('application/all.html.twig', array('all' => $appParts));
        }
    }


    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/all", name="all_index")
     * @Method("GET")
     */
    public function allApplicationAction(Request $request){
        /*
         * `$pros = $queryBuilder->select('u')
                ->from('BackendBundle:User','u')
                ->leftJoin('u.address' ,'a')
                ->where('a.city = :city')
                ->setParameter('city',$city->getName())
                ->andWhere('u.roles LIKE :role')
                ->setParameter('role', '%ROLE_PRO%')
                ->getQuery()
                ->getResult();*/

        $em= $this->getDoctrine()->getManager();

        $all= new ArrayCollection();

        $vehicles= $em->getRepository(Vehicle::class)->findAll();
        $spares= $em->getRepository(Spare::class)->findAll();

        foreach ($vehicles as $vehicle){
            $photos = $em->getRepository(Photo::class)->findByVehicle($vehicle);
            foreach ($photos as $photo){
                $vehicle->addPhoto($photo);

            }
            $all->add($vehicle);
        }
        foreach ($spares as $spare){
            $photos = $em->getRepository(Photo::class)->findBySpare($spare);
            foreach ($photos as $photo){
                $spare->addPhoto($photo);

            }
            $all->add($spare);
        }



        return $this->render('application/all.html.twig', array('all'=>$all));
    }


    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/pros", name="pro_index")
     * @Method("GET")
     */
    public function proApplicationAction(Request $request){

        $em= $this->getDoctrine()->getManager();

        $appPros = new ArrayCollection();
        $all=$em->createQuery("SELECT u FROM BackendBundle:User u WHERE u.roles  LIKE '%ROLE_PRO%'");
        $pros = $all->getResult();
        foreach ($pros as $pro){
            if($pro->getVehicles() !== null){
                $applications= $this->getDoctrine()->getManager()->getRepository(Vehicle::class)->findByUser($pro);
                if($applications !== []){
                    foreach ($applications as $application){
                        $photos = $em->getRepository(Photo::class)->findByVehicle($application);

                        foreach ($photos as $photo) {
                            $application->addPhoto($photo);
                        }
                        $appPros->add($application);
                    }
                }

            }

        }
        foreach ($pros as $pro){
            if($pro->getSpares() !== null){
                $spares= $this->getDoctrine()->getManager()->getRepository(Spare::class)->findByUser($pro);
                if($spares !== []){
                    foreach ($spares as $spare){
                        $photos = $em->getRepository(Photo::class)->findBySpare($spare);

                        foreach ($photos as $photo) {
                            $spare->addPhoto($photo);
                        }
                        $appPros->add($spare);
                    }
                }

            }

        }

        return $this->render('application/all.html.twig', array('all'=>$appPros ));
    }
//requirements={"i"="\d+"}
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/particulars", name="particular_index")
     * @Method("GET")
     */
    public function particularApplicationAction(Request $request){

        $em= $this->getDoctrine()->getManager();

        $appParticulars= new ArrayCollection();
        $all=$em->createQuery("SELECT u FROM BackendBundle:User u WHERE u.roles  LIKE '%ROLE_PARTICULIER%'");
        $particulars = $all->getResult();
        foreach ($particulars as $particular){
            if($particular->getVehicles() !== null){
                $applications= $this->getDoctrine()->getManager()->getRepository(Vehicle::class)->findByUser($particular);
                if($applications !== []){
                    foreach ($applications as $application){
                        $photos = $em->getRepository(Photo::class)->findByVehicle($application);

                        foreach ($photos as $photo) {
                            $application->addPhoto($photo);
                        }
                        $appParticulars->add($application);
                    }
                }

            }

        }

        foreach ($particulars as $particular){
            if($particular->getSpares() !== null){
                $spares= $this->getDoctrine()->getManager()->getRepository(Spare::class)->findByUser($particular);
                if($spares !== []){
                    foreach ($spares as $spare){
                        $photos = $em->getRepository(Photo::class)->findBySpare($spare);
                        foreach ($photos as $photo) {
                            $spare->addPhoto($photo);
                        }
                        $appParticulars->add($spare);
                    }
                }

            }

        }

        return $this->render('application/all.html.twig', array('all'=>$appParticulars));
    }


    /**
     * @param Vehicle $vehicle
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/afficher/{id}/{slug}", name="show_searched_application")
     */
    public function showApplicationAction(Vehicle $vehicle){

        $photos= $this->getDoctrine()->getManager()->getRepository(Photo::class)->findByVehicle($vehicle);

        foreach ($photos as $photo){
            $vehicle->addPhoto($photo);
        }

        return $this->render('vehicle/show.html.twig', array('vehicle'=>$vehicle,'photos'=>$vehicle->getPhotos()));
    }

    /**
     * @param Spare $spare
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/pieces-de-rechange/{id}/{slug}", name="show_searched_spare")
     */
    public function showApplicationSpareAction(Spare $spare, $slug){

            $photos= $this->getDoctrine()->getManager()->getRepository(Photo::class)->findBySpare($spare->getId());

            foreach ($photos as $photo){
                $spare->addPhoto($photo);
            }

        return $this->render('spare/show.html.twig', array('spare'=>$spare,'photos'=>$spare->getPhotos()));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/ville/{code}/{slug}", name="search_city")
     * @Method("GET")
     */
    public function searchByCityAction(Request $request){
        $categories= $this->getDoctrine()->getManager()->getRepository(Category::class)->findAll();
        $type = $request->query->get('type');

        $cities = $this->getDoctrine()->getManager()->getRepository(City::class)->findAll();
        return $this->render('application/app_city_list.html.twig',[
            'categories'=>$categories,
            'type' => $type,
            'cities'=>$cities,
            'code'=>(integer) $request->attributes->get('code')]);
    }
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/city-code/{code}", name="search_city_index")
     * @Method("GET")
     */
    public function searchByCityIndexAction(Request $request, $code){

        $em= $this->getDoctrine()->getManager();
        $city = $em->getRepository(City::class)->findOneByCode($code);

        $usersArray= new ArrayCollection();
        $all= new ArrayCollection();
        $addresses = $em->getRepository(Address::class)->findByCity($city->getName());
        if(null !== $addresses){
            foreach ($addresses as $address){
                $users= $em->getRepository(User::class)->findByAddress($address);
                foreach ($users as $user){
                    $usersArray->add($user);
                }
            }
        }
        foreach ($usersArray as $user){
            $vehicles = $em->getRepository(Vehicle::class)->findByUser($user);
            foreach ($vehicles as $vehicle){
                $photos = $em->getRepository(Photo::class)->findByVehicle($vehicle);
                foreach ($photos as $photo){
                    $vehicle->addPhoto($photo);

                }
                if($vehicle->getIsPublic()){
                    $all->add($vehicle);
                }
            }

            $spares = $em->getRepository(Spare::class)->findByUser($user);
            foreach ($spares as $spare){
                $photos = $em->getRepository(Photo::class)->findBySpare($spare);
                foreach ($photos as $photo){
                    $spare->addPhoto($photo);

                }
                if($spare->getIsPublic()){
                    $all->add($spare);
                }
            }
        }
        if(count($all) == 0 ) {
            return $this->render('application/not_data.twig.html');
        }else {
            return $this->render('application/all.html.twig',['all'=>$all]);
        }
        return $this->render('application/all.html.twig',['all'=>$all]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/city-pro/{code}", name="search_city_pro")
     * @Method("GET")
     */
    public function searchByCityProAction(Request $request, $code){

        $em= $this->getDoctrine()->getManager();
        $queryBuilder = $em->createQueryBuilder();
        $city = $em->getRepository(City::class)->findOneByCode($code);

        $usersArray= new ArrayCollection();
        $all= new ArrayCollection();

        $pros = $queryBuilder->select('u')
                ->from('BackendBundle:User','u')
                ->leftJoin('u.address' ,'a')
                ->where('a.city = :city')
                ->setParameter('city',$city->getName())
                ->andWhere('u.roles LIKE :role')
                ->setParameter('role', '%ROLE_PRO%')
                ->getQuery()
                ->getResult();

            foreach ($pros as $user){

                $usersArray->add($user);
            }


        foreach ($usersArray as $user){
            $vehicles = $em->getRepository(Vehicle::class)->findByUser($user);
            foreach ($vehicles as $vehicle){
                $photos = $em->getRepository(Photo::class)->findByVehicle($vehicle);
                foreach ($photos as $photo){
                    $vehicle->addPhoto($photo);

                }
                $all->add($vehicle);
            }

            $spares = $em->getRepository(Spare::class)->findByUser($user);
            foreach ($spares as $spare){
                $photos = $em->getRepository(Photo::class)->findBySpare($spare);
                foreach ($photos as $photo){
                    $spare->addPhoto($photo);

                }
                $all->add($spare);
            }
        }


        return $this->render('application/all.html.twig',['all'=>$all]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/city-part/{code}", name="search_city_part")
     * @Method("GET")
     */
    public function searchByCityPartAction(Request $request, $code){

        $em= $this->getDoctrine()->getManager();
        $queryBuilder = $em->createQueryBuilder();
        $city = $em->getRepository(City::class)->findOneByCode($code);

        $usersArray= new ArrayCollection();
        $all= new ArrayCollection();

        $pros = $queryBuilder->select('u')
            ->from('BackendBundle:User','u')
            ->leftJoin('u.address' ,'a')
            ->where('a.city = :city')
            ->setParameter('city',$city->getName())
            ->andWhere('u.roles LIKE :role')
            ->setParameter('role', '%ROLE_PARTICULIER%')
            ->getQuery()
            ->getResult();

        foreach ($pros as $user){

            $usersArray->add($user);
        }


        foreach ($usersArray as $user){
            $vehicles = $em->getRepository(Vehicle::class)->findByUser($user);
            foreach ($vehicles as $vehicle){
                $photos = $em->getRepository(Photo::class)->findByVehicle($vehicle);
                foreach ($photos as $photo){
                    $vehicle->addPhoto($photo);

                }
                $all->add($vehicle);
            }

            $spares = $em->getRepository(Spare::class)->findByUser($user);
            foreach ($spares as $spare){
                $photos = $em->getRepository(Photo::class)->findBySpare($spare);
                foreach ($photos as $photo){
                    $spare->addPhoto($photo);

                }
                $all->add($spare);
            }
        }


        return $this->render('application/all.html.twig',['all'=>$all]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/condition/{state}/{slug}", name="search_state")
     * @Method("GET")
     */
    public function searchByStateAction(Request $request){
        $categories= $this->getDoctrine()->getManager()->getRepository(Category::class)->findAll();
        $type = $request->query->get('type');

        $cities = $this->getDoctrine()->getManager()->getRepository(City::class)->findAll();
        return $this->render('application/app_condition_list.html.twig',[
            'cities'=>$cities,
            'type' => $type,
            'state'=>(integer) $request->attributes->get('state'),
            'categories'=>$categories]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/condition-state/{state}", name="search_condition_index")
     * @Method("GET")
     */
    public function searchByStateIndexAction(Request $request, $state){

        $em= $this->getDoctrine()->getManager();
        $all= new ArrayCollection();

        $vehicles= $em->getRepository(Vehicle::class)->findByState($state);
        $spares= $em->getRepository(Spare::class)->findByState($state);

        foreach ($vehicles as $vehicle){
            $photos = $em->getRepository(Photo::class)->findByVehicle($vehicle);
            foreach ($photos as $photo){
                $vehicle->addPhoto($photo);

            }
            if($vehicle->getIsPublic()){
                $all->add($vehicle);
            }
        }
        foreach ($spares as $spare){
            $photos = $em->getRepository(Photo::class)->findBySpare($spare);
            foreach ($photos as $photo){
                $spare->addPhoto($photo);

            }
            if($spare->getIsPublic()){
                $all->add($spare);
            }
        }
        if(count($all) == 0 ) {
            return $this->render('application/not_data.twig.html');
        }else {
            return $this->render('application/all.html.twig',['all'=>$all]);
        }
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/condition-state-pro/{state}", name="search_condition_pro")
     * @Method("GET")
     */
    public function searchByStateProAction(Request $request, $state){

        $em= $this->getDoctrine()->getManager();

        $appPros = new ArrayCollection();
        $all=$em->createQuery("SELECT u FROM BackendBundle:User u WHERE u.roles  LIKE '%ROLE_PRO%'");
        $pros = $all->getResult();
        foreach ($pros as $pro){
            if($pro->getVehicles() !== null){
                $applications= $this->getDoctrine()->getManager()->getRepository(Vehicle::class)->findBy(['user'=>$pro,'state'=>$state ]);
                if($applications !== []){
                    foreach ($applications as $application){
                        $photos = $em->getRepository(Photo::class)->findByVehicle($application);

                        foreach ($photos as $photo) {
                            $application->addPhoto($photo);
                        }
                        $appPros->add($application);
                    }
                }

            }

        }
        foreach ($pros as $pro){
            if($pro->getSpares() !== null){
                $spares= $this->getDoctrine()->getManager()->getRepository(Spare::class)->findBy(['user'=>$pro,'state'=>$state]);
                if($spares !== []){
                    foreach ($spares as $spare){
                        $photos = $em->getRepository(Photo::class)->findBySpare($spare);

                        foreach ($photos as $photo) {
                            $spare->addPhoto($photo);
                        }
                        $appPros->add($spare);
                    }
                }

            }

        }
        return $this->render('application/all.html.twig',['all'=>$appPros]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/condition-state-part/{state}", name="search_condition_part")
     * @Method("GET")
     */
    public function searchByStatePartAction(Request $request, $state){

        $em= $this->getDoctrine()->getManager();

        $appParticulars = new ArrayCollection();
        $all=$em->createQuery("SELECT u FROM BackendBundle:User u WHERE u.roles  LIKE '%ROLE_PARTICULIER%'");
        $particulars = $all->getResult();
        foreach ($particulars as $part){
            if($part->getVehicles() !== null){
                $applications= $this->getDoctrine()->getManager()->getRepository(Vehicle::class)->findBy(['user'=>$part,'state'=>$state ]);
                if($applications !== []){
                    foreach ($applications as $application){
                        $photos = $em->getRepository(Photo::class)->findByVehicle($application);

                        foreach ($photos as $photo) {
                            $application->addPhoto($photo);
                        }
                        $appParticulars->add($application);
                    }
                }

            }

        }
        foreach ($particulars as $part){
            if($part->getSpares() !== null){
                $spares= $this->getDoctrine()->getManager()->getRepository(Spare::class)->findBy(['user'=>$part,'state'=>$state]);
                if($spares !== []){
                    foreach ($spares as $spare){
                        $photos = $em->getRepository(Photo::class)->findBySpare($spare);

                        foreach ($photos as $photo) {
                            $spare->addPhoto($photo);
                        }
                        $appParticulars->add($spare);
                    }
                }

            }

        }
        return $this->render('application/all.html.twig',['all'=>$appParticulars]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/price", name="search_price")
     * @Method({"GET","POST"})
     */
    public function searchByPrice(Request $request){

        $min = $request->get('min_price');
        $max = $request->get('max_price');
        $categories= $this->getDoctrine()->getManager()->getRepository(Category::class)->findAll();

        $cities = $this->getDoctrine()->getManager()->getRepository(City::class)->findAll();
        return $this->render('application/app_price_list.html.twig',[
            'cities'=>$cities,
            'min'=>$min,
            'max'=>$max,
            'categories'=>$categories]);
    }
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/application-price/{min}/{max}", name="search_price_index")
     * @Method("GET")
     */
    public function searchByPriceIndexAction(Request $request){




        $em= $this->getDoctrine()->getManager();
        $queryBuilder = $em->createQueryBuilder();

        $min= intval($request->attributes->get('min'));
        $max= intval($request->attributes->get('max'));

        $all= new ArrayCollection();

        if($min < $max){
        $vehicles = $queryBuilder->select('v')
            ->from('BackendBundle:Vehicle','v')
            ->where('v.price BETWEEN :min AND :max')
            ->setParameter('min',$min)

            ->setParameter('max',$max)
            ->getQuery()
            ->getResult();
        $spares = $queryBuilder->select('s')
            ->from('BackendBundle:Spare','s')
            ->where('s.price BETWEEN :min AND :max')
            ->setParameter('min',$min)

            ->setParameter('max',$max)
            ->getQuery()
            ->getResult();

        foreach ($vehicles as $vehicle){
            $photos = $em->getRepository(Photo::class)->findByVehicle($vehicle);
            foreach ($photos as $photo){
                $vehicle->addPhoto($photo);

            }
            if($vehicle->getIsPublic()){
                $all->add($vehicle);
            }
            $all->add($vehicle);
        }
        foreach ($spares as $spare){
            $photos = $em->getRepository(Photo::class)->findBySpare($spare);
            foreach ($photos as $photo){
                $spare->addPhoto($photo);

            }
            if($spare->getIsPublic()){
                $all->add($spare);
            }
        }}
        if(count($spares) == 0&& count($vehicles) == 0 ) {
            return $this->render('application/not_data.twig.html');
        }else {
            return $this->render('application/all.html.twig',['all'=>$all]);
        }

    }


    /**
     * @Route("/search-engine/spare", name="search_engine_spare")
     * @Method({"GET","POST"})
     */
    public function indexSpareSearchAction(Request $request){
        $marque_id = $request->query->get('marque');
        $modele_id = $request->query->get('modele');
        $minp = $request->query->get('minp');
        $maxp = $request->query->get('maxp');

        $marques= $this->getDoctrine()->getManager()->getRepository(Marque::class)->findAll();

       // $marques= $this->getDoctrine()->getManager()->getRepository(Marque::class)->findAll();
        $form = $this->createFormBuilder();

        $categories= $this->getDoctrine()->getManager()->getRepository(Category::class)->findAll();
        $cities=  $this->getDoctrine()->getManager()->getRepository(City::class)->findAll();
        return $this->render('application/search_application.html.twig',
            [
                'minp' =>$minp,
                'maxp' =>$maxp,
                'marque_id'=> $marque_id,
                'modele_id'=> $modele_id,
                'categories'=>$categories,
                'cities'=>$cities,
                'marques' => $marques]);

    }

















}
