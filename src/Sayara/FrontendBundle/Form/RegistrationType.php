<?php

namespace Sayara\FrontendBundle\Form;
use Sayara\BackendBundle\Form\AddressType;
use Sayara\BackendBundle\Form\PhotoType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationType extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer=new StringToArrayTransformer();

        parent::buildForm($builder, $options);
        $builder
            ->add($builder->create(
                'roles', ChoiceType::class, array(
                'label' => 'Rôle',
                'required' => true,
                'mapped'=> true,
                'multiple' => false,
                'expanded'=> false,
                'choices' => array(
                    'Particulier' => 'ROLE_PARTICULIER',
                    'Proffessionnel' => 'ROLE_PRO',
                )

            ))->addModelTransformer($transformer))
            ->add('acceptCondition')
            ->add('gender',ChoiceType::class,['choices'=>['Homme'=>1,'Femme'=>2]])
            ->add('firstname')->add('lastname')->add('phone')->add('visiblePhone')->remove('updatedAt')
            ->remove('applications')
            ->add('address', AddressType::class,['label'=>false])->add('photo', PhotoType::class,['label'=>false]);

    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }


    public function getBlockPrefix()
    {
        return 'autoplus_form_register';
    }

    public function getName(){

        return $this->getBlockPrefix();
    }
}