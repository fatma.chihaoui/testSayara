<?php

namespace Sayara\FrontendBundle\Form;

use Sayara\BackendBundle\Entity\Type;
use Sayara\BackendBundle\Form\AddressType;
use Sayara\BackendBundle\Form\MarqueType;
use Sayara\BackendBundle\Form\PhotoType;
use Sayara\BackendBundle\Form\TypeType;
use Sayara\BackendBundle\Form\VehicleType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ApplicationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('description')
            ->add('title')
            ->add('createdAt')
            ->add('active')
            ->add('address',AddressType::class,[])
           /* ->add('type',EntityType::class,[
                'class'=> Type::class,
                'choice_label' => 'libelle',
                'placeholder' => '--- Choisissez le type de véhicule ! ---',
            ])*/
            ->add('vehicle',VehicleType::class,[])
            ->add('photos',CollectionType::class,[
                'entry_type'   => PhotoType::class,
                'allow_add'=> true,
            ])
        ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sayara\BackendBundle\Entity\Application',
            'csrf_protection' => false
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'sayara_backendbundle_application';
    }


}
