<?php

namespace Sayara\FrontendBundle\Form;
use Sayara\BackendBundle\Entity\Photo;
use Sayara\BackendBundle\Form\AddressType;
use Sayara\BackendBundle\Form\PhotoType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfileType extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer=new StringToArrayTransformer();

        parent::buildForm($builder, $options);
        $builder
            ->add($builder->create(
                'roles', ChoiceType::class, array(
                'label' => 'Rôle',
                'required' => true,
                'mapped'=> true,
                'multiple' => false,
                'expanded'=> false,
                'choices' => array(
                    'Particulier' => 'ROLE_PARTICULIER',
                    'Proffessionnel' => 'ROLE_PRO',
                )

            ))->addModelTransformer($transformer))
            ->add('gender',ChoiceType::class,['choices'=>['Homme'=>1,'Femme'=>2]])
            ->add('firstname')->add('lastname')->add('phone')->add('visiblePhone')->remove('updatedAt')
            ->remove('applications')
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'options' => array(
                    'translation_domain' => 'FOSUserBundle',
                    'attr' => array(
                        'autocomplete' => 'new-password',
                    ),
                ),
                'first_options' => array('label' => 'form.password'),
                'second_options' => array('label' => 'form.password_confirmation'),
                'invalid_message' => 'fos_user.password.mismatch',
            ))
            ->add('address', AddressType::class,['label'=>false])
            ->add('photo', PhotoType::class,['label'=>false])
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(['csrf_protection' => false]);
    }


    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\ProfileFormType';
    }


    public function getBlockPrefix()
    {
        return 'autoplus_form_profile';
    }

    public function getName(){

        return $this->getBlockPrefix();
    }
}