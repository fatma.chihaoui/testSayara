<?php

namespace Sayara\BackendBundle\Form;

use Sayara\BackendBundle\Entity\NewCar;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VersionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder


            ->add('libelle')
            ->add('disponibility')
            ->add('guarantee',IntegerType::class,['attr'=>['class'=>'form-control']])
            ->add('body',TextType::class,['attr'=>['class'=>'form-control']])
            ->add('nbSit')
            ->add('nbDoor',ChoiceType::class, array(
                'choices'  => array(
                    '2 portes' => 2,
                    '4 portes ' => 4,
                    'plus de 4 portes'=> 5,
                ),'placeholder' => '--- Choisissez ! ---',
            ))
            ->add('nbCylindre')
            ->add('energie', ChoiceType::class, array(
                'choices'  => array(
                    'Diesel ' => 'diesel',
                    'Gaz' => 'gaz',
                    'Essence' => 'essence',
                ),'placeholder' => '--- Choisissez ! ---',
            ))
            ->add('puissanceFiscale')
            ->add('puissance')
            ->add('couple')
            ->add('cylindred')
            ->add('boite')
            ->add('boite',ChoiceType::class, array(
                'choices'  => array(
                    'Manuel' => 'manuel',
                    'Automatique' => 'automatique',
                ),'placeholder' => '--- Choisissez ! ---',
            ))
            ->add('nbRapport')
            ->add('transmission')
            ->add('length')
            ->add('width')
            ->add('height')
            ->add('volumeCoffre')
            ->add('performanceKM')
            ->add('maxSpeed')
            ->add('urbanConsommation')
            ->add('extraUrbanConsommation')
            ->add('mixteConsommation')
            ->add('aBS')
            ->add('airbags')
            ->add('antiPatinage')
            ->add('antiDemarrageElectronic')
            ->add('fixationISOFIX')
            ->add('accoudoirs')
            ->add('autoradio')
            ->add('connectivity')
            ->add('screen')
            ->add('finitionIn')
            ->add('kitSmoking')
            ->add('kitSafety')
            ->add('sellerie')
            ->add('tapis')
            ->add('volant')
            ->add('antiBrouillard')
            ->add('bextvitres')
            ->add('boulonsRoueProtege')
            ->add('feuxPositionLED')
            ->add('jantes')
            ->add('peinture')
            ->add('phares')
            ->add('radarOfParking')
            ->add('allumageAutoDesFeux')
            ->add('climatisation')
            ->add('detecteurDePluie')
            ->add('directionAssiste')
            ->add('fermetureCentralise')
            ->add('ordinateurDeBord')
            ->add('retroExterieur')
            ->add('siegeAvantReglable')
            ->add('regulateurDeVitesse')
            ->add('vitresElectrique')
            ->add('volantReglable')

            ->add('car',EntityType::class,[
                'class'=>NewCar::class,
                'choice_label'=>'title',
                'placeholder' => '--- Choisissez ! ---',
            ]);
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sayara\BackendBundle\Entity\Version'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'sayara_backendbundle_version';
    }


}
