<?php

namespace Sayara\BackendBundle\Form;

use Doctrine\ORM\EntityManagerInterface;
use Sayara\BackendBundle\Entity\Marque;
use Sayara\BackendBundle\Entity\Modele;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewCarType extends AbstractType
{
    private $em;

    /**
     * The Type requires the EntityManager as argument in the constructor. It is autowired
     * in Symfony 3.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')

            ->add('price')
            ->add('description')
            ->add('marque')
            ->add('modele')

            ->add('imageins',CollectionType::class,[
                'entry_type' => ImageInType::class,
                'prototype' => true,
                'allow_add' => true,
                'by_reference' => false,
                'allow_delete'=> true,
                'entry_options' => array(
                    'attr' => array('label' => 'Image intérieures'),
                ),
            ])
            ->add('imageouts',CollectionType::class,[
                'entry_type' => ImageOutType::class,
                'prototype' => true,
                'allow_add' => true,
                'by_reference' => false,
                'allow_delete'=> true,
                'entry_options' => array(
                    'attr' => array('class' => ''),
                ),
            ])
        ;
        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPreSetData'));
        $builder->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'onPreSubmit'));

    }
    protected function addElements(FormInterface $form, Marque $marque = null) {
        // 4. Add the province element
        $form->add('marque', EntityType::class, array(
            'required' => false,
            'data' => $marque,
            'placeholder' => '--- Choisissez la marque ! ---',
            'class' => Marque::class
        ));

        // Modeles empty, unless there is a selected Marque (Edit View)
        $modeles = array();

        // If there is a marque stored in the Vehicle entity, load the modeles of it
        if ($marque) {
            // Fetch Modeles of the Marque if there's a selected marque
            $repoModel = $this->em->getRepository(Modele::class);

            $modeles = $repoModel->createQueryBuilder("q")
                ->where("q.marque = :marqueid")
                ->setParameter("marqueid", $marque->getId())
                ->getQuery()
                ->getResult();

            // Add the Modeles field with the properly data
            $form->add('modele', EntityType::class, array(
                'required' => false,
                'placeholder' => '--- Choisissez le model ! ---',
                'class' => Modele::class,
                'choices' => $modeles
            ));
            //dump($modeles);die();
        }

    }





    function onPreSubmit(FormEvent $event) {
        $form = $event->getForm();
        $data = $event->getData();

        // Search for selected Marque and convert it into an Entity
        $marque = $this->em->getRepository(Marque::class)->find($data['marque']);

        $this->addElements($form, $marque);
    }

    function onPreSetData(FormEvent $event) {
        $vehicle = $event->getData();
        $form = $event->getForm();

        // When you create a new vehicle, the Marque is always empty
        if($vehicle !== null){
            $marque = $vehicle->getMarque() ? $vehicle->getMarque() : null;
            $this->addElements($form, $marque);

        }


    }


    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sayara\BackendBundle\Entity\NewCar',
            'csrf_protection' => false
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'sayara_backendbundle_newcar';
    }


}
