<?php

namespace Sayara\BackendBundle\Form;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Sayara\BackendBundle\Entity\Category;
use Sayara\BackendBundle\Entity\City;
use Sayara\BackendBundle\Entity\Marque;
use Sayara\BackendBundle\Entity\Modele;
use Sayara\BackendBundle\Entity\Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VehicleType extends AbstractType
{
    private $em;

    /**
     * The Type requires the EntityManager as argument in the constructor. It is autowired
     * in Symfony 3.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('nbSit', ChoiceType::class, array(
                'choices'  => array(
                    '2 places' => 2,
                    '4 places' => 4,
                    '5 places' => 5,
                    '6 places' => 6,
                    '8 places' => 8
                ),'placeholder' => '--- Choisissez ! ---',
            ))
            ->add('nbCylindre')
            ->add('power', ChoiceType::class, array(
                'choices'  => array(
                    'Diesel ' => 'diesel',
                    'Gaz' => 'gaz',
                    'Essence' => 'essence',
                ),'placeholder' => '--- Choisissez ! ---',
            ))
            ->add('typeBoite',ChoiceType::class, array(
                'choices'  => array(
                    'Manuel' => 'manuel',
                    'Automatique' => 'automatique',
                ),'placeholder' => '--- Choisissez ! ---',
            ))
            ->add('weight')
            ->add('color')
            ->add('dateOfCirculation',DateType::class,['widget' => 'single_text','html5' => true,'attr' => ['class' => 'js-datepicker'],])
            ->add('nbDoor',ChoiceType::class, array(
                'choices'  => array(
                    '2 portes' => 2,
                    '4 portes ' => 4,
                    'plus de 4 portes'=> 5,
                ),'placeholder' => '--- Choisissez ! ---',
            ))
           /* ->add('category',EntityType::class,[
                'class'=>Category::class,
                'choice_label'=>'name',
                'placeholder' => '--- Choisissez ! ---',
                'data'=>'name'
            ])*/
            ->add('year')
            ->add('fuel')
            ->add('motor')
            ->add('cvdin')
            ->add('transmission')
            ->add('exterior')
            ->add('interior')
            ->add('price')
            ->add('mileage')
            ->add('puissance')
            ->add('description')
            ->add('marque')
            ->add('modele')
            //->add('city')
            ->add('city',EntityType::class,[
                'class'=>City::class,
                'choice_label'=>'name',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c');
                },
                'placeholder' => '--- Choisissez ! ---'
            ])
            ->add('state',ChoiceType::class,[
                'choices'  => array(
                    'Nouveau' => 1,
                    'Occasion' => 2,
                ),'placeholder' => '--- Choisissez ! ---',
            ])
        ;
        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPreSetData'));
        $builder->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'onPreSubmit'));

    }
    protected function addElements(FormInterface $form, Marque $marque = null) {
        // 4. Add the province element
        $form->add('marque', EntityType::class, array(
            'required' => false,
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('m')
                    ->orderBy('m.libelle', 'ASC');
            },
            'placeholder' => '--- Choisissez la marque ! ---',
            'class' => Marque::class
        ));

       /* $form->add('city', EntityType::class, array(
            'required' => false,
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('c')
                    ->orderBy('c.name', 'ASC');
            },
            'placeholder' => '--- Choisissez la marque ! ---',
            'class' => City::class
        ));*/


        // Modeles empty, unless there is a selected Marque (Edit View)
        $modeles = array();

        // If there is a marque stored in the Vehicle entity, load the modeles of it
        if ($marque) {
            // Fetch Modeles of the Marque if there's a selected marque
            $repoModel = $this->em->getRepository(Modele::class);

            $modeles = $repoModel->createQueryBuilder("q")
                ->where("q.marque = :marqueid")
                ->setParameter("marqueid", $marque->getId())
                ->getQuery()
                ->getResult();

            // Add the Modeles field with the properly data
            $form->add('modele', EntityType::class, array(
                'required' => false,
                'placeholder' => '--- Choisissez le model ! ---',
                'class' => Modele::class,
                'choices' => $modeles
            ));
            //dump($modeles);die();
        }

        }





    function onPreSubmit(FormEvent $event) {
        $form = $event->getForm();
        $data = $event->getData();

        // Search for selected Marque and convert it into an Entity
        $marque = $this->em->getRepository(Marque::class)->find($data['marque']);

        $this->addElements($form, $marque);
    }

    function onPreSetData(FormEvent $event) {
        $vehicle = $event->getData();
        $form = $event->getForm();

        // When you create a new vehicle, the Marque is always empty
        if($vehicle !== null){
            $marque = $vehicle->getMarque() ? $vehicle->getMarque() : null;
            $this->addElements($form, $marque);

        }


    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sayara\BackendBundle\Entity\Vehicle',
            'csrf_protection' => false
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'sayara_backendbundle_vehicle';
    }


}
