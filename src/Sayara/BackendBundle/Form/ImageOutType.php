<?php

namespace Sayara\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ImageOutType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('path',FileType::class,[
                'data_class'=>null,
                'attr'=>['class'=>'upload'
                ]]);
        /*->add('path',FileType::class,[
            'multiple'=>true,
            'data_class'=>null,
            'attr'=>['class'=>'upload ',
                'accept'=>'image/*',
                'multiple'=>'multiple'
            ]]);*/
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sayara\BackendBundle\Entity\ImageOut'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'sayara_backendbundle_imageout';
    }


}
