<?php

namespace Sayara\BackendBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Sayara\BackendBundle\Entity\City;

class AppFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {


        $cities= [
            'Ariana',
            'Ben Arous',
            'Bizerte',
            'Beja',
            'Gabes',
            'Gafsa',
            'Jendouba'
            ,'Kairouan'
            ,'Kasserine'
            ,'Kef'
            ,'Kebili'
            ,'La Manouba'
            ,'Mahdia'
            ,'Monastir'
            ,'Medenine'
            ,'Nabeul'
            ,'Sfax'
            ,'Sidi Bouzid'
            ,'Siliana'
            ,'Sousse'
            ,'Tataouine'
            ,'Tozeur'
            ,'Tunis'
            ,'Zaghouan'];

        for ($i=0; $i < count($cities); $i++){
            $city= new City();
            $city->setName($cities[$i]);
            $manager->persist($city);
        }
        $manager->flush();
    }


}