<?php

namespace Sayara\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

use Symfony\Component\HttpFoundation\File\File;
/**
 * Marque
 *
 * @ORM\Table(name="marques")
 * @ORM\Entity()
 * @Vich\Uploadable
 */
class Marque
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message ="Veuillez préciser la marque ")
     * @ORM\Column(name="libelle", type="string", length=255)
     */
    private $libelle;


    /**
     * @ORM\Column(type="datetime",nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="Vehicle", mappedBy="marque")
     */
    private $vehicles;


    /**
     * @ORM\OneToOne(targetEntity="Photo", cascade={"persist"})
     * @ORM\JoinColumn(name="photo_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $photo;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return Marque
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }


    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return Marque
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set photo.
     *
     * @param \Sayara\BackendBundle\Entity\Photo|null $photo
     *
     * @return Marque
     */
    public function setPhoto(\Sayara\BackendBundle\Entity\Photo $photo = null)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo.
     *
     * @return \Sayara\BackendBundle\Entity\Photo|null
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    function __toString()
    {
        return $this->getLibelle();
    }



    /**
     * Set vehicle.
     *
     * @param \Sayara\BackendBundle\Entity\Vehicle|null $vehicle
     *
     * @return Marque
     */
    public function setVehicle(\Sayara\BackendBundle\Entity\Vehicle $vehicle = null)
    {
        $this->vehicle = $vehicle;

        return $this;
    }

    /**
     * Get vehicle.
     *
     * @return \Sayara\BackendBundle\Entity\Vehicle|null
     */
    public function getVehicle()
    {
        return $this->vehicle;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->vehicles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add vehicle.
     *
     * @param \Sayara\BackendBundle\Entity\Vehicle $vehicle
     *
     * @return Marque
     */
    public function addVehicle(\Sayara\BackendBundle\Entity\Vehicle $vehicle)
    {
        $this->vehicles[] = $vehicle;

        return $this;
    }

    /**
     * Remove vehicle.
     *
     * @param \Sayara\BackendBundle\Entity\Vehicle $vehicle
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeVehicle(\Sayara\BackendBundle\Entity\Vehicle $vehicle)
    {
        return $this->vehicles->removeElement($vehicle);
    }

    /**
     * Get vehicles.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVehicles()
    {
        return $this->vehicles;
    }
}
