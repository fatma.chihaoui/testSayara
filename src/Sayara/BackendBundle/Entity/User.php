<?php

namespace Sayara\BackendBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 *
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="Sayara\BackendBundle\Repository\UserRepository")
 * @Vich\Uploadable
 */
class User extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @Assert\Email(message = " '{{ value }}' est invalide ! .",checkMX = true)
     * @Assert\NotNull(message ="Indiquez votre address email")
     */
    protected $email;

    /**
    * @Assert\NotNull(message ="Veuillez indiquer le mot de passe")
    *
    protected $plainPassword;*/

    /**
     * @Assert\NotNull(message = "Vous devez indiquer votre pseudo.")
     */
    protected $username;


    /**
     * @var int
     * @ORM\Column(type="integer",length=255, nullable= true)
     */
    private $gender;
    /**
     * @Assert\NotNull(message = "Vous devez indiquer votre prénom.")
     * @ORM\Column(type="string",length=255)
     */
    private $firstname;
    /**
     * @Assert\NotNull(message = "Vous devez indiquer votre nom.")
     * @ORM\Column(type="string",length=255)
     */
    private $lastname;
    /**
     * @Assert\Regex("/^[0-9]{8}/")
     * @Assert\NotNull(message = "Vous devez indiquer votre phone.")
     * @ORM\Column(type="integer")
     */
    private $phone;
    /**
     * @ORM\Column(type="boolean")
     */
    private $visiblePhone =false;

    /**
     * @Assert\Expression( "this.getAcceptCondition() == true",message="Vous devez avoir lire et accepter toutes les conditions !")
     * @ORM\Column(type="boolean")
     */
    private $acceptCondition =false;
    /**
     * @ORM\Column(type="datetime",nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="Vehicle", mappedBy="user")
     */
    private $vehicles;

    /**
     * @ORM\OneToMany(targetEntity="Spare", mappedBy="user")
     */
    private $spares;


    /**
     * @ORM\OneToMany(targetEntity="Article", mappedBy="user")
     */
    private $articles;

    /**
     * @Assert\Valid
     * @ORM\ManyToOne(targetEntity="Address", inversedBy="users", cascade={"persist"})
     * @ORM\JoinColumn(name="address_id", referencedColumnName="id")
     */
    private $address;

    /**
     * One User has One Photo.
     * @ORM\OneToOne(targetEntity="Photo", cascade={"persist"})
     * @ORM\JoinColumn(name="photo_id", referencedColumnName="id")
     */
    private $photo;

    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->articles= new ArrayCollection();

    }



    /**
     * Set firstname.
     *
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname.
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname.
     *
     * @param string $lastname
     *
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname.
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set phone.
     *
     * @param int $phone
     *
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone.
     *
     * @return int
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set visiblePhone.
     *
     * @param bool $visiblePhone
     *
     * @return User
     */
    public function setVisiblePhone($visiblePhone)
    {
        $this->visiblePhone = $visiblePhone;

        return $this;
    }

    /**
     * Get visiblePhone.
     *
     * @return bool
     */
    public function getVisiblePhone()
    {
        return $this->visiblePhone;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return User
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

  


    /**
     * Set address.
     *
     * @param \Sayara\BackendBundle\Entity\Address|null $address
     *
     * @return User
     */
    public function setAddress(\Sayara\BackendBundle\Entity\Address $address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address.
     *
     * @return \Sayara\BackendBundle\Entity\Address|null
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Add article.
     *
     * @param \Sayara\BackendBundle\Entity\Article $article
     *
     * @return User
     */
    public function addArticle(\Sayara\BackendBundle\Entity\Article $article)
    {
        $this->articles[] = $article;

        return $this;
    }

    /**
     * Remove article.
     *
     * @param \Sayara\BackendBundle\Entity\Article $article
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeArticle(\Sayara\BackendBundle\Entity\Article $article)
    {
        return $this->articles->removeElement($article);
    }

    /**
     * Get articles.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * Set photo.
     *
     * @param \Sayara\BackendBundle\Entity\Photo|null $photo
     *
     * @return User
     */
    public function setPhoto(\Sayara\BackendBundle\Entity\Photo $photo = null)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo.
     *
     * @return \Sayara\BackendBundle\Entity\Photo|null
     */
    public function getPhoto()
    {
        return $this->photo;
    }





    /**
     * Set gender.
     *
     * @param int $gender
     *
     * @return User
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender.
     *
     * @return int
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set acceptCondition.
     *
     * @param bool $acceptCondition
     *
     * @return User
     */
    public function setAcceptCondition($acceptCondition)
    {
        $this->acceptCondition = $acceptCondition;

        return $this;
    }

    /**
     * Get acceptCondition.
     *
     * @return bool
     */
    public function getAcceptCondition()
    {
        return $this->acceptCondition;
    }

    /**
     * Add vehicle.
     *
     * @param \Sayara\BackendBundle\Entity\Vehicle $vehicle
     *
     * @return User
     */
    public function addVehicle(\Sayara\BackendBundle\Entity\Vehicle $vehicle)
    {
        $this->vehicles[] = $vehicle;

        return $this;
    }

    /**
     * Remove vehicle.
     *
     * @param \Sayara\BackendBundle\Entity\Vehicle $vehicle
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeVehicle(\Sayara\BackendBundle\Entity\Vehicle $vehicle)
    {
        return $this->vehicles->removeElement($vehicle);
    }

    /**
     * Get vehicles.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVehicles()
    {
        return $this->vehicles;
    }

    /**
     * Add spare.
     *
     * @param \Sayara\BackendBundle\Entity\Spare $spare
     *
     * @return User
     */
    public function addSpare(\Sayara\BackendBundle\Entity\Spare $spare)
    {
        $this->spares[] = $spare;

        return $this;
    }

    /**
     * Remove spare.
     *
     * @param \Sayara\BackendBundle\Entity\Spare $spare
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeSpare(\Sayara\BackendBundle\Entity\Spare $spare)
    {
        return $this->spares->removeElement($spare);
    }

    /**
     * Get spares.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSpares()
    {
        return $this->spares;
    }
}
