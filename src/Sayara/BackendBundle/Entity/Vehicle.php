<?php

namespace Sayara\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Vehicle
 *
 * @ORM\Table(name="vehicle")
 * @ORM\Entity(repositoryClass="Sayara\BackendBundle\Repository\VehicleRepository")
 */
class Vehicle
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotNull(message ="Veuillez bien indiquez le titre d'annonce")
     * @ORM\Column(name="title_application", type="string", length=255)
     */
    private $title;


    /**
     * @var string
     *
     * @ORM\Column(name="description_application", type="text", nullable=true)
     */
    private $description;

    /**
     * @var int
     * @Assert\NotNull(message ="Veuillez préciser le nombre de places. ")
     * @Assert\GreaterThan(value = 1)
     * @ORM\Column(name="nbSit", type="integer", nullable=true)
     */
    private $nbSit;

    /**
     * @var int
     * @Assert\NotNull(message ="Veuillez préciser le nombre de cylindres. ")
     * @Assert\GreaterThan(value = 1)
     * @ORM\Column(name="nbCylindre", type="integer", nullable=true)
     */
    private $nbCylindre;

    /**
     * @var string
     * @Assert\NotNull(message ="Veuillez préciser l'énergie (Diesel, Gaz ou Essence) . ")
     * @ORM\Column(name="power", type="string", length=255, nullable=true)
     */
    private $power;

    /**
     * @var string
     * @Assert\NotNull(message ="Veuillez préciser la puissance . ")
     * @Assert\GreaterThan(value = 1)
     * @ORM\Column(name="puissance", type="integer", nullable=true)
     */
    private $puissance;

    /**
     * @var string
     * @Assert\NotNull(message ="Veuillez préciser le type du boite vitesse (Manuel ou Automatique) . ")
     * @ORM\Column(name="typeBoite", type="string", length=255,nullable=true)
     */
    private $typeBoite;

    /**
     * @var int
     * @ORM\Column(name="weight", type="integer",nullable=true)
     */
    private $weight;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=255, nullable=true)
     */
    private $color;

    /**
     * @var \DateTime
     * @Assert\NotNull(message ="Veuillez préciser la date ")
     * @ORM\Column(name="dateOfCirculation", type="date", nullable=true)
     */
    private $dateOfCirculation;

    /**
     * @var int
     *
     * @ORM\Column(name="nbDoor", type="integer", nullable=true)
     */
    private $nbDoor;

    /**
     * @var int
     * @Assert\NotNull(message ="Veuillez précisez l'age en nombre de mois")
     * @ORM\Column(name="year", type="integer")
     */
    private $year;

    /**
     * @var string
     *
     * @ORM\Column(name="fuel", type="string", length=255 , nullable=true)
     */
    private $fuel;

    /**
     * @var int
     *
     * @ORM\Column(name="motor", type="integer", nullable=true)
     */
    private $motor;

    /**
     * @var int
     *
     * @ORM\Column(name="cvdin", type="integer", nullable=true)
     */
    private $cvdin;

    /**
     * @var string
     *
     * @ORM\Column(name="transmission", type="string", length=255, nullable=true)
     */
    private $transmission;

    /**
     * @var int
     * @Assert\NotNull(message ="Veuillez indiquer l'etat de votre véhicule . ")
     * @ORM\Column(name="vehicle_state", type="integer", nullable=true)
     */
    private $state;

    /**
     * @var string
     *
     * @ORM\Column(name="exterior", type="string", length=255, nullable=true)
     */
    private $exterior;

    /**
     * @var string
     *
     * @ORM\Column(name="interior", type="string", length=255, nullable=true)
     */
    private $interior;

    /**
     * @var int
     * @Assert\NotNull(message ="Veuillez préciser le prix ")
     * @ORM\Column(name="price", type="integer", nullable=true)
     */
    private $price;

    /**
     * @var int
     * @Assert\NotNull(message ="Veuillez préciser le kilomètrage ")
     * @Assert\GreaterThan(value = 1)
     * @ORM\Column(name="kilometrage", type="integer", nullable=true)
     */
    private $mileage;


    /**
     * @Assert\Valid
     * @ORM\ManyToOne(targetEntity="Modele")
     * @ORM\JoinColumn(name="modele_id",referencedColumnName="id",onDelete="CASCADE")
     */
    private $modele;
    /**
     * @Assert\Valid
     * @ORM\ManyToOne(targetEntity="Marque")
     * @ORM\JoinColumn(name="marque_id",referencedColumnName="id",onDelete="CASCADE")
     */
    private $marque;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id",referencedColumnName="id",onDelete="CASCADE")
     */
    private $user;

    /**
     * @Assert\Valid
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumn(name="category_id",referencedColumnName="id")
     */
    private $category;


    /**
     * @ORM\OneToMany(targetEntity="Photo", mappedBy="application")
     */
    private $photos;

    /**
     * @var bool
     * @ORM\Column(name="is_public", type="boolean")
     */
    private $isPublic;


    /**
     * for soft delete
     * @var bool
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $active;

    /**
     * @var int
     * @ORM\Column(name="type_vehicle", type="integer")
     */
    private $type;

    /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;


    /**
     * @var \DateTime
     * @ORM\Column(name="expired_at", type="datetime", nullable=true)
     */
    private $expiredAt;


    /**
     * @Assert\Valid
     * @ORM\ManyToOne(targetEntity="City")
     * @ORM\JoinColumn(name="city_id",referencedColumnName="id",onDelete="CASCADE")
     */
    private $city;

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity(\Sayara\BackendBundle\Entity\City $city = null )
    {
        $this->city = $city;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->createdAt= new \DateTime('now');
        $this->isPublic= false;
        $this->active= true;
        $this->photos = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * Set nbSit.
     *
     * @param int $nbSit
     *
     * @return Vehicle
     */
    public function setNbSit($nbSit)
    {
        $this->nbSit = $nbSit;

        return $this;
    }

    /**
     * Get nbSit.
     *
     * @return int
     */
    public function getNbSit()
    {
        return $this->nbSit;
    }

    /**
     * Set nbCylindre.
     *
     * @param int $nbCylindre
     *
     * @return Vehicle
     */
    public function setNbCylindre($nbCylindre)
    {
        $this->nbCylindre = $nbCylindre;

        return $this;
    }

    /**
     * Get nbCylindre.
     *
     * @return int
     */
    public function getNbCylindre()
    {
        return $this->nbCylindre;
    }

    /**
     * Set power.
     *
     * @param string $power
     *
     * @return Vehicle
     */
    public function setPower($power)
    {
        $this->power = $power;

        return $this;
    }

    /**
     * Get power.
     *
     * @return string
     */
    public function getPower()
    {
        return $this->power;
    }

    /**
     * Set typeBoite.
     *
     * @param string $typeBoite
     *
     * @return Vehicle
     */
    public function setTypeBoite($typeBoite)
    {
        $this->typeBoite = $typeBoite;

        return $this;
    }

    /**
     * Get typeBoite.
     *
     * @return string
     */
    public function getTypeBoite()
    {
        return $this->typeBoite;
    }

    /**
     * Set weight.
     *
     * @param int $weight
     *
     * @return Vehicle
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight.
     *
     * @return int
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set color.
     *
     * @param string $color
     *
     * @return Vehicle
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color.
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set dateOfCirculation.
     *
     * @param \DateTime $dateOfCirculation
     *
     * @return Vehicle
     */
    public function setDateOfCirculation($dateOfCirculation)
    {
        $this->dateOfCirculation = $dateOfCirculation;

        return $this;
    }

    /**
     * Get dateOfCirculation.
     *
     * @return \DateTime
     */
    public function getDateOfCirculation()
    {
        return $this->dateOfCirculation;
    }

    /**
     * Set nbDoor.
     *
     * @param int $nbDoor
     *
     * @return Vehicle
     */
    public function setNbDoor($nbDoor)
    {
        $this->nbDoor = $nbDoor;

        return $this;
    }

    /**
     * Get nbDoor.
     *
     * @return int
     */
    public function getNbDoor()
    {
        return $this->nbDoor;
    }

    /**
     * Set year.
     *
     * @param int $year
     *
     * @return Vehicle
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year.
     *
     * @return int
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set fuel.
     *
     * @param string $fuel
     *
     * @return Vehicle
     */
    public function setFuel($fuel)
    {
        $this->fuel = $fuel;

        return $this;
    }

    /**
     * Get fuel.
     *
     * @return string
     */
    public function getFuel()
    {
        return $this->fuel;
    }

    /**
     * Set motor.
     *
     * @param int $motor
     *
     * @return Vehicle
     */
    public function setMotor($motor)
    {
        $this->motor = $motor;

        return $this;
    }

    /**
     * Get motor.
     *
     * @return int
     */
    public function getMotor()
    {
        return $this->motor;
    }

    /**
     * Set cvdin.
     *
     * @param int $cvdin
     *
     * @return Vehicle
     */
    public function setCvdin($cvdin)
    {
        $this->cvdin = $cvdin;

        return $this;
    }

    /**
     * Get cvdin.
     *
     * @return int
     */
    public function getCvdin()
    {
        return $this->cvdin;
    }

    /**
     * Set transmission.
     *
     * @param string $transmission
     *
     * @return Vehicle
     */
    public function setTransmission($transmission)
    {
        $this->transmission = $transmission;

        return $this;
    }

    /**
     * Get transmission.
     *
     * @return string
     */
    public function getTransmission()
    {
        return $this->transmission;
    }

    /**
     * Set exterior.
     *
     * @param string $exterior
     *
     * @return Vehicle
     */
    public function setExterior($exterior)
    {
        $this->exterior = $exterior;

        return $this;
    }

    /**
     * Get exterior.
     *
     * @return string
     */
    public function getExterior()
    {
        return $this->exterior;
    }

    /**
     * Set interior.
     *
     * @param string $interior
     *
     * @return Vehicle
     */
    public function setInterior($interior)
    {
        $this->interior = $interior;

        return $this;
    }

    /**
     * Get interior.
     *
     * @return string
     */
    public function getInterior()
    {
        return $this->interior;
    }

    /**
     * Set price.
     *
     * @param int $price
     *
     * @return Vehicle
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price.
     *
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Vehicle
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set mileage.
     *
     * @param int $mileage
     *
     * @return Vehicle
     */
    public function setMileage($mileage)
    {
        $this->mileage = $mileage;

        return $this;
    }

    /**
     * Get mileage.
     *
     * @return int
     */
    public function getMileage()
    {
        return $this->mileage;
    }

    /**
     * Set modele.
     *
     * @param \Sayara\BackendBundle\Entity\Modele|null $modele
     *
     * @return Vehicle
     */
    public function setModele(\Sayara\BackendBundle\Entity\Modele $modele = null)
    {
        $this->modele = $modele;

        return $this;
    }

    /**
     * Get modele.
     *
     * @return \Sayara\BackendBundle\Entity\Modele|null
     */
    public function getModele()
    {
        return $this->modele;
    }

    /**
     * Set marque.
     *
     * @param \Sayara\BackendBundle\Entity\Marque|null $marque
     *
     * @return Vehicle
     */
    public function setMarque(\Sayara\BackendBundle\Entity\Marque $marque = null)
    {
        $this->marque = $marque;

        return $this;
    }

    /**
     * Get marque.
     *
     * @return \Sayara\BackendBundle\Entity\Marque|null
     */
    public function getMarque()
    {
        return $this->marque;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Vehicle
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Add photo.
     *
     * @param \Sayara\BackendBundle\Entity\Photo $photo
     *
     * @return Vehicle
     */
    public function addPhoto(\Sayara\BackendBundle\Entity\Photo $photo)
    {
        $this->photos[] = $photo;

        return $this;
    }

    /**
     * Remove photo.
     *
     * @param \Sayara\BackendBundle\Entity\Photo $photo
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePhoto(\Sayara\BackendBundle\Entity\Photo $photo)
    {
        return $this->photos->removeElement($photo);
    }

    /**
     * Get photos.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * Set address.
     *
     * @param \Sayara\BackendBundle\Entity\Address|null $address
     *
     * @return Vehicle
     */
    public function setAddress(\Sayara\BackendBundle\Entity\Address $address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address.
     *
     * @return \Sayara\BackendBundle\Entity\Address|null
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set puissance.
     *
     * @param int|null $puissance
     *
     * @return Vehicle
     */
    public function setPuissance($puissance = null)
    {
        $this->puissance = $puissance;

        return $this;
    }

    /**
     * Get puissance.
     *
     * @return int|null
     */
    public function getPuissance()
    {
        return $this->puissance;
    }

    /**
     * Set isPublic.
     *
     * @param bool $isPublic
     *
     * @return Vehicle
     */
    public function setIsPublic($isPublic)
    {
        $this->isPublic = $isPublic;

        return $this;
    }

    /**
     * Get isPublic.
     *
     * @return bool
     */
    public function getIsPublic()
    {
        return $this->isPublic;
    }

    /**
     * Set type.
     *
     * @param int $type
     *
     * @return Vehicle
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set user.
     *
     * @param \Sayara\BackendBundle\Entity\User|null $user
     *
     * @return Vehicle
     */
    public function setUser(\Sayara\BackendBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \Sayara\BackendBundle\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Vehicle
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set active.
     *
     * @param bool $active
     *
     * @return Vehicle
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active.
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set state.
     *
     * @param int|null $state
     *
     * @return Vehicle
     */
    public function setState($state = null)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state.
     *
     * @return int|null
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set expiredAt.
     *
     * @param \DateTime|null $expiredAt
     *
     * @return Vehicle
     */
    public function setExpiredAt($expiredAt = null)
    {
        $this->expiredAt = $expiredAt;

        return $this;
    }

    /**
     * Get expiredAt.
     *
     * @return \DateTime|null
     */
    public function getExpiredAt()
    {
        return $this->expiredAt;
    }

    /**
     * Set category.
     *
     * @param \Sayara\BackendBundle\Entity\Category|null $category
     *
     * @return Vehicle
     */
    public function setCategory(\Sayara\BackendBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category.
     *
     * @return \Sayara\BackendBundle\Entity\Category|null
     */
    public function getCategory()
    {
        return $this->category;
    }
}
