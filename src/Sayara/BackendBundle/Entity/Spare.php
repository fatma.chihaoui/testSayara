<?php

namespace Sayara\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Spare
 *
 * @ORM\Table(name="spare")
 * @ORM\Entity(repositoryClass="Sayara\BackendBundle\Repository\SpareRepository")
 */
class Spare
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotNull(message ="Veuillez bien indiquez le titre d'annonce")
     * @ORM\Column(name="title_application", type="string", length=255)
     */
    private $title;


    /**
     * @var string
     *
     * @ORM\Column(name="description_application", type="text", nullable=true)
     */
    private $description;

    /**
     * @var int
     * @Assert\NotNull(message ="Veuillez précisez l'age en nombre de mois")
     * @ORM\Column(name="year", type="integer")
     */
    private $year;


    /**
     * @var int
     * @Assert\NotNull(message ="Veuillez préciser le prix ")
     * @ORM\Column(name="price", type="integer", nullable=true)
     */
    private $price;



    /**
     * @Assert\Valid
     * @ORM\ManyToOne(targetEntity="Modele")
     * @ORM\JoinColumn(name="modele_id",referencedColumnName="id",onDelete="CASCADE")
     */
    private $modele;
    /**
     * @Assert\Valid
     * @ORM\ManyToOne(targetEntity="Marque")
     * @ORM\JoinColumn(name="marque_id",referencedColumnName="id",onDelete="CASCADE")
     */
    private $marque;


    /**
     * @ORM\OneToMany(targetEntity="Photo", mappedBy="application")
     */
    private $photos;

    /**
     * @var bool
     * @ORM\Column(name="is_public", type="boolean")
     */
    private $isPublic;

    /**
     * @var int
     * @ORM\Column(name="type_spare", type="integer")
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id",referencedColumnName="id",onDelete="CASCADE")
     */
    private $user;

    /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="expired_at", type="datetime", nullable=true)
     */
    private $expiredAt;

    /**
     * @var int
     * @Assert\NotNull(message ="Veuillez indiquer l'etat de votre véhicule . ")
     * @ORM\Column(name="vehicle_state", type="integer", nullable=true)
     */
    private $state;

    /**
     * @Assert\Valid
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumn(name="category_id",referencedColumnName="id")
     */
    private $category;


    /**
     * @Assert\Valid
     * @ORM\ManyToOne(targetEntity="City")
     * @ORM\JoinColumn(name="city_id",referencedColumnName="id",onDelete="CASCADE")
     */
    private $city;

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->createdAt= new \DateTime('now');
        $this->photos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Spare
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return Spare
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set year.
     *
     * @param int $year
     *
     * @return Spare
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year.
     *
     * @return int
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set price.
     *
     * @param int|null $price
     *
     * @return Spare
     */
    public function setPrice($price = null)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price.
     *
     * @return int|null
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set isPublic.
     *
     * @param bool $isPublic
     *
     * @return Spare
     */
    public function setIsPublic($isPublic)
    {
        $this->isPublic = $isPublic;

        return $this;
    }

    /**
     * Get isPublic.
     *
     * @return bool
     */
    public function getIsPublic()
    {
        return $this->isPublic;
    }

    /**
     * Set type.
     *
     * @param int $type
     *
     * @return Spare
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set modele.
     *
     * @param \Sayara\BackendBundle\Entity\Modele|null $modele
     *
     * @return Spare
     */
    public function setModele(\Sayara\BackendBundle\Entity\Modele $modele = null)
    {
        $this->modele = $modele;

        return $this;
    }

    /**
     * Get modele.
     *
     * @return \Sayara\BackendBundle\Entity\Modele|null
     */
    public function getModele()
    {
        return $this->modele;
    }

    /**
     * Set marque.
     *
     * @param \Sayara\BackendBundle\Entity\Marque|null $marque
     *
     * @return Spare
     */
    public function setMarque(\Sayara\BackendBundle\Entity\Marque $marque = null)
    {
        $this->marque = $marque;

        return $this;
    }

    /**
     * Get marque.
     *
     * @return \Sayara\BackendBundle\Entity\Marque|null
     */
    public function getMarque()
    {
        return $this->marque;
    }

    /**
     * Add photo.
     *
     * @param \Sayara\BackendBundle\Entity\Photo $photo
     *
     * @return Spare
     */
    public function addPhoto(\Sayara\BackendBundle\Entity\Photo $photo)
    {
        $this->photos[] = $photo;

        return $this;
    }

    /**
     * Remove photo.
     *
     * @param \Sayara\BackendBundle\Entity\Photo $photo
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePhoto(\Sayara\BackendBundle\Entity\Photo $photo)
    {
        return $this->photos->removeElement($photo);
    }

    /**
     * Get photos.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * Set user.
     *
     * @param \Sayara\BackendBundle\Entity\User|null $user
     *
     * @return Spare
     */
    public function setUser(\Sayara\BackendBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \Sayara\BackendBundle\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return Spare
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set state.
     *
     * @param int|null $state
     *
     * @return Spare
     */
    public function setState($state = null)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state.
     *
     * @return int|null
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set expiredAt.
     *
     * @param \DateTime|null $expiredAt
     *
     * @return Spare
     */
    public function setExpiredAt($expiredAt = null)
    {
        $this->expiredAt = $expiredAt;

        return $this;
    }

    /**
     * Get expiredAt.
     *
     * @return \DateTime|null
     */
    public function getExpiredAt()
    {
        return $this->expiredAt;
    }

    /**
     * Set category.
     *
     * @param \Sayara\BackendBundle\Entity\Category|null $category
     *
     * @return Spare
     */
    public function setCategory(\Sayara\BackendBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category.
     *
     * @return \Sayara\BackendBundle\Entity\Category|null
     */
    public function getCategory()
    {
        return $this->category;
    }
}
