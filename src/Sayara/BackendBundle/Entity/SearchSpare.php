<?php

namespace Sayara\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SearchSpare
 *
 * @ORM\Table(name="search_spare")
 * @ORM\Entity(repositoryClass="Sayara\BackendBundle\Repository\SearchSpareRepository")
 */
class SearchSpare
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**

     * @ORM\ManyToOne(targetEntity="Modele")
     * @ORM\JoinColumn(name="modele_id",referencedColumnName="id",onDelete="CASCADE")
     */
    private $modele;
    /**

     * @ORM\ManyToOne(targetEntity="Marque")
     * @ORM\JoinColumn(name="marque_id",referencedColumnName="id",onDelete="CASCADE")
     */
    private $marque;

    /**
     * @var int
     *
     * @ORM\Column(name="minPrice", type="integer")
     */
    private $minPrice;

    /**
     * @var int
     *
     * @ORM\Column(name="maxPrice", type="integer")
     */
    private $maxPrice;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set minPrice.
     *
     * @param int $minPrice
     *
     * @return SearchSpare
     */
    public function setMinPrice($minPrice)
    {
        $this->minPrice = $minPrice;

        return $this;
    }

    /**
     * Get minPrice.
     *
     * @return int
     */
    public function getMinPrice()
    {
        return $this->minPrice;
    }

    /**
     * Set maxPrice.
     *
     * @param int $maxPrice
     *
     * @return SearchSpare
     */
    public function setMaxPrice($maxPrice)
    {
        $this->maxPrice = $maxPrice;

        return $this;
    }

    /**
     * Get maxPrice.
     *
     * @return int
     */
    public function getMaxPrice()
    {
        return $this->maxPrice;
    }

    /**
     * Set modele.
     *
     * @param \Sayara\BackendBundle\Entity\Modele|null $modele
     *
     * @return SearchSpare
     */
    public function setModele(\Sayara\BackendBundle\Entity\Modele $modele = null)
    {
        $this->modele = $modele;

        return $this;
    }

    /**
     * Get modele.
     *
     * @return \Sayara\BackendBundle\Entity\Modele|null
     */
    public function getModele()
    {
        return $this->modele;
    }

    /**
     * Set marque.
     *
     * @param \Sayara\BackendBundle\Entity\Marque|null $marque
     *
     * @return SearchSpare
     */
    public function setMarque(\Sayara\BackendBundle\Entity\Marque $marque = null)
    {
        $this->marque = $marque;

        return $this;
    }

    /**
     * Get marque.
     *
     * @return \Sayara\BackendBundle\Entity\Marque|null
     */
    public function getMarque()
    {
        return $this->marque;
    }
}
