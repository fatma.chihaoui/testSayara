<?php

namespace Sayara\BackendBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Modele
 * @ORM\Table(name="modeles")
 * @ORM\Entity()
 * @Vich\Uploadable
 */
class Modele
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string",length=255)
     */
    private $libelle;

    /**
     * @ORM\ManyToOne(targetEntity="Marque")
     * @ORM\JoinColumn(name="marque", referencedColumnName="id",onDelete="CASCADE")
     */
    private $marque;

    /**
     * @ORM\OneToMany(targetEntity="Version", mappedBy="id")
     */
    private $versions;
    /**
     * @ORM\OneToOne(targetEntity="Photo", cascade={"persist"})
     * @ORM\JoinColumn(name="photo_id", referencedColumnName="id")
     */
    private $photo;

    /**
     * @ORM\Column(type="datetime",nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * One Application for One vehicle
     * @ORM\OneToOne(targetEntity="Vehicle", cascade={"persist"})
     * @ORM\JoinColumn(name="vehicle_id", referencedColumnName="id")
     */
    private $vehicle;



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->versions = new ArrayCollection();
    }



    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle.
     *
     * @param string $libelle
     *
     * @return Modele
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle.
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return Modele
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set marque.
     *
     * @param \Sayara\BackendBundle\Entity\Marque|null $marque
     *
     * @return Modele
     */
    public function setMarque(\Sayara\BackendBundle\Entity\Marque $marque = null)
    {
        $this->marque = $marque;

        return $this;
    }

    /**
     * Get marque.
     *
     * @return \Sayara\BackendBundle\Entity\Marque|null
     */
    public function getMarque()
    {
        return $this->marque;
    }

    /**
     * Add version.
     *
     * @param \Sayara\BackendBundle\Entity\Version $version
     *
     * @return Modele
     */
    public function addVersion(\Sayara\BackendBundle\Entity\Version $version)
    {
        $this->versions[] = $version;

        return $this;
    }

    /**
     * Remove version.
     *
     * @param \Sayara\BackendBundle\Entity\Version $version
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeVersion(\Sayara\BackendBundle\Entity\Version $version)
    {
        return $this->versions->removeElement($version);
    }

    /**
     * Get versions.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVersions()
    {
        return $this->versions;
    }

    /**
     * Set photo.
     *
     * @param \Sayara\BackendBundle\Entity\Photo|null $photo
     *
     * @return Modele
     */
    public function setPhoto(\Sayara\BackendBundle\Entity\Photo $photo = null)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo.
     *
     * @return \Sayara\BackendBundle\Entity\Photo|null
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set vehicle.
     *
     * @param \Sayara\BackendBundle\Entity\Vehicle|null $vehicle
     *
     * @return Modele
     */
    public function setVehicle(\Sayara\BackendBundle\Entity\Vehicle $vehicle = null)
    {
        $this->vehicle = $vehicle;

        return $this;
    }

    /**
     * Get vehicle.
     *
     * @return \Sayara\BackendBundle\Entity\Vehicle|null
     */
    public function getVehicle()
    {
        return $this->vehicle;
    }

    function __toString()
    {
        return $this->getLibelle();
    }


}
