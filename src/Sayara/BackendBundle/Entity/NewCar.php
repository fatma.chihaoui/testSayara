<?php

namespace Sayara\BackendBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * NewCar
 *
 * @ORM\Table(name="cars")
 * @ORM\Entity()
 */
class NewCar
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="title_application", type="string", length=255, nullable=true)
     */
    private $title;


    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;



    /**
     * @var int
     * @Assert\NotNull(message ="Veuillez préciser le prix ")
     * @ORM\Column(name="price", type="integer", nullable=true)
     */
    private $price;

    /**
     * @Assert\Valid
     * @ORM\ManyToOne(targetEntity="Modele")
     * @ORM\JoinColumn(name="modele_id",referencedColumnName="id",onDelete="CASCADE")
     */
    private $modele;
    /**
     * @Assert\Valid
     * @ORM\ManyToOne(targetEntity="Marque")
     * @ORM\JoinColumn(name="marque_id",referencedColumnName="id",onDelete="CASCADE")
     */
    private $marque;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id",referencedColumnName="id",onDelete="CASCADE")
     */
    private $user;


    /**
     * @ORM\OneToMany(targetEntity="Photo", mappedBy="newCar")
     */
    private $photos;

    /**
     * @ORM\OneToMany(targetEntity="ImageIn", mappedBy="car",cascade={"persist"})
     */
    private $imageins;

    /**
     * @ORM\OneToMany(targetEntity="ImageOut", mappedBy="car",cascade={"persist"})
     * @ORM\JoinColumn(name="imageouts_id",referencedColumnName="id",onDelete="CASCADE")
     */
    private $imageouts;

    /**
     * @ORM\OneToMany(targetEntity="Version", mappedBy="car")
     */
    private $versions;

    /**
     * @var bool
     * @ORM\Column(name="is_public", type="boolean")
     */
    private $isPublic;


    /**
     * for soft delete
     * @var bool
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $active;


    /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;


    /**
     * @var \DateTime
     * @ORM\Column(name="expired_at", type="datetime", nullable=true)
     */
    private $expiredAt;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->createdAt= new \DateTime('now');
        $this->expiredAt= $this->createdAt->modify('+1 month');
        $this->isPublic= false;
        $this->active= true;
        $this->photos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->imageins= new ArrayCollection();
        $this->imageouts= new ArrayCollection();
        $this->versions= new ArrayCollection();
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }





    /**
     * Set title.
     *
     * @param string $title
     *
     * @return NewCar
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return NewCar
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set price.
     *
     * @param int|null $price
     *
     * @return NewCar
     */
    public function setPrice($price = null)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price.
     *
     * @return int|null
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set isPublic.
     *
     * @param bool $isPublic
     *
     * @return NewCar
     */
    public function setIsPublic($isPublic)
    {
        $this->isPublic = $isPublic;

        return $this;
    }

    /**
     * Get isPublic.
     *
     * @return bool
     */
    public function getIsPublic()
    {
        return $this->isPublic;
    }

    /**
     * Set active.
     *
     * @param bool $active
     *
     * @return NewCar
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active.
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return NewCar
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set expiredAt.
     *
     * @param \DateTime|null $expiredAt
     *
     * @return NewCar
     */
    public function setExpiredAt($expiredAt = null)
    {
        $this->expiredAt = $expiredAt;

        return $this;
    }

    /**
     * Get expiredAt.
     *
     * @return \DateTime|null
     */
    public function getExpiredAt()
    {
        return $this->expiredAt;
    }

    /**
     * Set modele.
     *
     * @param \Sayara\BackendBundle\Entity\Modele|null $modele
     *
     * @return NewCar
     */
    public function setModele(\Sayara\BackendBundle\Entity\Modele $modele = null)
    {
        $this->modele = $modele;

        return $this;
    }

    /**
     * Get modele.
     *
     * @return \Sayara\BackendBundle\Entity\Modele|null
     */
    public function getModele()
    {
        return $this->modele;
    }

    /**
     * Set marque.
     *
     * @param \Sayara\BackendBundle\Entity\Marque|null $marque
     *
     * @return NewCar
     */
    public function setMarque(\Sayara\BackendBundle\Entity\Marque $marque = null)
    {
        $this->marque = $marque;

        return $this;
    }

    /**
     * Get marque.
     *
     * @return \Sayara\BackendBundle\Entity\Marque|null
     */
    public function getMarque()
    {
        return $this->marque;
    }

    /**
     * Set user.
     *
     * @param \Sayara\BackendBundle\Entity\User|null $user
     *
     * @return NewCar
     */
    public function setUser(\Sayara\BackendBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \Sayara\BackendBundle\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add photo.
     *
     * @param \Sayara\BackendBundle\Entity\Photo $photo
     *
     * @return NewCar
     */
    public function addPhoto(\Sayara\BackendBundle\Entity\Photo $photo)
    {
        $this->photos[] = $photo;

        return $this;
    }

    /**
     * Remove photo.
     *
     * @param \Sayara\BackendBundle\Entity\Photo $photo
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePhoto(\Sayara\BackendBundle\Entity\Photo $photo)
    {
        return $this->photos->removeElement($photo);
    }

    /**
     * Get photos.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * Add imagein.
     *
     * @param \Sayara\BackendBundle\Entity\ImageIn $imagein
     *
     * @return NewCar
     */
    public function addImagein(\Sayara\BackendBundle\Entity\ImageIn $imagein)
    {
        $this->imageins[] = $imagein;

        return $this;
    }

    /**
     * Remove imagein.
     *
     * @param \Sayara\BackendBundle\Entity\ImageIn $imagein
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeImagein(\Sayara\BackendBundle\Entity\ImageIn $imagein)
    {
        return $this->imageins->removeElement($imagein);
    }

    /**
     * Get imageins.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImageins()
    {
        return $this->imageins;
    }

    /**
     * Add imageout.
     *
     * @param \Sayara\BackendBundle\Entity\ImageOut $imageout
     *
     * @return NewCar
     */
    public function addImageout(\Sayara\BackendBundle\Entity\ImageOut $imageout)
    {
        $this->imageouts[] = $imageout;

        return $this;
    }

    /**
     * Remove imageout.
     *
     * @param \Sayara\BackendBundle\Entity\ImageOut $imageout
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeImageout(\Sayara\BackendBundle\Entity\ImageOut $imageout)
    {
        return $this->imageouts->removeElement($imageout);
    }

    /**
     * Get imageouts.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImageouts()
    {
        return $this->imageouts;
    }

    /**
     * Add version.
     *
     * @param \Sayara\BackendBundle\Entity\Version $version
     *
     * @return NewCar
     */
    public function addVersion(\Sayara\BackendBundle\Entity\Version $version)
    {
        $this->versions[] = $version;

        return $this;
    }

    /**
     * Remove version.
     *
     * @param \Sayara\BackendBundle\Entity\Version $version
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeVersion(\Sayara\BackendBundle\Entity\Version $version)
    {
        return $this->versions->removeElement($version);
    }

    /**
     * Get versions.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVersions()
    {
        return $this->versions;
    }
}
