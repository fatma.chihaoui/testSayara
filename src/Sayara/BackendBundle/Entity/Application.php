<?php

namespace Sayara\BackendBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Application
 *
 * @ORM\Table(name="applications")
 * @ORM\Entity(repositoryClass="Sayara\BackendBundle\Repository\ApplicationRepository")
 */
class Application
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable= true)
     */
    private $createdAt;


    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;




    /**
     * Application constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime('now');
        $this->photos = new ArrayCollection();
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Application
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Application
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }



    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Application
     */
    public function setCreatedAt($created)
    {
        $this->createdAt = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set active.
     *
     * @param bool $active
     *
     * @return Application
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active.
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }


    /**
     * Set user.
     *
     * @param \Sayara\BackendBundle\Entity\User|null $user
     *
     * @return Application
     */
    public function setUser(\Sayara\BackendBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \Sayara\BackendBundle\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set address.
     *
     * @param \Sayara\BackendBundle\Entity\Address|null $address
     *
     * @return Application
     */
    public function setAddress(\Sayara\BackendBundle\Entity\Address $address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address.
     *
     * @return \Sayara\BackendBundle\Entity\Address|null
     */
    public function getAddress()
    {
        return $this->address;
    }
//
//    /**
//     * Set type.
//     *
//     * @param \Sayara\BackendBundle\Entity\Type|null $type
//     *
//     * @return Application
//     */
//    public function setType(\Sayara\BackendBundle\Entity\Type $type = null)
//    {
//        $this->type = $type;
//
//        return $this;
//    }
//
//    /**
//     * Get type.
//     *
//     * @return \Sayara\BackendBundle\Entity\Type|null
//     */
//    public function getType()
//    {
//        return $this->type;
//    }

    /**
     * Add photo.
     *
     * @param \Sayara\BackendBundle\Entity\Photo $photo
     *
     * @return Application
     */
    public function addPhoto(\Sayara\BackendBundle\Entity\Photo $photo)
    {
        $this->photos[] = $photo;

        return $this;
    }

    /**
     * Remove photo.
     *
     * @param \Sayara\BackendBundle\Entity\Photo $photo
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePhoto(\Sayara\BackendBundle\Entity\Photo $photo)
    {
        return $this->photos->removeElement($photo);
    }

    /**
     * Get photos.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * Set vehicle.
     *
     * @param \Sayara\BackendBundle\Entity\Vehicle|null $vehicle
     *
     * @return Application
     */
    public function setVehicle(\Sayara\BackendBundle\Entity\Vehicle $vehicle = null)
    {
        $this->vehicle = $vehicle;

        return $this;
    }

    /**
     * Get vehicle.
     *
     * @return \Sayara\BackendBundle\Entity\Vehicle|null
     */
    public function getVehicle()
    {
        return $this->vehicle;
    }
}
