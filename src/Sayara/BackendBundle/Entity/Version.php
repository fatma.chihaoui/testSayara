<?php

namespace Sayara\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Version
 *
 * @ORM\Table(name="versions")
 * @ORM\Entity(repositoryClass="Sayara\BackendBundle\Repository\VersionRepository")
 */
class Version
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=true )
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\Column(type="string",length=255, nullable=true )
     */
    private $libelle;

    /**
     * @var string
     *
     * @ORM\Column(name="disponibility",    type="string", length=255, nullable=true)
     */
    private $disponibility;

    /**
     * @var int
     *
     * @ORM\Column(name="guarantee", type="integer", nullable=true )
     */
    private $guarantee;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="string", length=255, nullable=true )
     */
    private $body;

    /**
     * @var int
     *
     * @ORM\Column(name="nbSit", type="integer", nullable=true )
     */
    private $nbSit;

    /**
     * @var int
     *
     * @ORM\Column(name="nbDoor", type="integer", nullable=true )
     */
    private $nbDoor;

    /**
     * @var int
     *
     * @ORM\Column(name="NbCylindre", type="integer", nullable=true )
     */
    private $nbCylindre;

    /**
     * @var string
     *
     * @ORM\Column(name="energie", type="string", length=255, nullable=true )
     */
    private $energie;

    /**
     * @var int
     *
     * @ORM\Column(name="puissanceFiscale", type="integer", nullable=true )
     */
    private $puissanceFiscale;

    /**
     * @var int
     *
     * @ORM\Column(name="puissance", type="integer", nullable=true )
     */
    private $puissance;

    /**
     * @var string
     *
     * @ORM\Column(name="couple", type="string", length=255, nullable=true )
     */
    private $couple;

    /**
     * @var int
     *
     * @ORM\Column(name="cylindred", type="integer", nullable=true )
     */
    private $cylindred;

    /**
     * @var string
     *
     * @ORM\Column(name="boite", type="string", length=255, nullable=true )
     */
    private $boite;

    /**
     * @var int
     *
     * @ORM\Column(name="nbRapport", type="integer", nullable=true )
     */
    private $nbRapport;

    /**
     * @var string
     *
     * @ORM\Column(name="transmission", type="string", length=255, nullable=true )
     */
    private $transmission;

    /**
     * @var int
     *
     * @ORM\Column(name="length", type="integer", nullable=true )
     */
    private $length;

    /**
     * @var string
     *
     * @ORM\Column(name="width", type="string", length=255, nullable=true )
     */
    private $width;

    /**
     * @var int
     *
     * @ORM\Column(name="height", type="integer", nullable=true )
     */
    private $height;

    /**
     * @var int
     *
     * @ORM\Column(name="volumeCoffre", type="integer", nullable=true )
     */
    private $volumeCoffre;

    /**
     * @var float
     *
     * @ORM\Column(name="performanceKM", type="float", nullable=true )
     */
    private $performanceKM;

    /**
     * @var int
     *
     * @ORM\Column(name="maxSpeed", type="integer", nullable=true )
     */
    private $maxSpeed;

    /**
     * @var float
     *
     * @ORM\Column(name="urbanConsommation", type="float", nullable=true )
     */
    private $urbanConsommation;

    /**
     * @var float
     *
     * @ORM\Column(name="extraUrbanConsommation", type="float", nullable=true )
     */
    private $extraUrbanConsommation;

    /**
     * @var float
     *
     * @ORM\Column(name="mixteConsommation", type="float", nullable=true )
     */
    private $mixteConsommation;

    /**
     * @var string
     *
     * @ORM\Column(name="ABS", type="string", length=255, nullable=true )
     */
    private $aBS;

    /**
     * @var string
     *
     * @ORM\Column(name="airbags", type="string", nullable=true )
     */
    private $airbags;

    /**
     * @var string
     *
     * @ORM\Column(name="antiPatinage", type="string", length=255, nullable=true )
     */
    private $antiPatinage;

    /**
     * @var string
     *
     * @ORM\Column(name="antiDemarrageElectronic", type="string", length=255, nullable=true )
     */
    private $antiDemarrageElectronic;

    /**
     * @var string
     *
     * @ORM\Column(name="fixationISOFIX", type="string", length=255, nullable=true )
     */
    private $fixationISOFIX;

    /**
     * @var string
     *
     * @ORM\Column(name="accoudoirs", type="string", length=255, nullable=true )
     */
    private $accoudoirs;

    /**
     * @var string
     *
     * @ORM\Column(name="autoradio", type="string", length=255, nullable=true )
     */
    private $autoradio;

    /**
     * @var string
     *
     * @ORM\Column(name="connectivity", type="string", nullable=true )
     */
    private $connectivity;
    /**
     * @var string
     *
     * @ORM\Column(name="screen", type="string", length=255, nullable=true )
     */
    private $screen;

    /**
     * @var string
     *
     * @ORM\Column(name="finitionIn", type="string", length=255, nullable=true )
     */
    private $finitionIn;

    /**
     * @var string
     *
     * @ORM\Column(name="kitSmoking", type="string", length=255, nullable=true )
     */
    private $kitSmoking;

    /**
     * @var string
     *
     * @ORM\Column(name="kitSafety", type="string", length=255, nullable=true )
     */
    private $kitSafety;

    /**
     * @var string
     *
     * @ORM\Column(name="sellerie", type="string", length=255, nullable=true )
     */
    private $sellerie;

    /**
     * @var string
     *
     * @ORM\Column(name="tapis", type="string", length=255, nullable=true )
     */
    private $tapis;

    /**
     * @var string
     *
     * @ORM\Column(name="volant", type="string", length=255, nullable=true )
     */
    private $volant;

    /**
     * @var string
     *
     * @ORM\Column(name="antiBrouillard", type="string", length=255, nullable=true )
     */
    private $antiBrouillard;

    /**
     * @var string
     *
     * @ORM\Column(name="bextvitres", type="string", length=255, nullable=true )
     */
    private $bextvitres;

    /**
     * @var string
     *
     * @ORM\Column(name="boulonsRoueProtege", type="string", length=255, nullable=true )
     */
    private $boulonsRoueProtege;

    /**
     * @var string
     *
     * @ORM\Column(name="feuxPositionLED", type="string", length=255, nullable=true )
     */
    private $feuxPositionLED;

    /**
     * @var string
     *
     * @ORM\Column(name="jantes", type="string", length=255, nullable=true )
     */
    private $jantes;

    /**
     * @var string
     *
     * @ORM\Column(name="peinture", type="string", length=255, nullable=true )
     */
    private $peinture;

    /**
     * @var string
     *
     * @ORM\Column(name="phares", type="string", length=255, nullable=true )
     */
    private $phares;

    /**
     * @var string
     *
     * @ORM\Column(name="radarOfParking", type="string", length=255, nullable=true )
     */
    private $radarOfParking;

    /**
     * @var bool
     *
     * @ORM\Column(name="allumageAutoDesFeux", type="string", length=255, nullable=true)
     */
    private $allumageAutoDesFeux;

    /**
     * @var string
     *
     * @ORM\Column(name="climatisation", type="string", length=255, nullable=true )
     */
    private $climatisation;

    /**
     * @var string
     *
     * @ORM\Column(name="detecteurDePluie", type="string", length=255, nullable=true )
     */
    private $detecteurDePluie;

    /**
     * @var string
     *
     * @ORM\Column(name="DirectionAssiste", type="string", length=255, nullable=true )
     */
    private $directionAssiste;

    /**
     * @var string
     *
     * @ORM\Column(name="fermetureCentralise", type="string", length=255, nullable=true )
     */
    private $fermetureCentralise;

    /**
     * @var string
     *
     * @ORM\Column(name="OrdinateurDeBord", type="string", length=255, nullable=true )
     */
    private $ordinateurDeBord;

    /**
     * @var string
     *
     * @ORM\Column(name="retroExterieur", type="string", length=255, nullable=true )
     */
    private $retroExterieur;

    /**
     * @var string
     *
     * @ORM\Column(name="siegeAvantReglable", type="string", length=255, nullable=true )
     */
    private $siegeAvantReglable;

    /**
     * @var string
     *
     * @ORM\Column(name="regulateurDeVitesse", type="string", length=255, nullable=true )
     */
    private $regulateurDeVitesse;

    /**
     * @var string
     *
     * @ORM\Column(name="vitresElectrique", type="string", length=255, nullable=true )
     */
    private $vitresElectrique;

    /**
     * @var string
     *
     * @ORM\Column(name="volantReglable", type="string", length=255, nullable=true )
     */
    private $volantReglable;


    /**
     * @ORM\ManyToOne(targetEntity="NewCar")
     * @ORM\JoinColumn(name="car_id",referencedColumnName="id",onDelete="CASCADE")
     */
    private $car;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set disponibility.
     *
     * @param bool $disponibility
     *
     * @return Version
     */
    public function setDisponibility($disponibility)
    {
        $this->disponibility = $disponibility;

        return $this;
    }

    /**
     * Get disponibility.
     *
     * @return bool
     */
    public function getDisponibility()
    {
        return $this->disponibility;
    }

    /**
     * Set guarantee.
     *
     * @param int $guarantee
     *
     * @return Version
     */
    public function setGuarantee($guarantee)
    {
        $this->guarantee = $guarantee;

        return $this;
    }

    /**
     * Get guarantee.
     *
     * @return int
     */
    public function getGuarantee()
    {
        return $this->guarantee;
    }

    /**
     * Set body.
     *
     * @param string $body
     *
     * @return Version
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body.
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set nbSit.
     *
     * @param int $nbSit
     *
     * @return Version
     */
    public function setNbSit($nbSit)
    {
        $this->nbSit = $nbSit;

        return $this;
    }

    /**
     * Get nbSit.
     *
     * @return int
     */
    public function getNbSit()
    {
        return $this->nbSit;
    }

    /**
     * Set nbDoor.
     *
     * @param int $nbDoor
     *
     * @return Version
     */
    public function setNbDoor($nbDoor)
    {
        $this->nbDoor = $nbDoor;

        return $this;
    }

    /**
     * Get nbDoor.
     *
     * @return int
     */
    public function getNbDoor()
    {
        return $this->nbDoor;
    }

    /**
     * Set nbCylindre.
     *
     * @param int $nbCylindre
     *
     * @return Version
     */
    public function setNbCylindre($nbCylindre)
    {
        $this->nbCylindre = $nbCylindre;

        return $this;
    }

    /**
     * Get nbCylindre.
     *
     * @return int
     */
    public function getNbCylindre()
    {
        return $this->nbCylindre;
    }

    /**
     * Set energie.
     *
     * @param string $energie
     *
     * @return Version
     */
    public function setEnergie($energie)
    {
        $this->energie = $energie;

        return $this;
    }

    /**
     * Get energie.
     *
     * @return string
     */
    public function getEnergie()
    {
        return $this->energie;
    }

    /**
     * Set puissanceFiscale.
     *
     * @param int $puissanceFiscale
     *
     * @return Version
     */
    public function setPuissanceFiscale($puissanceFiscale)
    {
        $this->puissanceFiscale = $puissanceFiscale;

        return $this;
    }

    /**
     * Get puissanceFiscale.
     *
     * @return int
     */
    public function getPuissanceFiscale()
    {
        return $this->puissanceFiscale;
    }

    /**
     * Set puissance.
     *
     * @param int $puissance
     *
     * @return Version
     */
    public function setPuissance($puissance)
    {
        $this->puissance = $puissance;

        return $this;
    }

    /**
     * Get puissance.
     *
     * @return int
     */
    public function getPuissance()
    {
        return $this->puissance;
    }

    /**
     * Set couple.
     *
     * @param string $couple
     *
     * @return Version
     */
    public function setCouple($couple)
    {
        $this->couple = $couple;

        return $this;
    }

    /**
     * Get couple.
     *
     * @return string
     */
    public function getCouple()
    {
        return $this->couple;
    }

    /**
     * Set cylindred.
     *
     * @param int $cylindred
     *
     * @return Version
     */
    public function setCylindred($cylindred)
    {
        $this->cylindred = $cylindred;

        return $this;
    }

    /**
     * Get cylindred.
     *
     * @return int
     */
    public function getCylindred()
    {
        return $this->cylindred;
    }

    /**
     * Set boite.
     *
     * @param string $boite
     *
     * @return Version
     */
    public function setBoite($boite)
    {
        $this->boite = $boite;

        return $this;
    }

    /**
     * Get boite.
     *
     * @return string
     */
    public function getBoite()
    {
        return $this->boite;
    }

    /**
     * Set nbRapport.
     *
     * @param int $nbRapport
     *
     * @return Version
     */
    public function setNbRapport($nbRapport)
    {
        $this->nbRapport = $nbRapport;

        return $this;
    }

    /**
     * Get nbRapport.
     *
     * @return int
     */
    public function getNbRapport()
    {
        return $this->nbRapport;
    }

    /**
     * Set transmission.
     *
     * @param string $transmission
     *
     * @return Version
     */
    public function setTransmission($transmission)
    {
        $this->transmission = $transmission;

        return $this;
    }

    /**
     * Get transmission.
     *
     * @return string
     */
    public function getTransmission()
    {
        return $this->transmission;
    }

    /**
     * Set length.
     *
     * @param int $length
     *
     * @return Version
     */
    public function setLength($length)
    {
        $this->length = $length;

        return $this;
    }

    /**
     * Get length.
     *
     * @return int
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * Set width.
     *
     * @param string $width
     *
     * @return Version
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width.
     *
     * @return string
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set height.
     *
     * @param int $height
     *
     * @return Version
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height.
     *
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set volumeCoffre.
     *
     * @param int $volumeCoffre
     *
     * @return Version
     */
    public function setVolumeCoffre($volumeCoffre)
    {
        $this->volumeCoffre = $volumeCoffre;

        return $this;
    }

    /**
     * Get volumeCoffre.
     *
     * @return int
     */
    public function getVolumeCoffre()
    {
        return $this->volumeCoffre;
    }

    /**
     * Set performanceKM.
     *
     * @param float $performanceKM
     *
     * @return Version
     */
    public function setPerformanceKM($performanceKM)
    {
        $this->performanceKM = $performanceKM;

        return $this;
    }

    /**
     * Get performanceKM.
     *
     * @return float
     */
    public function getPerformanceKM()
    {
        return $this->performanceKM;
    }

    /**
     * Set maxSpeed.
     *
     * @param int $maxSpeed
     *
     * @return Version
     */
    public function setMaxSpeed($maxSpeed)
    {
        $this->maxSpeed = $maxSpeed;

        return $this;
    }

    /**
     * Get maxSpeed.
     *
     * @return int
     */
    public function getMaxSpeed()
    {
        return $this->maxSpeed;
    }

    /**
     * Set urbanConsommation.
     *
     * @param float $urbanConsommation
     *
     * @return Version
     */
    public function setUrbanConsommation($urbanConsommation)
    {
        $this->urbanConsommation = $urbanConsommation;

        return $this;
    }

    /**
     * Get urbanConsommation.
     *
     * @return float
     */
    public function getUrbanConsommation()
    {
        return $this->urbanConsommation;
    }

    /**
     * Set extraUrbanConsommation.
     *
     * @param float $extraUrbanConsommation
     *
     * @return Version
     */
    public function setExtraUrbanConsommation($extraUrbanConsommation)
    {
        $this->extraUrbanConsommation = $extraUrbanConsommation;

        return $this;
    }

    /**
     * Get extraUrbanConsommation.
     *
     * @return float
     */
    public function getExtraUrbanConsommation()
    {
        return $this->extraUrbanConsommation;
    }

    /**
     * Set mixteConsommation.
     *
     * @param float $mixteConsommation
     *
     * @return Version
     */
    public function setMixteConsommation($mixteConsommation)
    {
        $this->mixteConsommation = $mixteConsommation;

        return $this;
    }

    /**
     * Get mixteConsommation.
     *
     * @return float
     */
    public function getMixteConsommation()
    {
        return $this->mixteConsommation;
    }

    /**
     * Set aBS.
     *
     * @param string $aBS
     *
     * @return Version
     */
    public function setABS($aBS)
    {
        $this->aBS = $aBS;

        return $this;
    }

    /**
     * Get aBS.
     *
     * @return string
     */
    public function getABS()
    {
        return $this->aBS;
    }

    /**
     * Set airbags.
     *
     * @param string $airbags
     *
     * @return Version
     */
    public function setAirbags($airbags)
    {
        $this->airbags = $airbags;

        return $this;
    }

    /**
     * Get airbags.
     *
     * @return string
     */
    public function getAirbags()
    {
        return $this->airbags;
    }

    /**
     * Set antiPatinage.
     *
     * @param string $antiPatinage
     *
     * @return Version
     */
    public function setAntiPatinage($antiPatinage)
    {
        $this->antiPatinage = $antiPatinage;

        return $this;
    }

    /**
     * Get antiPatinage.
     *
     * @return string
     */
    public function getAntiPatinage()
    {
        return $this->antiPatinage;
    }

    /**
     * Set antiDemarrageElectronic.
     *
     * @param string $antiDemarrageElectronic
     *
     * @return Version
     */
    public function setAntiDemarrageElectronic($antiDemarrageElectronic)
    {
        $this->antiDemarrageElectronic = $antiDemarrageElectronic;

        return $this;
    }

    /**
     * Get antiDemarrageElectronic.
     *
     * @return string
     */
    public function getAntiDemarrageElectronic()
    {
        return $this->antiDemarrageElectronic;
    }

    /**
     * Set fixationISOFIX.
     *
     * @param string $fixationISOFIX
     *
     * @return Version
     */
    public function setFixationISOFIX($fixationISOFIX)
    {
        $this->fixationISOFIX = $fixationISOFIX;

        return $this;
    }

    /**
     * Get fixationISOFIX.
     *
     * @return string
     */
    public function getFixationISOFIX()
    {
        return $this->fixationISOFIX;
    }

    /**
     * Set accoudoirs.
     *
     * @param string $accoudoirs
     *
     * @return Version
     */
    public function setAccoudoirs($accoudoirs)
    {
        $this->accoudoirs = $accoudoirs;

        return $this;
    }

    /**
     * Get accoudoirs.
     *
     * @return string
     */
    public function getAccoudoirs()
    {
        return $this->accoudoirs;
    }

    /**
     * Set autoradio.
     *
     * @param string $autoradio
     *
     * @return Version
     */
    public function setAutoradio($autoradio)
    {
        $this->autoradio = $autoradio;

        return $this;
    }

    /**
     * Get autoradio.
     *
     * @return string
     */
    public function getAutoradio()
    {
        return $this->autoradio;
    }

    /**
     * Set connectivity.
     *
     * @param string $connectivity
     *
     * @return Version
     */
    public function setConnectivity($connectivity)
    {
        $this->connectivity = $connectivity;

        return $this;
    }

    /**
     * Get connectivity.
     *
     * @return string
     */
    public function getConnectivity()
    {
        return $this->connectivity;
    }

    /**
     * Set screen.
     *
     * @param string $screen
     *
     * @return Version
     */
    public function setScreen($screen)
    {
        $this->screen = $screen;

        return $this;
    }

    /**
     * Get screen.
     *
     * @return string
     */
    public function getScreen()
    {
        return $this->screen;
    }

    /**
     * Set finitionIn.
     *
     * @param string $finitionIn
     *
     * @return Version
     */
    public function setFinitionIn($finitionIn)
    {
        $this->finitionIn = $finitionIn;

        return $this;
    }

    /**
     * Get finitionIn.
     *
     * @return string
     */
    public function getFinitionIn()
    {
        return $this->finitionIn;
    }

    /**
     * Set kitSmoking.
     *
     * @param string $kitSmoking
     *
     * @return Version
     */
    public function setKitSmoking($kitSmoking)
    {
        $this->kitSmoking = $kitSmoking;

        return $this;
    }

    /**
     * Get kitSmoking.
     *
     * @return string
     */
    public function getKitSmoking()
    {
        return $this->kitSmoking;
    }

    /**
     * Set kitSafety.
     *
     * @param string $kitSafety
     *
     * @return Version
     */
    public function setKitSafety($kitSafety)
    {
        $this->kitSafety = $kitSafety;

        return $this;
    }

    /**
     * Get kitSafety.
     *
     * @return string
     */
    public function getKitSafety()
    {
        return $this->kitSafety;
    }

    /**
     * Set sellerie.
     *
     * @param string $sellerie
     *
     * @return Version
     */
    public function setSellerie($sellerie)
    {
        $this->sellerie = $sellerie;

        return $this;
    }

    /**
     * Get sellerie.
     *
     * @return string
     */
    public function getSellerie()
    {
        return $this->sellerie;
    }

    /**
     * Set tapis.
     *
     * @param string $tapis
     *
     * @return Version
     */
    public function setTapis($tapis)
    {
        $this->tapis = $tapis;

        return $this;
    }

    /**
     * Get tapis.
     *
     * @return string
     */
    public function getTapis()
    {
        return $this->tapis;
    }

    /**
     * Set volant.
     *
     * @param string $volant
     *
     * @return Version
     */
    public function setVolant($volant)
    {
        $this->volant = $volant;

        return $this;
    }

    /**
     * Get volant.
     *
     * @return string
     */
    public function getVolant()
    {
        return $this->volant;
    }

    /**
     * Set antiBrouillard.
     *
     * @param string $antiBrouillard
     *
     * @return Version
     */
    public function setAntiBrouillard($antiBrouillard)
    {
        $this->antiBrouillard = $antiBrouillard;

        return $this;
    }

    /**
     * Get antiBrouillard.
     *
     * @return string
     */
    public function getAntiBrouillard()
    {
        return $this->antiBrouillard;
    }

    /**
     * Set bextvitres.
     *
     * @param string $bextvitres
     *
     * @return Version
     */
    public function setBextvitres($bextvitres)
    {
        $this->bextvitres = $bextvitres;

        return $this;
    }

    /**
     * Get bextvitres.
     *
     * @return string
     */
    public function getBextvitres()
    {
        return $this->bextvitres;
    }

    /**
     * Set boulonsRoueProtege.
     *
     * @param string $boulonsRoueProtege
     *
     * @return Version
     */
    public function setBoulonsRoueProtege($boulonsRoueProtege)
    {
        $this->boulonsRoueProtege = $boulonsRoueProtege;

        return $this;
    }

    /**
     * Get boulonsRoueProtege.
     *
     * @return string
     */
    public function getBoulonsRoueProtege()
    {
        return $this->boulonsRoueProtege;
    }

    /**
     * Set feuxPositionLED.
     *
     * @param string $feuxPositionLED
     *
     * @return Version
     */
    public function setFeuxPositionLED($feuxPositionLED)
    {
        $this->feuxPositionLED = $feuxPositionLED;

        return $this;
    }

    /**
     * Get feuxPositionLED.
     *
     * @return string
     */
    public function getFeuxPositionLED()
    {
        return $this->feuxPositionLED;
    }

    /**
     * Set jantes.
     *
     * @param string $jantes
     *
     * @return Version
     */
    public function setJantes($jantes)
    {
        $this->jantes = $jantes;

        return $this;
    }

    /**
     * Get jantes.
     *
     * @return string
     */
    public function getJantes()
    {
        return $this->jantes;
    }

    /**
     * Set peinture.
     *
     * @param string $peinture
     *
     * @return Version
     */
    public function setPeinture($peinture)
    {
        $this->peinture = $peinture;

        return $this;
    }

    /**
     * Get peinture.
     *
     * @return string
     */
    public function getPeinture()
    {
        return $this->peinture;
    }

    /**
     * Set phares.
     *
     * @param string $phares
     *
     * @return Version
     */
    public function setPhares($phares)
    {
        $this->phares = $phares;

        return $this;
    }

    /**
     * Get phares.
     *
     * @return string
     */
    public function getPhares()
    {
        return $this->phares;
    }

    /**
     * Set radarOfParking.
     *
     * @param string $radarOfParking
     *
     * @return Version
     */
    public function setRadarOfParking($radarOfParking)
    {
        $this->radarOfParking = $radarOfParking;

        return $this;
    }

    /**
     * Get radarOfParking.
     *
     * @return string
     */
    public function getRadarOfParking()
    {
        return $this->radarOfParking;
    }

    /**
     * Set allumageAutoDesFeux.
     *
     * @param bool $allumageAutoDesFeux
     *
     * @return Version
     */
    public function setAllumageAutoDesFeux($allumageAutoDesFeux)
    {
        $this->allumageAutoDesFeux = $allumageAutoDesFeux;

        return $this;
    }

    /**
     * Get allumageAutoDesFeux.
     *
     * @return bool
     */
    public function getAllumageAutoDesFeux()
    {
        return $this->allumageAutoDesFeux;
    }

    /**
     * Set climatisation.
     *
     * @param string $climatisation
     *
     * @return Version
     */
    public function setClimatisation($climatisation)
    {
        $this->climatisation = $climatisation;

        return $this;
    }

    /**
     * Get climatisation.
     *
     * @return string
     */
    public function getClimatisation()
    {
        return $this->climatisation;
    }

    /**
     * Set detecteurDePluie.
     *
     * @param string $detecteurDePluie
     *
     * @return Version
     */
    public function setDetecteurDePluie($detecteurDePluie)
    {
        $this->detecteurDePluie = $detecteurDePluie;

        return $this;
    }

    /**
     * Get detecteurDePluie.
     *
     * @return string
     */
    public function getDetecteurDePluie()
    {
        return $this->detecteurDePluie;
    }

    /**
     * Set directionAssiste.
     *
     * @param string $directionAssiste
     *
     * @return Version
     */
    public function setDirectionAssiste($directionAssiste)
    {
        $this->directionAssiste = $directionAssiste;

        return $this;
    }

    /**
     * Get directionAssiste.
     *
     * @return string
     */
    public function getDirectionAssiste()
    {
        return $this->directionAssiste;
    }

    /**
     * Set fermetureCentralise.
     *
     * @param string $fermetureCentralise
     *
     * @return Version
     */
    public function setFermetureCentralise($fermetureCentralise)
    {
        $this->fermetureCentralise = $fermetureCentralise;

        return $this;
    }

    /**
     * Get fermetureCentralise.
     *
     * @return string
     */
    public function getFermetureCentralise()
    {
        return $this->fermetureCentralise;
    }

    /**
     * Set ordinateurDeBord.
     *
     * @param string $ordinateurDeBord
     *
     * @return Version
     */
    public function setOrdinateurDeBord($ordinateurDeBord)
    {
        $this->ordinateurDeBord = $ordinateurDeBord;

        return $this;
    }

    /**
     * Get ordinateurDeBord.
     *
     * @return string
     */
    public function getOrdinateurDeBord()
    {
        return $this->ordinateurDeBord;
    }

    /**
     * Set retroExterieur.
     *
     * @param string $retroExterieur
     *
     * @return Version
     */
    public function setRetroExterieur($retroExterieur)
    {
        $this->retroExterieur = $retroExterieur;

        return $this;
    }

    /**
     * Get retroExterieur.
     *
     * @return string
     */
    public function getRetroExterieur()
    {
        return $this->retroExterieur;
    }

    /**
     * Set siegeAvantReglable.
     *
     * @param string $siegeAvantReglable
     *
     * @return Version
     */
    public function setSiegeAvantReglable($siegeAvantReglable)
    {
        $this->siegeAvantReglable = $siegeAvantReglable;

        return $this;
    }

    /**
     * Get siegeAvantReglable.
     *
     * @return string
     */
    public function getSiegeAvantReglable()
    {
        return $this->siegeAvantReglable;
    }

    /**
     * Set regulateurDeVitesse.
     *
     * @param string $regulateurDeVitesse
     *
     * @return Version
     */
    public function setRegulateurDeVitesse($regulateurDeVitesse)
    {
        $this->regulateurDeVitesse = $regulateurDeVitesse;

        return $this;
    }

    /**
     * Get regulateurDeVitesse.
     *
     * @return string
     */
    public function getRegulateurDeVitesse()
    {
        return $this->regulateurDeVitesse;
    }

    /**
     * Set vitresElectrique.
     *
     * @param string $vitresElectrique
     *
     * @return Version
     */
    public function setVitresElectrique($vitresElectrique)
    {
        $this->vitresElectrique = $vitresElectrique;

        return $this;
    }

    /**
     * Get vitresElectrique.
     *
     * @return string
     */
    public function getVitresElectrique()
    {
        return $this->vitresElectrique;
    }

    /**
     * Set volantReglable.
     *
     * @param string $volantReglable
     *
     * @return Version
     */
    public function setVolantReglable($volantReglable)
    {
        $this->volantReglable = $volantReglable;

        return $this;
    }

    /**
     * Get volantReglable.
     *
     * @return string
     */
    public function getVolantReglable()
    {
        return $this->volantReglable;
    }



    /**
     * Set car.
     *
     * @param \Sayara\BackendBundle\Entity\NewCar|null $car
     *
     * @return Version
     */
    public function setCar(\Sayara\BackendBundle\Entity\NewCar $car = null)
    {
        $this->car = $car;

        return $this;
    }

    /**
     * Get car.
     *
     * @return \Sayara\BackendBundle\Entity\NewCar|null
     */
    public function getCar()
    {
        return $this->car;
    }

    /**
     * Set libelle.
     *
     * @param string|null $libelle
     *
     * @return Version
     */
    public function setLibelle($libelle = null)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle.
     *
     * @return string|null
     */
    public function getLibelle()
    {
        return $this->libelle;
    }
}
