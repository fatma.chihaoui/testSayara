<?php

namespace Sayara\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="Sayara\BackendBundle\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotNull(message ="Veuillez indiquer la catégorie . ")
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="code", type="integer")
     */
    private $code;


    /**
     * @var bool
     *
     * @ORM\Column(name="parent", type="boolean", nullable=true)
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="Vehicle", mappedBy="category")
     */
    private $vehicles;

    /**
     * @ORM\OneToMany(targetEntity="Spare", mappedBy="category")
     */
    private $spares;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->vehicles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->spares = new \Doctrine\Common\Collections\ArrayCollection();
        $this->code= random_int(100,999);
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set parent.
     *
     * @param bool $parent
     *
     * @return Category
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent.
     *
     * @return bool
     */
    public function getParent()
    {
        return $this->parent;
    }


    /**
     * Add vehicle.
     *
     * @param \Sayara\BackendBundle\Entity\Vehicle $vehicle
     *
     * @return Category
     */
    public function addVehicle(\Sayara\BackendBundle\Entity\Vehicle $vehicle)
    {
        $this->vehicles[] = $vehicle;

        return $this;
    }

    /**
     * Remove vehicle.
     *
     * @param \Sayara\BackendBundle\Entity\Vehicle $vehicle
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeVehicle(\Sayara\BackendBundle\Entity\Vehicle $vehicle)
    {
        return $this->vehicles->removeElement($vehicle);
    }

    /**
     * Get vehicles.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVehicles()
    {
        return $this->vehicles;
    }

    /**
     * Add spare.
     *
     * @param \Sayara\BackendBundle\Entity\Spare $spare
     *
     * @return Category
     */
    public function addSpare(\Sayara\BackendBundle\Entity\Spare $spare)
    {
        $this->spares[] = $spare;

        return $this;
    }

    /**
     * Remove spare.
     *
     * @param \Sayara\BackendBundle\Entity\Spare $spare
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeSpare(\Sayara\BackendBundle\Entity\Spare $spare)
    {
        return $this->spares->removeElement($spare);
    }

    /**
     * Get spares.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSpares()
    {
        return $this->spares;
    }

    /**
     * Set code.
     *
     * @param int $code
     *
     * @return Category
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }
}
