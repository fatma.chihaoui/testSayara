<?php

namespace Sayara\BackendBundle\Controller;

use Sayara\BackendBundle\Entity\SearchSpare;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Searchspare controller.
 *
 * @Route("searchspare")
 */
class SearchSpareController extends Controller
{
    /**
     * Lists all searchSpare entities.
     *
     * @Route("/", name="searchspare_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $searchSpares = $em->getRepository('BackendBundle:SearchSpare')->findAll();

        return $this->render('searchspare/index.html.twig', array(
            'searchSpares' => $searchSpares,
        ));
    }

    /**
     * Finds and displays a searchSpare entity.
     *
     * @Route("/{id}", name="searchspare_show")
     * @Method("GET")
     */
    public function showAction(SearchSpare $searchSpare)
    {

        return $this->render('searchspare/show.html.twig', array(
            'searchSpare' => $searchSpare,
        ));
    }
}
