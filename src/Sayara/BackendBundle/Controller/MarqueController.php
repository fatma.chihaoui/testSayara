<?php

namespace Sayara\BackendBundle\Controller;

use Sayara\BackendBundle\Entity\Marque;
use Sayara\BackendBundle\Entity\Photo;
use Sayara\BackendBundle\Entity\Vehicle;
use Sayara\FrontendBundle\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Vich\UploaderBundle\Entity\File;


/**
 * Marque controller.
 * @Security("has_role('ROLE_ADMIN')")
 * @Route("/admin/marque")
 */
class MarqueController extends Controller
{
    private $fileUploader;

    public function __construct(FileUploader $fileUploader)
    {
        $this->fileUploader= $fileUploader;
    }
    /**
     * Lists all marque entities.
     *
     * @Route("/", name="marque_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $marques = $em->getRepository('BackendBundle:Marque')->findAll();

        return $this->render('marque/index.html.twig', array(
            'marques' => $marques,
        ));
    }

    /**
     * Creates a new marque entity.
     *
     * @Route("/new", name="marque_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $marques= $this->getDoctrine()->getManager()->getRepository(Marque::class)->findAll();
        $marque = new Marque();
        $form = $this->createForm('Sayara\BackendBundle\Form\MarqueType', $marque);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data= $form->getData();

            $file= $data->getPhoto()->getPath();
            if($file!== null){
                $fileName = $this->fileUploader->upload($file);

                $data->getPhoto()->setPath($fileName);
                $data->getPhoto()->setName("autoplus" . $data->getLibelle());
            }else{
                $data->setPhoto(null);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($marque);
            $em->flush();

            return $this->redirectToRoute('marque_new');
        }

        return $this->render('marque/new.html.twig', array(
            'marque' => $marque,
            'form' => $form->createView(),
            'marques'=>$marques
        ));
    }

    /**
     * Finds and displays a marque entity.
     *
     * @Route("/{id}", name="marque_show")
     * @Method("GET")
     */
    public function showAction(Marque $marque)
    {


        return $this->render('marque/show.html.twig', array(
            'marque' => $marque,
        ));
    }

    /**
     * Displays a form to edit an existing marque entity.
     *
     * @Route("/{id}/edit", name="marque_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Marque $marque)
    {

        $editForm = $this->createForm('Sayara\BackendBundle\Form\MarqueType', $marque);
        $photo = null;
        if($marque->getPhoto()){
            $photo = new Photo();
            $photo->setPath($marque->getPhoto()->getPath());
            $photo->setName($marque->getPhoto()->getName());
        }
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $data= $editForm->getData();
            //$data->setPhoto($photo);

            $file= $data->getPhoto()->getPath();

                         if($file!== null){
                             //die;
                             $fileName = $this->fileUploader->upload($file);

                             $data->getPhoto()->setPath($fileName);
                             $data->getPhoto()->setName("autoplus" . $data->getLibelle());
                         }else{
                             if($photo)
                                $data->setPhoto($photo);
                         }


                       $em = $this->getDoctrine()->getManager();
                       $em->persist($marque);
                       $em->flush();

            return $this->redirectToRoute('marque_edit', array('id' => $marque->getId()));
        }

        return $this->render('marque/edit.html.twig', array(
            'marque' => $marque,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a marque entity.
     *
     * @Route("/delete-marque", name="marque_delete")
     * @Method({"GET", "POST"})
     */
    public function deleteMarqueAction(Request $request)
    {

        $marque =$this->getDoctrine()->getManager()->getRepository(Marque::class)->find($request->get('id'));
        $this->getDoctrine()->getManager()->remove($marque);
        $this->getDoctrine()->getManager()->flush();


        return new JsonResponse("delete marque");
    }

}
