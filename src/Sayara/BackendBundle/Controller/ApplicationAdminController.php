<?php

namespace Sayara\BackendBundle\Controller;

use Sayara\BackendBundle\Entity\Application;
use Sayara\BackendBundle\Entity\Photo;
use Sayara\BackendBundle\Entity\Spare;
use Sayara\BackendBundle\Entity\Vehicle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ApplicationAdminController
 * @package Sayara\BackendBundle\Controller
 * @Route("/admin/application")
 * @Security("has_role('ROLE_ADMIN')")
 */
class ApplicationAdminController extends Controller
{

    /**
     * @param Request $request
     * @Route("/particulars", name="application_particulars_index")
     * @Method("GET")
     * @return Response
     */
    public function listeParticularApplicationAction(Request $request){
        $em=$this->getDoctrine()->getManager();
        $all=$em->createQuery("SELECT u FROM BackendBundle:User u WHERE u.roles  LIKE '%ROLE_PARTICULIER%'");
        $particulars = $all->getResult();

        $results=[];
        foreach ($particulars as $particular){
            if($particular->getVehicles() !== null){
                $applications= $this->getDoctrine()->getManager()->getRepository(Vehicle::class)->findByUser($particular);
                if($applications !== []){$results = $applications;}

            }

        }
        foreach ($results as $result) {

            $photos = $em->getRepository(Photo::class)->findByVehicle($result->getId());

            foreach ($photos as $photo) {
                $result->addPhoto($photo);
            }
        }
        $resultsSpare=[];
        foreach ($particulars as $particular){
            if($particular->getSpares() !== null){
                $spares= $this->getDoctrine()->getManager()->getRepository(Spare::class)->findByUser($particular);
                if($spares !== []){$resultsSpare = $spares;}

            }

        }
        foreach ($resultsSpare as $spare) {

            $photos = $em->getRepository(Photo::class)->findBySpare($spare->getId());

            foreach ($photos as $photo) {
                $spare->addPhoto($photo);
            }
        }

        return $this->render('Backend/application_particulars.html.twig', array(
            'applications' => $results,
            'spares'=>$resultsSpare
        ));

    }

    /**
     * @param Request $request
     * @Route("/pros-list", name="application_pro_index")
     * @Method("GET")
     * @return Response
     */
    public function listProApplicationAction(Request $request){
        $em=$this->getDoctrine()->getManager();
        $all=$em->createQuery("SELECT u FROM BackendBundle:User u WHERE u.roles  LIKE '%ROLE_PRO%'");
        $pros = $all->getResult();
        $resultsPro=[];
        foreach ($pros as $pro){
            if($pro->getVehicles() !== null){
                $applications = $this->getDoctrine()->getManager()->getRepository(Vehicle::class)->findByUser($pro);

                if($applications !== []){$resultsPro = $applications;}

            }

        }
        foreach ($resultsPro as $result){

            $photos= $em->getRepository(Photo::class)->findByVehicle($result->getId());

            foreach ($photos as $photo){
                $result->addPhoto($photo);
            }

        }
        return $this->render('Backend/application_pros.html.twig', array(
            'applications' => $resultsPro,
        ));

    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/app-verifying", name="application_verifying")
     * @Method({"POST","GET"})
     */
    public function verifyApplicationAction(Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $vehicle =$em->getRepository(Vehicle::class)->find($request->get('app_id'));
        return $this->render("Backend/_modal_show_application.html.twig",array("vehicle"=>$vehicle));
    }

    /**
     * @param Request $request
     * @Route("/photos", name="application_photos")
     * @Method({"POST","GET"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function displayPhotosAction(Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $application = $em->getRepository(Vehicle::class)->find($request->get("id"));
        $photos= $em->getRepository(Photo::class)->findByVehicle($application);
        foreach ($photos as $photo){
            $application->addPhoto($photo);
        }

        return $this->render('Backend/_modal_displaying_photos.html.twig',array("photos"=>$application->getPhotos(),"id"=>$request->get("id")));
    }


    /**
     * @param Request $request
     * @return Response
     * @Route("/published", name="application_public")
     * @Method({"POST","GET"})
     */
    public function putAsPublicAction(Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $vehicle = $em->getRepository(Vehicle::class)->find($request->get("id"));
        $vehicle->setIsPublic(true);
        $em->flush();
        return new Response("Success");

    }
    /**
     * @param Request $request
     * @return Response
     * @Route("/hide", name="application_hide")
     * @Method({"POST","GET"})
     */
    public function putAsHiddenAction(Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $vehicle = $em->getRepository(Vehicle::class)->find($request->get("id"));
        $vehicle->setIsPublic(false);
        $em->flush();
        return new Response("Success");

    }


    /**
     * @param Request $request
     * @return Response
     * @Route("/delete-application", name="application_delete")
     * @Method({"POST","GET"})
     */
    public function deleteApplicationAction(Request $request)
    {
        $vehicle = $this->getDoctrine()->getManager()->getRepository(Vehicle::class)->find($request->get("id"));

        $this->getDoctrine()->getManager()->remove($vehicle);
        $this->getDoctrine()->getManager()->flush();
        return new Response("Success");
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/validate-photos-application", name="application_validatePhotos")
     * @Method({"POST","GET"})
     */
    public function validateApplicationPhotosAction(Request $request)
    {
        $vehicle = $this->getDoctrine()->getManager()->getRepository(Vehicle::class)->find($request->get("app_id"));
        $ids =$request->get("ids");
        $photosToDelete=[];
        foreach ($ids as $id){
            $photo= $this->getDoctrine()->getManager()->getRepository(Photo::class)->find($id);
            $photosToDelete [] = $photo;
        }foreach ($photosToDelete as $item){
        $this->getDoctrine()->getManager()->remove($item);
    }
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse(array('success' => true, 'media' => $ids ));
    }


    /**
     * @param Request $request
     * @Route("/spares-particulars", name="spare_particulars_index")
     * @Method("GET")
     * @return Response
     */
    public function listeParticularSpareAction(Request $request){
        $em=$this->getDoctrine()->getManager();
        $all=$em->createQuery("SELECT u FROM BackendBundle:User u WHERE u.roles  LIKE '%ROLE_PARTICULIER%'");
        $particulars = $all->getResult();
        $resultsSparePart=[];
        foreach ($particulars as $particular){
            if($particular->getSpares() !== null){
                $spares= $this->getDoctrine()->getManager()->getRepository(Spare::class)->findByUser($particular);
                if($spares !== []){$resultsSparePart = $spares;}

            }

        }
        foreach ($resultsSparePart as $result) {

            $photos = $em->getRepository(Photo::class)->findBySpare($result->getId());

            foreach ($photos as $photo) {
                $result->addPhoto($photo);
            }
        }


        return $this->render('Backend/spare_particulars.html.twig', array(

            'spares'=>$resultsSparePart
        ));

    }

    /**
     * @param Request $request
     * @Route("/spare/pros-list", name="spare_pro_index")
     * @Method("GET")
     * @return Response
     */
    public function listProSpareAction(Request $request){
        $em=$this->getDoctrine()->getManager();
        $all=$em->createQuery("SELECT u FROM BackendBundle:User u WHERE u.roles  LIKE '%ROLE_PRO%'");
        $pros = $all->getResult();
        $resultSparePro=[];
        foreach ($pros as $pro){
            if($pro->getSpares() !== null){
                $spares = $this->getDoctrine()->getManager()->getRepository(Spare::class)->findByUser($pro);

                if($spares !== []){$resultSparePro = $spares;}

            }

        }
        foreach ($resultSparePro as $result){

            $photos= $em->getRepository(Photo::class)->findBySpare($result->getId());

            foreach ($photos as $photo){
                $result->addPhoto($photo);
            }

        }
        return $this->render('Backend/spare_pros.html.twig', array(
            'spares' => $resultSparePro,
        ));

    }
    /**
     * @param Request $request
     * @Route("/spare-photos", name="spare_photos")
     * @Method({"POST","GET"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function displaySparePhotosAction(Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $spare = $em->getRepository(Spare::class)->find($request->get("id"));
        $photos= $em->getRepository(Photo::class)->findBySpare($spare);
        foreach ($photos as $photo){
            $spare->addPhoto($photo);
        }

        return $this->render('Backend/_modal_displaying_photos.html.twig',array("photos"=>$spare->getPhotos(),"id"=>$request->get("id")));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/spare-app-verifying", name="spare_verifying")
     * @Method({"POST","GET"})
     */
    public function verifySpareApplicationAction(Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $spare =$em->getRepository(Spare::class)->find($request->get('app_id'));
        return $this->render("Backend/_modal_show_spare.html.twig",array("spare"=>$spare));
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/spare-published", name="spare_public")
     * @Method({"POST","GET"})
     */
    public function putSpareAsPublicAction(Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $spare = $em->getRepository(Spare::class)->find($request->get("id"));
        $spare->setIsPublic(true);
        $em->flush();
        return new Response("Success");

    }
    /**
     * @param Request $request
     * @return Response
     * @Route("/spare-hide", name="spare_hide")
     * @Method({"POST","GET"})
     */
    public function putSpareAsHiddenAction(Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $spare = $em->getRepository(Spare::class)->find($request->get("id"));
        $spare->setIsPublic(false);
        $em->flush();
        return new Response("Success");

    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/validate-photos-spare", name="spare_validatePhotos")
     * @Method({"POST","GET"})
     */
    public function validateSparePhotosAction(Request $request)
    {
        $spare = $this->getDoctrine()->getManager()->getRepository(Spare::class)->find($request->get("app_id"));
        $ids =$request->get("ids");
        $photosToDelete=[];
        foreach ($ids as $id){
            $photo= $this->getDoctrine()->getManager()->getRepository(Photo::class)->find($id);
            $photosToDelete [] = $photo;
        }foreach ($photosToDelete as $item){
            $this->getDoctrine()->getManager()->remove($item);
        }
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse(array('success' => true, 'media' => $ids ));
    }


    /**
     * @param Request $request
     * @return Response
     * @Route("/delete-spare-application", name="spare_delete")
     * @Method({"POST","GET"})
     */
    public function deleteSpareAction(Request $request)
    {
        $spare = $this->getDoctrine()->getManager()->getRepository(Spare::class)->find($request->get("id"));

        $this->getDoctrine()->getManager()->remove($spare);
        $this->getDoctrine()->getManager()->flush();
        return new Response("Success");
    }

    /**
     * @Route("/delete-uploaded-photo-admin", name="delete_uploaded_photo_admin")
     * @Method({"GET", "POST"})
     */
    public function deleteUploadedPhotoAction(Request $request){
        $file_id= $request->get('deleted_photo');

        $em= $this->getDoctrine()->getManager();
        //$photo= $em->getRepository(Photo::class)->findOneBy(['name'=>$file]);
        $toDelete= $em->getRepository(Photo::class)->findOneBy(['id'=>$file_id]);


        $em->remove($toDelete);
        $em->flush();

        return new JsonResponse(array('success' => true, 'photo' => $toDelete->getName() ));
    }






}
