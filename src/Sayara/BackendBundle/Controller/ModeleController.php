<?php

namespace Sayara\BackendBundle\Controller;

use Sayara\BackendBundle\Entity\Modele;
use Sayara\BackendBundle\Entity\Photo;
use Sayara\FrontendBundle\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Modele controller.
 *
 * @Route("/admin/modele")
 */
class ModeleController extends Controller
{
    private $fileUploader;

    public function __construct(FileUploader $fileUploader)
    {
        $this->fileUploader= $fileUploader;
    }
    /**
     * Lists all modele entities.
     *
     * @Route("/", name="modele_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $modeles = $em->getRepository('BackendBundle:Modele')->findAll();

        return $this->render('modele/index.html.twig', array(
            'modeles' => $modeles,
        ));
    }

    /**
     * Creates a new modele entity.
     *
     * @Route("/new", name="modele_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $modele = new Modele();
        $modeles= $this->getDoctrine()->getManager()->getRepository(Modele::class)->findAll();
        $form = $this->createForm('Sayara\BackendBundle\Form\ModeleType', $modele);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $data= $form->getData();

            $file= $data->getPhoto()->getPath();
            if($file!== null){
                $fileName = $this->fileUploader->upload($file);

                $data->getPhoto()->setPath($fileName);
                $data->getPhoto()->setName("autoplus" . $data->getLibelle());
            }else{
                $data->setPhoto(null);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($modele);
            $em->flush();

            return $this->redirectToRoute('modele_new');
        }

        return $this->render('modele/new.html.twig', array(
            'modele' => $modele,
            'form' => $form->createView(),
            'modeles'=> $modeles
        ));
    }

    /**
     * Finds and displays a modele entity.
     *
     * @Route("/{id}", name="modele_show")
     * @Method("GET")
     */
    public function showAction(Modele $modele)
    {
        return $this->render('modele/show.html.twig', array(
            'modele' => $modele,
        ));
    }

    /**
     * Displays a form to edit an existing modele entity.
     *
     * @Route("/{id}/edit", name="modele_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Modele $modele)
    {
        $editForm = $this->createForm('Sayara\BackendBundle\Form\ModeleType', $modele);
        $photo = null;
        if($modele->getPhoto()){
            $photo = new Photo();
            $photo->setPath($modele->getPhoto()->getPath());
            $photo->setName($modele->getPhoto()->getName());
        }
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            if ($editForm->isSubmitted() && $editForm->isValid()) {

                $data= $editForm->getData();

                $file= $data->getPhoto()->getPath();
                if($file!== null){
                    $fileName = $this->fileUploader->upload($file);

                    $data->getPhoto()->setPath($fileName);
                    $data->getPhoto()->setName("autoplus" . $data->getLibelle());
                }else{
                    $data->setPhoto($photo);
                }

                $em = $this->getDoctrine()->getManager();
                $em->persist($modele);
                $em->flush();

                return $this->redirectToRoute('modele_new');
            }



            return $this->redirectToRoute('modele_edit', array('id' => $modele->getId()));
        }

        return $this->render('modele/edit.html.twig', array(
            'modele' => $modele,
            'edit_form' => $editForm->createView(),
        ));
    }


    /**
     * Deletes a modele entity.
     *
     * @Route("/delete-modele", name="modele_delete")
     * @Method({"GET", "POST"})
     */
    public function deleteModeleAction(Request $request)
    {
        $modele=$this->getDoctrine()->getManager()->getRepository(Modele::class)->find($request->get('id'));
        $this->getDoctrine()->getManager()->remove($modele);
        $this->getDoctrine()->getManager()->flush();


        return new JsonResponse("delete marque");
    }

}
