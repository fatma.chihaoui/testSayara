<?php

namespace Sayara\BackendBundle\Controller;

use Sayara\BackendBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/admin")
 * @Security("has_role('ROLE_ADMIN')")
 */
class UserController extends Controller
{

    /**
     * @Route("/list-particular", name="particular-list")
     *
     */
    public function allParticulierAction(Request $request){

        $em=$this->getDoctrine()->getManager();
        $all=$em->createQuery("SELECT u FROM BackendBundle:User u WHERE u.roles  LIKE '%ROLE_PARTICULIER%'");

        $paginator=$this->get('knp_paginator');
        $particulars=$paginator->paginate(
            $all,
            $request->query->get('page', 1),
            $request->query->get('limit',5)
        );
        return $this->render("Backend/list_particulars.html.twig",array('users'=>$particulars));
    }

    /**
     * @Route("/list-pro", name="pro-list")
     *
     */
    public function allProAction(Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $all=$em->createQuery("SELECT u FROM BackendBundle:User u WHERE u.roles  LIKE '%ROLE_PRO%'");

        $paginator=$this->get('knp_paginator');
        $pros=$paginator->paginate(
            $all,
            $request->query->get('page', 1),
            $request->query->get('limit', 5)
        );
        return $this->render("Backend/list_pro.html.twig",array('users'=>$pros));
    }

    /**
     * @Route("/delete-photo-profile", name="photo-profile-delete")
     *
     */
    public function deletePhotoProfileAction(Request $request){
        $user=$this->getDoctrine()->getManager()->getRepository(User::class)->find($request->get('id'));

        $user->getPhoto()->setPath(null);
        $this->getDoctrine()->getManager()->flush();
        return new Response("succes delete");

    }


    /**
     * @Route("/edit-profile", name="profile-edit")
     *
     * @Method("POST")
     */
    public function editUserAction(Request $request){
        $id=$request->get('id');
        $user=$this->getDoctrine()->getManager()->getRepository(User::class)->find($id);
        $user->setFirstname($request->get('firstname'));
        $user->setLastname($request->get('lastname'));
        $user->setGender($request->get('gender'));
        
        $user->setPhone($request->get('phone'));
        $this->getDoctrine()->getManager()->persist($user);
        $this->getDoctrine()->getManager()->flush();
        return new Response("Success");
    }

    /**
     * @Route("/delete-user", name="profile-delete")
     *
     */
    public function deleteUserAction(Request $request)
    {
        $user=$this->getDoctrine()->getManager()->getRepository(User::class)->find($request->get("id"));
        $this->getDoctrine()->getManager()->remove($user);
        $this->getDoctrine()->getManager()->flush();
        return new Response('Success sup user');

    }
    /**
     * @Route("/state-user", name="edit_state_user")
     *
     */
    public function enableUserAction(Request $request)
    {
        $user=$this->getDoctrine()->getManager()->getRepository(User::class)->find($request->get("id"));
        if($user->isEnabled()){

            $user->setEnabled(false);
        }else{
            $user->setEnabled(true);
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('Success sup user');

    }











}
