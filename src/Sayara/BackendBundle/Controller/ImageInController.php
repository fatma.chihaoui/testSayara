<?php

namespace Sayara\BackendBundle\Controller;

use Sayara\BackendBundle\Entity\ImageIn;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Imagein controller.
 *
 * @Route("imagein")
 */
class ImageInController extends Controller
{
    /**
     * Lists all imageIn entities.
     *
     * @Route("/", name="imagein_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $imageIns = $em->getRepository('BackendBundle:ImageIn')->findAll();

        return $this->render('imagein/index.html.twig', array(
            'imageIns' => $imageIns,
        ));
    }

    /**
     * Creates a new imageIn entity.
     *
     * @Route("/new", name="imagein_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $imageIn = new Imagein();
        $form = $this->createForm('Sayara\BackendBundle\Form\ImageInType', $imageIn);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($imageIn);
            $em->flush();

            return $this->redirectToRoute('imagein_show', array('id' => $imageIn->getId()));
        }

        return $this->render('imagein/new.html.twig', array(
            'imageIn' => $imageIn,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a imageIn entity.
     *
     * @Route("/{id}", name="imagein_show")
     * @Method("GET")
     */
    public function showAction(ImageIn $imageIn)
    {
        $deleteForm = $this->createDeleteForm($imageIn);

        return $this->render('imagein/show.html.twig', array(
            'imageIn' => $imageIn,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing imageIn entity.
     *
     * @Route("/{id}/edit", name="imagein_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, ImageIn $imageIn)
    {
        $deleteForm = $this->createDeleteForm($imageIn);
        $editForm = $this->createForm('Sayara\BackendBundle\Form\ImageInType', $imageIn);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('imagein_edit', array('id' => $imageIn->getId()));
        }

        return $this->render('imagein/edit.html.twig', array(
            'imageIn' => $imageIn,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a imageIn entity.
     *
     * @Route("/{id}", name="imagein_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, ImageIn $imageIn)
    {
        $form = $this->createDeleteForm($imageIn);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($imageIn);
            $em->flush();
        }

        return $this->redirectToRoute('imagein_index');
    }

    /**
     * Creates a form to delete a imageIn entity.
     *
     * @param ImageIn $imageIn The imageIn entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ImageIn $imageIn)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('imagein_delete', array('id' => $imageIn->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
