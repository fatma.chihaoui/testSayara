<?php

namespace Sayara\BackendBundle\Controller;

use Sayara\BackendBundle\Entity\ImageOut;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Imageout controller.
 *
 * @Route("imageout")
 */
class ImageOutController extends Controller
{
    /**
     * Lists all imageOut entities.
     *
     * @Route("/", name="imageout_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $imageOuts = $em->getRepository('BackendBundle:ImageOut')->findAll();

        return $this->render('imageout/index.html.twig', array(
            'imageOuts' => $imageOuts,
        ));
    }

    /**
     * Creates a new imageOut entity.
     *
     * @Route("/new", name="imageout_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $imageOut = new Imageout();
        $form = $this->createForm('Sayara\BackendBundle\Form\ImageOutType', $imageOut);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($imageOut);
            $em->flush();

            return $this->redirectToRoute('imageout_show', array('id' => $imageOut->getId()));
        }

        return $this->render('imageout/new.html.twig', array(
            'imageOut' => $imageOut,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a imageOut entity.
     *
     * @Route("/{id}", name="imageout_show")
     * @Method("GET")
     */
    public function showAction(ImageOut $imageOut)
    {
        $deleteForm = $this->createDeleteForm($imageOut);

        return $this->render('imageout/show.html.twig', array(
            'imageOut' => $imageOut,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing imageOut entity.
     *
     * @Route("/{id}/edit", name="imageout_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, ImageOut $imageOut)
    {
        $deleteForm = $this->createDeleteForm($imageOut);
        $editForm = $this->createForm('Sayara\BackendBundle\Form\ImageOutType', $imageOut);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('imageout_edit', array('id' => $imageOut->getId()));
        }

        return $this->render('imageout/edit.html.twig', array(
            'imageOut' => $imageOut,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a imageOut entity.
     *
     * @Route("/{id}", name="imageout_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, ImageOut $imageOut)
    {
        $form = $this->createDeleteForm($imageOut);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($imageOut);
            $em->flush();
        }

        return $this->redirectToRoute('imageout_index');
    }

    /**
     * Creates a form to delete a imageOut entity.
     *
     * @param ImageOut $imageOut The imageOut entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ImageOut $imageOut)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('imageout_delete', array('id' => $imageOut->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
