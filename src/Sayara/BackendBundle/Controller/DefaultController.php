<?php

namespace Sayara\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


/**
 * Class DefaultController
 * @package Sayara\BackendBundle\Controller
 *
 *
 */
class DefaultController extends Controller
{
    /**
     * @Route("/admin", name="adminpage")
     */
    public function indexAction()
    {
        return $this->render('Backend/dashbord.html.twig');
    }


}
