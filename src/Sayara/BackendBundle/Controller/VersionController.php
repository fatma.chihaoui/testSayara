<?php

namespace Sayara\BackendBundle\Controller;

use Sayara\BackendBundle\Entity\NewCar;
use Sayara\BackendBundle\Entity\Version;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\SecurityBundle\Tests\Functional\Bundle\AclBundle\Entity\Car;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Version controller.
 *
 * @Route("/admin/version")
 * @Security("has_role('ROLE_ADMIN')")
 */
class VersionController extends Controller
{
    /**
     * Lists all version entities.
     *
     * @Route("/", name="version_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $versions = $em->getRepository('BackendBundle:Version')->findAll();

        return $this->render('version/index.html.twig', array(
            'versions' => $versions,
        ));
    }
    /**
     * Creates a new version entity.
     *
     * @Route("/new/{id}", name="version_new_selected")
     * @Method({"GET", "POST"})
     */
    public function newVersionCarAction(Request $request, NewCar $newCar)
    {
        $version = new Version();
        $form = $this->createForm('Sayara\BackendBundle\Form\VersionType', $version);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $version->setCar($newCar);
            $em->persist($version);
            $em->flush();

            return $this->redirectToRoute('new_car_index');
        }

        return $this->render('version/new_selected_car.html.twig', array(
            'version' => $version,
            'newCar'=> $newCar,
            'form' => $form->createView(),
        ));
    }
    /**
     * Creates a new version entity.
     *
     * @Route("/new", name="version_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $version = new Version();
        $form = $this->createForm('Sayara\BackendBundle\Form\VersionType', $version);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($version);
            $em->flush();

            return $this->redirectToRoute('new_car_index');
        }

        return $this->render('version/new.html.twig', array(
            'version' => $version,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a version entity.
     *
     * @Route("/{id}", name="version_show")
     * @Method("GET")
     */
    public function showAction(Version $version)
    {
        $deleteForm = $this->createDeleteForm($version);

        return $this->render('version/show.html.twig', array(
            'version' => $version,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing version entity.
     *
     * @Route("/{id}/edit", name="version_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Version $version)
    {
        $deleteForm = $this->createDeleteForm($version);
        $editForm = $this->createForm('Sayara\BackendBundle\Form\VersionType', $version);
        $editForm->handleRequest($request);
        $em= $this->getDoctrine()->getManager();
        $car = $em->getRepository(NewCar::class)->findOneBy(['id'=>$version->getCar()->getId()]);
        $modele = $car->getModele();

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            //return $this->redirectToRoute('version_edit', array('id' => $version->getId()));
            return $this->redirectToRoute('new_car_index');
        }

        return $this->render('version/edit.html.twig', array(
            'modele' => $modele,
            'version' => $version,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a version entity.
     *
     * @Route("/{id}", name="version_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Version $version)
    {
        $form = $this->createDeleteForm($version);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($version);
            $em->flush();
        }

        return $this->redirectToRoute('version_index');
    }

    /**
     * Creates a form to delete a version entity.
     *
     * @param Version $version The version entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Version $version)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('version_delete', array('id' => $version->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Deletes a voiture entity.
     *
     * @Route("/delete-new-car", name="new_car_delete")
     * @Method({"GET", "POST"})
     */
    public function deleteModeleAction(Request $request)
    {
        $NewCar=$this->getDoctrine()->getManager()->getRepository(NewCar::class)->find($request->get('id'));
        $this->getDoctrine()->getManager()->remove($NewCar);
        $this->getDoctrine()->getManager()->flush();


        return new JsonResponse("delete marque");
    }
}
