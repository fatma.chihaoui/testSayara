<?php

namespace Sayara\BackendBundle\Controller;

use Sayara\BackendBundle\Entity\Modele;
use Sayara\BackendBundle\Entity\NewCar;
use Sayara\BackendBundle\Entity\Photo;
use Sayara\BackendBundle\Entity\Vehicle;
use Sayara\BackendBundle\Form\NewCarType;
use Sayara\FrontendBundle\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * Vehicle controller.
 * @Route("/admin/vehicle")
 * @Security("has_role('ROLE_ADMIN')")
 */
class VehicleController extends Controller
{
    private $fileUploader;

    public function __construct(FileUploader $fileUploader)
    {
        $this->fileUploader= $fileUploader;
    }
    /**
     * Lists all vehicle entities.
     *
     * @Route("/new-car-list", name="new_car_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $newCars = $em->getRepository(NewCar::class)->findAll();


        return $this->render('Backend/new_car_list.html.twig', array(
            'cars' => $newCars,
        ));
    }

    /**
     * Creates a new vehicle entity.
     *
     * @Route("/new", name="car_vehicle_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $newCar = new NewCar();
        $form = $this->createForm(NewCarType::class, $newCar);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $newCar->setTitle($newCar->getMarque()." ".$newCar->getModele());
            $imagesin = $newCar->getImageins();
            foreach ($imagesin as $in){
                $fileName = $this->fileUploader->upload($in->getPath());
                $in->setPath($fileName);
                $in->setCar($newCar);
            }

            $imagesout = $newCar->getImageouts();
            foreach ($imagesout as $out){
                $fileName = $this->fileUploader->upload($out->getPath());
                $out->setPath($fileName);
                $out->setCar($newCar);
            }


            $em->persist($newCar);
            $em->flush();
//
//            $photos= $request->files->all();
//            dump($photos);die();
//            foreach ($photos as $key => $value){
//                /** @var Symfony\Component\HttpFoundation\File\UploadedFile $value */
//
//
//                if(null !== $value){
//                    $fileName = $this->fileUploader->upload($value);
//                    $photo= new Photo();
//                    $photo->setPath($fileName);
//                    $photo->setName("sayara.tn".$newCar->getMarque()."".$newCar->getModele());
//                    $photo->setType(substr($key,0,-1));
//                    $newCar->addPhoto($photo);
//                    $photo->setNewCar($newCar);
//
//
//                    $em->persist($photo);
//                }
//
//
//            }
//            $em->flush();
//            //dump($newCar->getPhotos());die();

            return $this->redirectToRoute('vehicle_edit' , array('id' => $newCar->getId()));
        }

        return $this->render('Backend/new_car.html.twig', array(
            'vehicle' => $newCar,
            'form' => $form->createView(),
        ));
    }





    /**
     * Displays a form to edit an existing vehicle entity.
     *
     * @Route("/{id}/edit", name="vehicle_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, NewCar $vehicle)
    {

        $editForm = $this->createForm('Sayara\BackendBundle\Form\NewCarType', $vehicle);
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $data= $editForm->getData();
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('new_car_index');
        }

        return $this->render('Backend/edit_new_car.html.twig', array(
            'vehicle' => $vehicle,
            'edit_form' => $editForm->createView(),
        ));
    }



    /**
     *
     * @Method({"GET", "POST"})
     * @Route("/ajax/snippet/uploaded/receive/new/car", name="get_uploaded_photos_new_car")
     */

    public function getUploadedPhotos(Request $request){

        $id= $request->get('id');
        $vehicle = $this->getDoctrine()->getManager()->getRepository(NewCar::class)->find($id);
        $photos= $this->getDoctrine()->getManager()->getRepository(Photo::class)->findByNewCar($vehicle);


        $photosData= [];
        foreach ($photos as $photo){
            $photosData[] =['name' => $photo->getName()];
        }

        return new JsonResponse(array('success' => true, 'photos' => $photosData ));
    }

    /**
     *
     * @Method({"GET", "POST"})
     * @Route("/ajax/snippet/image/send/new/car/{id}", name="ajax_snippet_photo_send_new_car")
     */
    public function ajaxSnippetImageSendAction(Request $request, $id)
    {

        $photo = new Photo();
        $media = $request->files->get('file');

        $vehicle = $this->getDoctrine()->getManager()->getRepository(NewCar::class)->find($id);

        $photo->setFile($media);

        //$media->
        $photo->setPath($media->getPathName());
        $photo->setName($media->getClientOriginalName());


        $photo->setNewCar($vehicle);
        $vehicle->addPhoto($photo);

        $photo->upload();
        $em = $this->getDoctrine()->getManager();
        $em->persist($photo);
        $em->flush();

        if ($vehicle->getPhotos() !== null){
            $photos= $vehicle->getPhotos();
        }
        return new JsonResponse(array('success' => true, 'media' => $media, 'photos'=>$photos ));
    }


    /**
     * Lists all new cars.
     *
     * @Route("/new-car-slider", name="new_car_slider")
     * @Method("GET")
     */
    public function listNewCarSliderAction()
    {
        $em = $this->getDoctrine()->getManager();

        $newCars = $em->getRepository(NewCar::class)->findAll();

        return $this->render('default/index.html.twig', array(
            'cars' => $newCars,
        ));
    }















}
