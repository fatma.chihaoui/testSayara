<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'sayara2');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', '127.0.0.1');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ' ^k(>U-@cm_k]YDG{Fr`nB0~YhVq}RRRiXQs/abO}/(jR*xo7[t4iZndN7wtR5#%');
define('SECURE_AUTH_KEY',  'pq=$:{_>ZQh&&wypomf$M14s*{}5=YGs=E1H|#B=,V6MEpXd>@,x-yh)[O;agERe');
define('LOGGED_IN_KEY',    '!rH,yct?RUEmxCgg3g!OD{T<qO<mwGvh!2Ca&pKL<F! `]H;T.Ju,4(26v^j,Lhn');
define('NONCE_KEY',        'bJ%u$|$[?NXoS:=$G1*#2_qN+.:+v+q|+15WTQ1(pwL-6I03(X*k+{F1~)`&L=Ug');
define('AUTH_SALT',        '9cU=)b!M8YwD^drC#adpP`N@>?c[491i(6,k*@GD^(0)GILiU.@BPkE! {rNy^@?');
define('SECURE_AUTH_SALT', '@/+8}M! +M7)mo?*r}e>?n:YdgDS1_@|Rjwz*gqgx@ws(@K#&#$y)t[m%KMD-^d#');
define('LOGGED_IN_SALT',   'KV=_JBg1M5bBh#k*j=iaUQ)s)3Q6ZAM|3i6>-2Fx@t:YmvfDq@q#U|QFZ(PfNaO1');
define('NONCE_SALT',       '4F$skD)ql~<Ip6XC?[XAY|J]Hf==@g<Il&QV<T+NK@^x6< D5@}%}$?zjN *!^Ss');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'swp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
