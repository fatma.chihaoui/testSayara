jQuery(function($) {

	"use strict";
	

	/**
	 * Sticky Header
	 */

	
	/**
	 * Slicknav - a Mobile Menu
	 */
	
	
	

	/**
	 * Slick
	 */
	
	$('.slick-hero-slider').slick({
		dots: true,
		infinite: true,
		speed: 500,
		slidesToShow: 1,
		slidesToScroll: 1,
		centerMode: true,
		infinite: true,
		centerPadding: '0',
		focusOnSelect: true,
		adaptiveHeight: true,
		autoplay: true,
		autoplaySpeed: 4500,
		pauseOnHover: true,
	});
	
	/**
	 * Gallery Slideshow - slick
	 */
	$('.gallery-slideshow-review').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		speed: 500,
		arrows: true,
		fade: true,
		asNavFor: '.gallery-nav-review'
	});
	$('.gallery-nav-review').slick({
		slidesToShow: 8,
		slidesToScroll: 1,
		speed: 500,
		asNavFor: '.gallery-slideshow-review',
		dots: false,
		centerMode: true,
		focusOnSelect: true,
		infinite: true,
		responsive: [
			{
			breakpoint: 1199,
			settings: {
				slidesToShow: 7,
				}
			}, 
			{
			breakpoint: 991,
			settings: {
				slidesToShow: 6,
				}
			}, 
			{
			breakpoint: 767,
			settings: {
				slidesToShow: 5,
				}
			}, 
			{
			breakpoint: 480,
			settings: {
				slidesToShow: 3,
				}
			}
		]
	});

	$('.gallery-slideshow-review-2').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		speed: 500,
		arrows: true,
		fade: true,
		asNavFor: '.gallery-nav-review-2'
	});
	$('.gallery-nav-review-2').slick({
		slidesToShow: 8,
		slidesToScroll: 1,
		speed: 500,
		asNavFor: '.gallery-slideshow-review-2',
		dots: false,
		centerMode: true,
		focusOnSelect: true,
		infinite: true,
		responsive: [
			{
			breakpoint: 1199,
			settings: {
				slidesToShow: 7,
				}
			}, 
			{
			breakpoint: 991,
			settings: {
				slidesToShow: 6,
				}
			}, 
			{
			breakpoint: 767,
			settings: {
				slidesToShow: 5,
				}
			}, 
			{
			breakpoint: 480,
			settings: {
				slidesToShow: 3,
				}
			}
		]
	});
	
	$('.slick-testimonial').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		speed: 500,
		arrows: false,
		fade: false,
		asNavFor: '.slick-testimonial-nav',
		adaptiveHeight: true,
	});
	$('.slick-testimonial-nav').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		speed: 500,
		centerPadding: '0',
		asNavFor: '.slick-testimonial',
		dots: false,
		centerMode: true,
		focusOnSelect: true,
		infinite: true,
		responsive: [
			{
			breakpoint: 1199,
			settings: {
				slidesToShow: 3,
				}
			}, 
			{
			breakpoint: 991,
			settings: {
				slidesToShow: 3,
				}
			}, 
			{
			breakpoint: 767,
			settings: {
				slidesToShow: 3,
				}
			}, 
			{
			breakpoint: 480,
			settings: {
				slidesToShow: 3,
				}
			}
		]
	});

	$('.gallery-slideshow-not-tab').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		speed: 500,
		arrows: true,
		fade: true,
		asNavFor: '.gallery-nav-not-tab'
	});
	$('.gallery-nav-not-tab').slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		speed: 500,
		asNavFor: '.gallery-slideshow-not-tab',
		dots: false,
		centerMode: true,
		focusOnSelect: true,
		infinite: true,
		responsive: [
			{
			breakpoint: 1199,
			settings: {
				slidesToShow: 5,
				}
			}, 
			{
			breakpoint: 991,
			settings: {
				slidesToShow: 4,
				}
			}, 
			{
			breakpoint: 767,
			settings: {
				slidesToShow: 3,
				}
			}, 
			{
			breakpoint: 480,
			settings: {
				slidesToShow: 2,
				}
			}
		]
	});

	$('#detailTab').on('shown.bs.tab', function (e) {

		$('.gallery-slideshow-tab-01').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			speed: 500,
			arrows: true,
			fade: true,
			asNavFor: '.gallery-nav-tab-01'
		});
		$('.gallery-nav-tab-01').slick({
			slidesToShow: 5,
			slidesToScroll: 1,
			speed: 500,
			asNavFor: '.gallery-slideshow-tab-01',
			dots: false,
			centerMode: true,
			focusOnSelect: true,
			infinite: true,
			responsive: [
				{
				breakpoint: 1199,
				settings: {
					slidesToShow: 5,
					}
				}, 
				{
				breakpoint: 991,
				settings: {
					slidesToShow: 4,
					}
				}, 
				{
				breakpoint: 767,
				settings: {
					slidesToShow: 3,
					}
				}, 
				{
				breakpoint: 480,
				settings: {
					slidesToShow: 2,
					}
				}
			]
		});

	});
	
});

