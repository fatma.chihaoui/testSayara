jQuery(function($){
	"use strict";

var AUTOPLUS = window.AUTOPLUS || {};

/* ==================================================
   Hero Flex Slider
================================================== */
	AUTOPLUS.heroflex = function() {
		$('.heroflex').each(function(){
				var carouselInstance = $(this); 
				var carouselAutoplay = carouselInstance.attr("data-autoplay") == 'yes' ? true : false
				var carouselPagination = carouselInstance.attr("data-pagination") == 'yes' ? true : false
				var carouselArrows = carouselInstance.attr("data-arrows") == 'yes' ? true : false
				var carouselDirection = carouselInstance.attr("data-direction") ? carouselInstance.attr("data-direction") : "horizontal"
				var carouselStyle = carouselInstance.attr("data-style") ? carouselInstance.attr("data-style") : "fade"
				var carouselSpeed = carouselInstance.attr("data-speed") ? carouselInstance.attr("data-speed") : "5000"
				var carouselPause = carouselInstance.attr("data-pause") == 'yes' ? true : false
				
				carouselInstance.flexslider({
					animation: carouselStyle,
					easing: "swing",               
					direction: carouselDirection,       
					slideshow: carouselAutoplay,              
					slideshowSpeed: carouselSpeed, 
					pauseOnAction: false,    
					touch: true,    
					animationSpeed: 600,         
					initDelay: 0,              
					randomize: false,            
					pauseOnHover: carouselPause,       
					controlNav: carouselPagination,           
					directionNav: carouselArrows,            
					prevText: "",         
					nextText: ""
				});
		});
	}
/* ==================================================
   slider_slick Slider
================================================== */
	AUTOPLUS.Home_Multi_Sider = function() {

		 $('.slider_slick').slick({
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  arrows: false,
		  fade: true,
		  asNavFor: '.slider_nav'
		});
		$('.slider_nav').slick({
		  slidesToShow: 3,
		  slidesToScroll: 1,
		  asNavFor: '.slider_slick',
		  dots: false,
		    arrows: false,
		  centerMode: true,
		  focusOnSelect: true,
		   responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 3,
		        slidesToScroll: 3, 
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 2
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		    // You can unslick at a given breakpoint now by adding:
		    // settings: "unslick"
		    // instead of a settings object
		  ]
		});
	}
/* ==================================================
   element_slider Slider
================================================== */
AUTOPLUS.element_slider = function() {

	$('.element_slider').slick({
	  	infinite: true,
	  	slidesToShow: 3,
	  	slidesToScroll: 3,
		nextArrow: '.prx_next_slick',
		prevArrow: '.prx_prev_slick',
		 responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 3,
		        slidesToScroll: 3, 
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 2
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		    // You can unslick at a given breakpoint now by adding:
		    // settings: "unslick"
		    // instead of a settings object
		  ]
	});
}
/* ==================================================
   element_slider_2 Slider
================================================== */
AUTOPLUS.element_slider_2 = function() {

	$('.element_slider_2').slick({
	  	infinite: true,
	  	slidesToShow: 2,
	  	slidesToScroll: 2,
	  	arrows: false,
		dots:true ,
		 responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 3,
		        slidesToScroll: 3, 
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 2
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		    // You can unslick at a given breakpoint now by adding:
		    // settings: "unslick"
		    // instead of a settings object
		  ]
	});
}
/* ==================================================
   dropdown menu
================================================== 
AUTOPLUS.dropdown_menu = function() {
 
$('ul.nav li.dropdown').bind('mouseover', function(){ 
      $(this).find('.dropdown-menu').fadeIn(500);	
 
}).bind('mouseout', function(){

	  $(this).find('.dropdown-menu').fadeOut(500); 
});

}


//AUTOPLUS.dropdown_menu();*/
AUTOPLUS.heroflex();
AUTOPLUS.Home_Multi_Sider() ; 
AUTOPLUS.element_slider() ;
AUTOPLUS.element_slider_2() ;

})
