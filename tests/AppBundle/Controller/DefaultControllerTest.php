<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public $client;
    public function testIndex()
    {

    }
    /**
     * @dataProvider urlProvider
     */
    public function testPageIsSuccessful($url)
    {
        $client = self::createClient();
        $client->request('GET', $url);

        $this->assertTrue($client->getResponse()->isSuccessful());

    }
    public function urlProvider()
    {
        $client = self::createClient();
        $router = $client->getContainer()->get('router');
        $urls =  array(
            [$router->generate('new_car_page_index', array(), false)],
            [$router->generate('home_page', [], false)],
            [$router->generate('application_new', ['type'=>1, 'slug'=> "categ"], false)],
            [$router->generate('application_new', ['type'=>2, 'slug'=> "categ"], false)],
            [$router->generate('spare_new', array(), false)],
            [$router->generate('search_index', array(), false)],
            [$router->generate('new_car_page_index2', ['marque'=>2, 'slug'=> "categ"], false)],
            [$router->generate('search_category', ['code'=>275, 'slug'=> "categ"], false)],
            [$router->generate('search_city', ['code'=>3079, 'slug'=> "categ"], false)],
            ['/annonces/?marque=&modele=&type=1&city=&minp=&maxp='],
            ['?marque=41&modele=5&type=1&city=1&minp=100&maxp=1000'],
            ['?marque=41&modele=5&type=2&minp=100&maxp=1000']


        );
        //dump($urls);
        return $urls;
    }
}
